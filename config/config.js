module.exports = {
    paths: {
        mainPath: 'khydma/io'
    },
    topics: {
        newMessage: 'newMessage',
        newConversation: 'newConversation',
        deleteConversation: 'deleteConversation',
        updatedConversation: 'updatedConversation',
    },
    permissionLevels: {
        admin: 4096,
        user: 1,
        pro: 1024,
        superAdmin: 8192
    }
};
