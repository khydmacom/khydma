let checkPermission = (req, res) => {
    // console.log();
    let user = req.user.user;
    if (user && user.permissionLevel > 2048) {
        return true;
    } else {
        res.json({code: 69, message: "vous n'avez pas la permission suffisante"})
    }
};

let checkSuperPermission = (req, res) => {
    let user = req.user.user;
    if (user && user.permissionLevel > 4096) {
        return true;
    } else {
        res.json({code: 69, message: "vous n'avez pas la permission suffisante"})
    }
};

module.exports = {
    checkPermission,
    checkSuperPermission
};
