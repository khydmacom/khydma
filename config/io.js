const socketIo = require('socket.io');
const jwt = require('jsonwebtoken');

const Message = require('../models/message');
const Conversation = require('../models/conversation');

const config = require('./config');
const SECRET = require('./keys').serverSecret;

const {topics} = config;


// to secure our socket requests we make sure every user is authenticated

const authenticate = (socket, next) => {
    if (socket.handshake.query && socket.handshake.query.token) {
        jwt.verify(socket.handshake.query.token, SECRET, (err, decoded) => {
            if (err) return next(new Error('Authentication error'));

            socket.decoded = decoded;
            next();
        });
    } else {
        next(new Error('Authentication error'));
    }
};


const users = [];
const connections = [];

const initialize = (server) => {

    console.log('connection io started');

    const io = socketIo(server);

    io.use(authenticate).on('connection', (socket) => {
        connections.push(socket);

        socket.on('userid', data => {
            if (data.userid) {
                socket.userid = data.userid;
                let user = {userid: socket.userid, id: socket.id};
                let existing = searchUser(user.userid);
                if (existing === false) {
                    users.push(user);
                    console.log('connected currentUsers == ', users);
                }
            }
        });

        socket.on(topics.newMessage, data => {
            const {receiverId, userId, proId, ...others} = data;
            // const conversationId = others;
            Conversation.updateOne({userId, proId}, (_success, body, _err) => {
                if (_success) {
                    Message.newMessage({conversationId: body._id, ...others}, (success, newmessage, err) => {
                        if (success) {
                            Conversation.updateOne({
                                userId,
                                proId,
                                lastMessageId: newmessage._id
                            }, (__success, _body, ___err) => {
                                let instances = searchConnections(receiverId);

                                socket.emit(topics.newMessage, newmessage);
                                socket.emit(topics.newConversation, _body);
                                instances.map((inst, index) => {
                                    socket.to(inst.id).emit(topics.newMessage, newmessage);
                                    socket.to(inst.id).emit(topics.newConversation, _body);
                                })
                            });
                        }
                    });
                }
            });
        });

    })
};

const searchUser = userid => {
    for (let i = 0; i < users.length; i++) {
        if (users[i].userid === userid) {
            return users[i];
        }
    }

    return false;
};


const searchConnections = userid => {
    let found = [];
    for (let conn of connections) {
        // special condition should be with '=='
        if (conn.userid == userid) {
            found.push(conn);
        }
    }

    if (found.length > 0) {
        console.log(found)
        return found;
    } else {
        return false;
    }
};

module.exports = initialize;
