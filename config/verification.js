const verifier = require('email-verify');
const infoCodes = verifier.infoCodes;

async function validateEmail(email) {
    // var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    let verified = false;
    let tested = false;
    verifier.verify(email, function (err, info) {
        if (err) {
            console.log(err);
            verified = false;
            tested = true;
        } else {
            console.log("Success (T/F): " + info.success);
            console.log("Info: " + info.info);

            verified = info.success;
            tested = true;
        }
    });
    await tested;
        return verified;
}

function verifSignData(req, res, next) {
    const {name, lastName, email, password} = req.body;
    if (!name || !email || !lastName || !password) {
        return res.status(204).json({message: 'S\'il vous plaît entrer tous les champs'});
    } else if (!validateEmail(email)) {
        return res.status(204).json({message: 'Vous devez entrer une adresse email valide'});
    } else if (password.length < 8) {
        return res.status(204).json({message: 'Le mot de passe doit être au moins de 8 caractères'});
    } else next();
}

module.exports = {
    validateEmail,
    verifSignData
};
