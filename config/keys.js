module.exports = {
    mongoURI: 'mongodb://localhost/khydmaDev',
    serverSecret: 'xmY1njJNPekTQm5ZtYKwpSOiMbGu8V14',
    fb: {
        app_secret: '84d1669c6f202b723a40e4cac4c17daf',
        app_id: '2494749053932405',
        callback: "https://xdabier.com/users/facebook/callback",
        debug_token: "https://graph.facebook.com/debug_token",
        oaauth: "https://graph.facebook.com/oauth"
    },
    google: {
        clientId: "572380816287-v992qkvdfdi318f2dd1ubv1uc0i2fhgi.apps.googleusercontent.com",
        clientSecret: "e89hoD8B-rMAn2yfguic20gP"
    },
    driverFields: {
        pro: 'pro',
        artisan: 'artisan',
        permis: 'permis',
        id: 'id',
        license: 'license'
    },
    tripStates: {
        confirmed: 'confirmed',
        cancelled: 'cancelled',
        pending: 'pending'
    },
    tripStatuses: {
        started: 'started',
        ended: 'ended',
        pendingConfirmation: 'pendingConfirmation'
    },
    tripTypes: {
        immediate: 'immediate',
        reservation: 'reservation',
        pack: 'pack',
        thirdParty: 'thirdParty'
    }
};
