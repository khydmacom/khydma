const LocalStrategy = require('passport-local').Strategy;
const FacebookTokenStrategy = require('passport-facebook-token');
const GoogleTokenStrategy = require('passport-google-token').Strategy;
const FB = require('./keys').fb;
const GOOGLE = require('./keys').google;
// const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

// Load User model
const User = require('../models/user');

module.exports = function (passport) {
    passport.use(
        new LocalStrategy({usernameField: 'email'}, (email, password, done) => {
            // Match user
            User.authenticate(email, password, (err, user, msg) => {
                return done(err, user, msg);
            });
        })
    );

    passport.use(new FacebookTokenStrategy({
            clientID: FB.app_id,
            clientSecret: FB.app_secret
        },
        (accessToken, refreshToken, profile, done) => {
        console.log(accessToken)
            User.upsertFbUser(accessToken, refreshToken, profile, (err, user, message) => {
                return done(err, user, message);
            });
        })
    );

    passport.use(new GoogleTokenStrategy({
            clientID: GOOGLE.clientId,
            clientSecret: GOOGLE.clientSecret
        },
        (accessToken, refreshToken, profile, done) => {
            User.upsertGGUser(accessToken, refreshToken, profile, (err, user, message) => {
                return done(err, user, message);
            });
        })
    );
};
