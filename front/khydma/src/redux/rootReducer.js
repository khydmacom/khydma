import {combineReducers} from 'redux';
import {
    authReducer, currentUsersReducer, rootsReducer,
    usersReducer, categoriesReducer, locationsReducer,
    prosReducer, proFormFillingReducer, messagingReducer
} from '../views/reducers';

const rootReducer = combineReducers({
    authReducer,
    currentUsersReducer,
    rootsReducer,
    usersReducer,
    categoriesReducer,
    locationsReducer,
    prosReducer,
    proFormFillingReducer,
    messagingReducer
});

export default rootReducer;
