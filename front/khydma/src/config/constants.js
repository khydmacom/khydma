// We have disabled the constants related to Google maps for the moment due to their aggressive billing plans


// https://geocoder.tilehosting.com/r/10.8112885/35.7642515.js?key=ZrbjckrAMg6TvoL1RfOz
const API_Dev = "http://localhost:3085/api/";
const API_Socket_Dev = "http://localhost:3085";
const API_OSM_KEY = "W77rARck56iGas1YysID";
// const API_GOOGLE_KEY = "AIzaSyDfbqtslPFs2LNOwfyGkDzfw8hI3zlFBg8";
// const GOOGLE_PLACES_LINK = `https://maps.googleapis.com/maps/api/js?key=${API_GOOGLE_KEY}&libraries=places`;
const OSM_AUTOCOMPLETE_LINK = `https://cdn.klokantech.com/osmnames/v1/autocomplete.js`;
const OSM_GEOCODER_BASE_LINK = `https://nominatim.openstreetmap.org/reverse?format=geojson&`;
// const OSM_GEOCODER_BASE_LINK = `https://search.osmnames.org/r/`;
const OSM_AUTOCOMPLETE_BASE_API = `https://geocoder.tilehosting.com/`;
const COUNTRY_CODE = 'tn';

const engineKey = "%x4Z#iz*]V\"$kP]"

// const GOOGLE_GEOCODING_BASE_LINK = `https://maps.googleapis.com/maps/api/geocode/json?latlng=`;



const localStorageIndexes = {
    user: 'user'
};

const ioTriggers = {
    error: 'error',
    connect: 'connect',
    me: 'me',
    updateCategories: 'updateCategories',
    userid: 'userid'
};

// const googleAutoCompleteListeners = {
//     place_changed: 'place_changed'
// };

// const googleAutoCompleteOptions = {
//     types: ['(cities)'],
//     componentRestrictions: {country: "tn"}
// };

const pubSubConstants = {
    showFilter: 'showFilter',
    hideFilter: 'hideFilter',
    updateMainDashboardPadding: 'updateMainDashboardPadding',
    reUpdateDrawerWidth: 'reUpdateDrawerWidth',
    navigateBackHome: 'navigateBackHome',
    refreshTokenInHeader: 'refreshTokenInHeader'
};

export default {
    API_Dev,
    API_Socket_Dev,
    API_OSM_KEY,
    OSM_AUTOCOMPLETE_LINK,
    OSM_GEOCODER_BASE_LINK,
    OSM_AUTOCOMPLETE_BASE_API,
    COUNTRY_CODE,
    // API_GOOGLE_KEY,
    // GOOGLE_PLACES_LINK,
    localStorageIndexes,
    ioTriggers,
    pubSubConstants,
    engineKey
    // googleAutoCompleteListeners,
    // GOOGLE_GEOCODING_BASE_LINK
}



// removed method due to ditching google api
/*GeoCoder.geocode({location}, (result: {
    types: string[],
    formatted_address: string,
    address_components: {
        short_name: string,
        long_name: string,
        types: string[]
    }[],
    place_id: string,
    geometry: {
        location: any,
        location_type: any,
        viewport: any,
        bounds: any
    }
}[], status) => {
    console.log(result, status);
    if (status === "OK") {
        let i = result.findIndex(this.searchNeededResult);
        if (i > -1) {
            this.setState({
                loc: [location.lng, location.lat],
                locString: result[i].formatted_address,
                locationTitle: result[i].address_components[0].short_name
            })
        }
    }
})*/


// removed method due to ditching google api
/*searchNeededResult(res: {
    types: string[],
    formatted_address: string,
    address_components: {
        short_name: string,
        long_name: string,
        types: string[]
    }[],
    place_id: string,
    geometry: {
        location: any,
        location_type: any,
        viewport: any,
        bounds: any
    }
}) {
    return (res.types[0] === "locality" || res.types[0] === "administrative_area_level_1");
}*/
