import axios from 'axios';
import CONSTS from '../constants';
import qs from 'qs';

const {API_Dev} = CONSTS;

axios.defaults.baseURL = API_Dev;

function signUp(body: {"email": string, "name": string, "lastName": string, "phone": number, "password": string}) {
    return axios.post(`users/register`, qs.stringify(body));
}

function login(body: {email: string, password: string}) {
    return axios.post(`users/login`, qs.stringify(body));
}

function getToken(body: {refreshToken: string}) {
    return axios.post(`users/token`, qs.stringify(body));
}

function rejectToken(body: {refreshToken: string}) {
    return axios.post(`users/token/reject`, qs.stringify(body));
}

export default {
    getToken,
    login,
    rejectToken,
    signUp
}
