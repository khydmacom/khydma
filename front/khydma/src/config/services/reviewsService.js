import axios from 'axios';
import CONSTS from '../constants';
import qs from 'qs';

const {localStorageIndexes, API_Dev} = CONSTS;

axios.defaults.baseURL = API_Dev;
const baseRoute = 'reviews';


function setTokenInHeader() {
    if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
        axios.defaults.headers.common = {'Authorization': `bearer ${JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.accessToken}`};
    else
        axios.defaults.headers.common = {'Authorization': ` `};
}

function setQueryParams(query: object): string {
    let keys = Object.keys(query);

    if (keys.length > 0) {
        let queryParams = "?";
        keys.forEach((key, index) => {
            queryParams += `${key}=${query[key]}&`;
        });
        queryParams = queryParams.slice(0, -1);
        return queryParams;
    } else {
        return "";
    }
}

setTokenInHeader();

function getReviews(query) {
    setTokenInHeader();
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/${queryParams}`);
}
function getReviewsStats(query) {
    setTokenInHeader();
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/stats/${queryParams}`);
}

function getOneReview(query = {}) {
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/one/${queryParams}`);
}

function addReview(body: any) {
    setTokenInHeader();

    return axios.post(`${baseRoute}/new/`, qs.stringify(body));
}

function deleteReview(body: any) {
    setTokenInHeader();

    return axios.post(`${baseRoute}/delete/`, qs.stringify(body));
}

export default {
    getReviews,
    addReview,
    deleteReview,
    getOneReview,
    getReviewsStats
}
