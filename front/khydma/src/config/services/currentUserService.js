import axios from 'axios';
import CONSTS from '../constants';
const {localStorageIndexes, API_Dev} = CONSTS;

axios.defaults.baseURL = API_Dev;


function setTokenInHeader() {
    if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
        axios.defaults.headers.common = {'Authorization': `bearer ${JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.accessToken}`};
    else
        axios.defaults.headers.common = {'Authorization': ` `};
}

setTokenInHeader();

function getMe() {
    setTokenInHeader();
    return axios.get('users/me');
}

function getMyProfile() {
    setTokenInHeader();
    return axios.get('users/me/all');
}

function checkAdmin() {
    setTokenInHeader();
    return axios.get('users/admin');
}

function checkPro() {
    setTokenInHeader();
    return axios.get('users/pro');
}

export default {
    getMe,
    checkAdmin,
    getMyProfile,
    checkPro
}
