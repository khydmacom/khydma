import axios from 'axios';
import CONSTS from '../constants';
import qs from 'qs';
import {array} from "prop-types";

const {localStorageIndexes, API_Dev} = CONSTS;


axios.defaults.baseURL = API_Dev;
const baseRoute = 'pro';

function setTokenInHeader() {
    if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
        axios.defaults.headers.common = {'Authorization': `bearer ${JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.accessToken}`};
    else
        axios.defaults.headers.common = {'Authorization': ` `};
}
setTokenInHeader();


function setQueryParams(query: object): string {
    let keys = Object.keys(query);

    if (keys.length > 0) {
        let queryParams = "?";
        keys.forEach((key, index) => {
            queryParams += `${key}=${query[key]}&`;
        });
        queryParams = queryParams.slice(0, -1);
        return queryParams;
    } else {
        return "";
    }
}

function getPros(query = {}) {
    let queryParams = setQueryParams(query);
    // console.log(`${baseRoute}${queryParams.includes('label') ? '/category/' : '/'}${queryParams}`)
    return axios.get(`${baseRoute}${queryParams.includes('label') ? '/category/' : '/'}${queryParams}`);
}

function getIndexedPros(query = {}) {
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/indexes/${queryParams}`);
}

function searchPros(query = {}) {
    let queryParams = setQueryParams(query);
    // console.log(`${baseRoute}/search/${queryParams}`);
    return axios.get(`${baseRoute}/search/${queryParams}`);
}

function getOnePro(query = {}) {
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/one/${queryParams}`);
}

function getOneGalleryImage(query = {}) {
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/gallery/one/${queryParams}`);
}

function getGalleryImages(query = {}) {
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/gallery/${queryParams}`);
}

function deleteGalleryImages(body) {
    return axios.post(`${baseRoute}/gallery/delete/`, qs.stringify(body));
}

function addPro(body: any) {
    setTokenInHeader();

    return axios.post(`${baseRoute}/new/`, qs.stringify(body));
}

function updatePro(body: any) {
    setTokenInHeader();

    return axios.post(`${baseRoute}/update/`, qs.stringify(body));
}

function updateProImage(body: any, cb) {
    setTokenInHeader();
    const form = new FormData();
    form.append('picturePro', body.file);
    form.append('_id', body._id);

    return axios.post(`${baseRoute}/update/image`, form, {
        onUploadProgress: ProgressEvent => {
            cb({
                loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
            })
        }, headers: {'Content-Type': 'multipart/form-data'}
    });
}

function addProGalleryImage(body: any, cb) {
    setTokenInHeader();
    const form = new FormData();
    const bodyKey = Object.keys(body);
    bodyKey.forEach((key, index) => {
       form.append(key, body[key]);
    });

    return axios.post(`${baseRoute}/gallery/add/`, form, {
        onUploadProgress: ProgressEvent => {
            cb({
                loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
            })
        }, headers: {'Content-Type': 'multipart/form-data'}
    });
}

function deletePro(_id: string) {
    setTokenInHeader();

    return axios.get(`${baseRoute}/delete/${_id}`);
}


export default {
    addPro,
    deletePro,
    getPros,
    updatePro,
    getOnePro,
    updateProImage,
    searchPros,
    getIndexedPros,
    addProGalleryImage,
    getGalleryImages,
    getOneGalleryImage,
    deleteGalleryImages
}
