import axios from 'axios';
import CONSTS from '../constants';
import qs from 'qs';

const {localStorageIndexes, API_Dev} = CONSTS;




axios.defaults.baseURL = API_Dev;

function setTokenInHeader() {
    if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
        axios.defaults.headers.common = {'Authorization': `bearer ${JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.accessToken}`};
    else
        axios.defaults.headers.common = {'Authorization': ` `};
}

setTokenInHeader();

function getUser(_id: string) {
    return axios.get(`users/${_id}`);
}

function updatePassword(body: {"email": string, "password": string, "newPassword": string, "_id": string}) {
    setTokenInHeader();

    return axios.post('users/update-pass', qs.stringify(body));
}

function update(body: any) {
    setTokenInHeader();

    return axios.post('users/update', qs.stringify(body));
}

function remove(body: {"_id": string}) {
    setTokenInHeader();

    return axios.post('users/remove', qs.stringify(body));
}

function updateUserImage(body: any, cb) {
    setTokenInHeader();
    const form = new FormData();
    form.append('profilePicture', body.file);

    return axios.post(`users/update/image`, form, {
        onUploadProgress: ProgressEvent => {
            cb({
                loaded: (ProgressEvent.loaded / ProgressEvent.total * 100),
            })
        }, headers: {'Content-Type': 'multipart/form-data'}
    });
}


export default {
    getUser,
    update,
    updatePassword,
    remove,
    updateUserImage
}
