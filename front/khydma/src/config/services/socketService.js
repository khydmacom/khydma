import * as io from 'socket.io-client';
import CONSTS from '../../config/constants';

const {ioTriggers, localStorageIndexes, API_Socket_Dev} = CONSTS;

export default class SocketServices {
    socket = {};

    sendUser(userid: string): void {
        this.socket.emit(ioTriggers.userid, {userid: userid});
    }

    connectSocket(userid: string, cb: function) {
        let token = JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.accessToken;

        this.socket = io(API_Socket_Dev, {query: {token: token}});

        this.socket.on(ioTriggers.error, (error) => {
            console.log('====================================');
            console.log('socket error = ', error);
            console.log('====================================');
        });

        this.socket.on(ioTriggers.connect, () => {
            this.sendUser(userid);
            cb();
        })
    }
}
