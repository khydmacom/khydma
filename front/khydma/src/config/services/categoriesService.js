import axios from 'axios';
import CONSTS from '../constants';
import qs from 'qs';

const {localStorageIndexes, API_Dev} = CONSTS;




axios.defaults.baseURL = API_Dev;
const baseRoute = 'category';


function setTokenInHeader() {
    if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
        axios.defaults.headers.common = {'Authorization': `bearer ${JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.accessToken}`};
    else
        axios.defaults.headers.common = {'Authorization': ` `};
}

setTokenInHeader();

function getCategories() {
    return axios.get(`${baseRoute}/`);
}

function getOneCategory(_id: string) {
    return axios.get(`${baseRoute}/${_id}`);
}

function addCategory(body: any) {
    setTokenInHeader();

    return axios.post(`${baseRoute}/new/`, qs.stringify(body));
}

function updateCategory(body: any) {
    setTokenInHeader();

    return axios.post(`${baseRoute}/update/`, qs.stringify(body));
}

function updateCategoryImage(body: any, cb) {
    setTokenInHeader();
    const form = new FormData();
    form.append('picture', body.file);
    form.append('_id', body._id);

    return axios.post(`${baseRoute}/update/image`, form, {
        onUploadProgress: ProgressEvent => {
            cb({
                loaded: (ProgressEvent.loaded / ProgressEvent.total*100),
            })
        }, headers: { 'Content-Type': 'multipart/form-data' }
    });
}

function deleteCategory(_id: string) {
    setTokenInHeader();

    return axios.get(`${baseRoute}/delete/${_id}`);
}


export default {
    addCategory,
    deleteCategory,
    getCategories,
    updateCategory,
    getOneCategory,
    updateCategoryImage
}
