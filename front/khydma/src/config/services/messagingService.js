import axios from 'axios';
import CONSTS from '../constants';
import qs from 'qs';

const {localStorageIndexes, API_Dev} = CONSTS;

axios.defaults.baseURL = API_Dev;
const baseRoute = 'messaging';


function setTokenInHeader() {
    if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
        axios.defaults.headers.common = {'Authorization': `bearer ${JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.accessToken}`};
    else
        axios.defaults.headers.common = {'Authorization': ` `};
}

function setQueryParams(query: object): string {
    let keys = Object.keys(query);

    if (keys.length > 0) {
        let queryParams = "?";
        keys.forEach((key, index) => {
            queryParams += `${key}=${query[key]}&`;
        });
        queryParams = queryParams.slice(0, -1);
        return queryParams;
    } else {
        return "";
    }
}

setTokenInHeader();

function getConversations(query) {
    setTokenInHeader();
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/${queryParams}`);
}

function getMessages(query) {
    setTokenInHeader();
    let queryParams = setQueryParams(query);
    return axios.get(`${baseRoute}/messages/${queryParams}`);
}


export default {
    getConversations,
    getMessages
}
