import React from 'react';
import {
    CategoriesListComponent,
    OverviewComponent, ParametersComponent,
    ProsListComponent,
    UsersListComponent
} from "../views/containers/adminDashboard/components";

import {Button, createMuiTheme} from "@material-ui/core";
import {
    CategoriesComponent,
    HomeComponent,
    LoginComponent,
    SignUpComponent,
    SearchResultsComponent,
    ProFormComponent,
    ProfileComponent
} from "../views/containers";
import {object} from "prop-types";
import homeIcon from "../assets/icons/svg/home.svg";
import heartIcon from "../assets/icons/svg/heartbeat.svg";
import portfolioIcon from "../assets/icons/svg/portfolio.svg";
import eventsIcon from "../assets/icons/svg/event.svg";
import bookIcon from "../assets/icons/svg/hardbound-book-variant.svg";
import {Misc} from "./index";
import {Link} from "@reach/router";

const THEME = createMuiTheme({
    palette: {
        primary: {
            // light: '#fcfffc',
            main: '#FCFFFC',
            // dark: '#b0b2b0',
            // contrastText: '#000',
        },
        secondary: {
            // light: '#55b96e',
            main: '#2BA84A',
            // dark: '#1e7533',
            // contrastText: '#fff',
        }
    }, typography: {
        useNextVariants: true,
    }
});

const FORM_THEME = createMuiTheme({
    palette: {
        primary: {
            // light: '#fcfffc',
            main: '#2BA84A'
            // dark: '#b0b2b0',
            // contrastText: '#000',
        },
        secondary: {
            // light: '#55b96e',
            main: '#FCFFFC',
            // dark: '#1e7533',
            // contrastText: '#fff',
        }
    }, typography: {
        useNextVariants: true,
    }
});

function isURL(str) {
    var regex = /(((http|https):\/\/)|www.)(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
    var pattern = new RegExp(regex);
    return pattern.test(str);
}

const getWindowDimensions = () => {
    const {innerWidth: width, innerHeight: height} = window;
    return {
        width,
        height
    };
};

const getSizeType = () => {
    let width = getWindowDimensions().width;

    if (width <= 599) {
        return 'xs';
    } else if (width >= 600 && width <= 959) {
        return 'sm';
    } else if (width >= 960 && width <= 1279) {
        return 'md';
    } else if (width >= 1280 && width <= 1919) {
        return 'lg';
    } else if (width >= 1920 && width <= 5000) {
        return 'xl';
    }
};

const mainContainerRouter = (route, parameters) => {
    switch (route) {
        case 'home':
            return <HomeComponent/>;
        case 'login':
            return <LoginComponent/>;
        case 'signup':
            return <SignUpComponent/>;
        case 'search':
            return <SearchResultsComponent/>;
        case 'categories':
            return <CategoriesComponent/>;
        case 'proform':
            return <ProFormComponent/>;
        case 'profile':
            return <ProfileComponent/>;
        default:
            return <HomeComponent/>;
    }
};

const validateEmail = (values, errors = {}) => {
    if (!values.email) {
        errors.email = 'L\'email est requis';
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
    ) {
        errors.email = 'Adresse e-mail invalide.';
    }
    return errors;
};

const validatePass = (values, errors = {}) => {
    if (!values.password) {
        errors.password = 'Mot de pass est requis.';
    } else if (values.password.length < 8) {
        errors.password = 'Le mot de passe doit comporter au moins 8 caractères.';
    }
    return errors;
};

const validateNewPass = (values, errors = {}) => {
    if (!values.newPassword) {
        errors.newPassword = 'Nouvelle mot de pass est requis.';
    } else if (values.password === values.newPassword) {
        errors.newPassword = 'Ne doit pas être le même que le mot de passe.';
    } else if (values.newPassword.length < 8) {
        errors.newPassword = 'Le mot de passe doit comporter au moins 8 caractères.';
    }
    return errors;
};

const isEqual = (objOne: any, objTwo: any) => {
    return Object.values(objOne).toString() === Object.values(objTwo).toString();
};

const validatePassTwo = (values, errors = {}) => {
    if (!values.password2) {
        errors.password2 = 'Doit être le même que le mot de passe.';
    } else if (values.password !== values.password2) {
        errors.password2 = 'Doit être le même que le mot de passe.';
    }
    return errors;
};

const getBusinessFeeRate = (rate) => {
    switch (rate) {
        case "C":
            return "Consultation";
        case "H":
            return "Horaire";
        case "F":
            return "Forfaitaire";
        case "Q":
            return "Quotas";
        case "J":
            return "Journalier";
        default:
            return "Négociable";
    }
};

const validateName = (values, errors = {}) => {
    if (!values.name) {
        errors.name = 'Nom est requis.';
    }
    return errors;
};

const validateLocation = (values, errors = {}) => {
    if (!values.locString) {
        errors.locString = 'Emplacement est requis.';
    }
    return errors;
};

const validateProName = (values, errors = {}) => {
    if (!values.businessName) {
        errors.businessName = 'Nom est requis.';
    }
    return errors;
};

const validateCategoryId = (values, errors = {}) => {
    if (!values.categoryId) {
        errors.categoryId = 'La catégorie est requis.';
    }
    return errors;
};

const validateLastName = (values, errors = {}) => {
    if (!values.lastName) {
        errors.lastName = 'Prénom est requis.';
    }
    return errors;
};

const validatePhone = (values, errors = {}) => {
    if (!values.phone) {
        errors.phone = 'Numéro de téléphone est requis.';
    } else if (values.phone.length < 8) {
        errors.phone = 'Le numéro de téléphone doit comporter au moins 8 caractères.';
    }
    return errors;
};

const validateProPhone = (values, errors = {}) => {
    if (!values.businessPhone) {
        errors.businessPhone = 'Numéro de téléphone est requis.';
    } else if (values.businessPhone.length !== 8) {
        errors.businessPhone = 'Le numéro de téléphone doit comporter au moins 8 caractères.';
    }
    return errors;
};

const validateIntro = (values, errors = {}) => {
    if (!values.introduction) {
        errors.introduction = 'L\'introduction est requis.';
    } else if (values.introduction.length < 80) {
        errors.introduction = 'L\'introduction  doit comporter au moins 80 caractères.';
    }
    return errors;
};

const validateWorkingDays = (values, errors = {}) => {
    if ((!values.workingDaysStarting && values.workingDaysEnding) || (values.workingDaysStarting && !values.workingDaysEnding)) {
        errors.workingDaysStarting = 'Vous devez remplir les deux jours ouvrables de début et de fin ou aucun d\'entre eux';
        errors.workingDaysEnding = 'Vous devez remplir les deux jours ouvrables de début et de fin ou aucun d\'entre eux';
    }

    return errors;
};

const validateLinks = (values, errors = {}) => {
    if (values.internetLinks) {
        let urlsArray = values.internetLinks.split(",");
        urlsArray.map((url, index) => {
            if (!isURL(url)) {
                return errors.internetLinks = "Vous devez entrer des urls valides";
            }
        });
    }
    return errors;
};

const validateBusinessFee = (values, errors = {}) => {
    if ((!values.businessFee && values.businessFeeRate) || (values.businessFee && !values.businessFeeRate)) {
        errors.businessFee = 'Vous devez remplir à la fois les frais et le type de frais ou aucun d\'entre eux';
        errors.businessFeeRate = 'Vous devez remplir à la fois les frais et le type de frais ou aucun d\'entre eux';
    }

    return errors;
};

const validateWorkingHours = (values, errors = {}) => {
    if ((!values.workingHoursEnding && values.workingHoursStarting) || (values.workingHoursEnding && !values.workingHoursStarting)) {
        errors.workingHoursEnding = 'Vous devez remplir à la fois les deux heures de travail ou aucun d\'entre eux';
        errors.workingHoursStarting = 'Vous devez remplir à la fois les deux heures de travail ou aucun d\'entre eux';
    }

    return errors;
};

const validateForm = (values) => {
    let errors = {};
    errors = validateEmail(values, errors);
    errors = validatePass(values, errors);
    // errors = validatePhone(values, errors);
    errors = validatePassTwo(values, errors);
    errors = validateLastName(values, errors);
    errors = validateName(values, errors);

    return errors;
};

const validateProForm = (values) => {
    let errors = {};
    errors = validateProName(values, errors);
    errors = validateProPhone(values, errors);
    // errors = validateBusinessFee(values, errors);
    errors = validateWorkingDays(values, errors);
    errors = validateWorkingHours(values, errors);
    errors = validateLinks(values, errors);
    errors = validateIntro(values, errors);
    errors = validateCategoryId(values, errors);
    return errors;
};

const validateUpdateForm = (values) => {
    let errors = {};
    errors = validateEmail(values, errors);
    // errors = validatePhone(values, errors);
    errors = validateLastName(values, errors);
    errors = validateName(values, errors);
    return errors;
};

const validateUpdatePass = (values) => {
    let errors = {};
    errors = validatePass(values, errors);
    errors = validateNewPass(values, errors);
    return errors;
};

const validateLoginForm = (values) => {
    let errors = {};

    errors = validateEmail(values, errors);
    errors = validatePass(values, errors);

    return errors;
};

const adminRouter = (route, parameters) => {
    switch (route) {
        case 'users':
            return <UsersListComponent parameters={parameters}/>;
        case 'pros':
            return <ProsListComponent parameters={parameters}/>;
        case 'personalProfile':
            return <OverviewComponent parameters={parameters}/>;
        case 'categories':
            return <CategoriesListComponent parameters={parameters}/>;
        case 'params':
            return <ParametersComponent parameters={parameters}/>;
        default:
            return <OverviewComponent parameters={parameters}/>;
    }
};

const searchCategory = (cats, _id) => {
    for (let i = 0; i < cats.length; i++) {
        if (cats[i]._id === _id) {
            return true;
        }
    }
    return false;
};

const reorderCategories = (array = []) => {
    let newArray = [];
    let parentsList = [];
    array.forEach((cat, index) => {
        if (cat.parentId && !searchCategory(parentsList, cat.parentId._id))
            parentsList.push(cat.parentId)
    });
    if (parentsList.length > 0)
        parentsList.forEach((cat, index) => {
            newArray.push({...cat, categories: []});
            array.forEach((_cat, idx) => {
                if (_cat.parentId && _cat.parentId._id === cat._id)
                    newArray[index].categories.push(_cat);
                // reordering categories based on the order number
                newArray[index].categories = newArray[index].categories.sort((x, y) => x.order - y.order)
            });
        });

    // reordering categories based on the order number
    return {newArray: newArray.sort((x, y) => x.order - y.order), parentsList: parentsList.sort((x, y) => x.order - y.order)};
};

const reorderProsForHome = (array = []) => {
    let newArray = [];
    let categoriesList = [];

    array.forEach((pro, index) => {
       if (pro.categoryId && !searchCategory(categoriesList, pro.categoryId._id))
           categoriesList.push(pro.categoryId);
    });

    if (categoriesList.length > 0)
        categoriesList.forEach((cat, idx) => {
           newArray.push({...cat, pros: []});
           array.forEach((pro, _i) => {
               if (pro.categoryId && (pro.categoryId._id === cat._id))
                   newArray[idx].pros.push(pro);
           })
        })
    // console.log(categoriesList)
    return newArray;
};

const rateParser = (rateString) => {
    switch (rateString) {
        case "ones":
            return 1;
        case "twos":
            return 2;
        case "threes":
            return 3;
        case "fours":
            return 4;
        case "fives":
            return 5;
    }
};

const headerServices = [
    {
        name: 'Maison',
        icon: homeIcon,
        order: 0
    },
    {
        name: 'Bien-être',
        icon: heartIcon,
        order: 1
    },
    {
        name: 'Affaires',
        icon: portfolioIcon,
        order: 2
    },
    {
        name: 'Evénements',
        icon: eventsIcon,
        order: 3
    },
    {
        name: 'Formation',
        icon: bookIcon,
        order: 4
    }
];

var ServicesRenderer = () => {
    return headerServices.sort((x, y) => x.order - y.order).map((service, index) => {
        return <div key={index} className="serviceIcon">
            <Link to={"/categories?q=" + service.name}>
                <Button>
                    <div className="buttonContent">
                        <img src={service.icon} alt="events icon"/>
                        <span>{service.name}</span>
                    </div>
                </Button>
            </Link>
        </div>
    })
};

export default {
    getWindowDimensions,
    getSizeType,
    adminRouter,
    THEME,
    mainContainerRouter,
    validateForm,
    FORM_THEME,
    validateLoginForm,
    validateUpdateForm,
    validateUpdatePass,
    validateProForm,
    isEqual,
    getBusinessFeeRate,
    reorderCategories,
    reorderProsForHome,
    rateParser,
    headerServices,
    ServicesRenderer
};
