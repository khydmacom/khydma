import React, {Component} from 'react';
import './App.css';
import {HomeComponent, SignUpComponent} from "./views";
import {Router, Redirect, Link} from "@reach/router";
import * as PropTypes from 'prop-types';
import {
    AdminDashboardComponent,
    CategoriesComponent,
    LoginComponent, MainContainerComponent,
    ProfileComponent,
    ProFormComponent,
    SearchResultsComponent
} from "./views/containers";
import {
    CategoriesListComponent,
    OverviewComponent, ParametersComponent,
    ProsListComponent,
    UsersListComponent
} from "./views/containers/adminDashboard/components";
import LoginAdminComponent from "./views/containers/loginAdmin/loginAdminComponent";
import {Provider, connect} from "react-redux";
import {reduxStore} from "./redux";
import DashboardComponent from "./views/containers/dashboard/dashboardComponent";
import {
    ConversationsComponent,
    NotificationsComponent,
    PersonalProfileComponent
} from "./views/containers/dashboard/components";
import GalleryDashProComponent from "./views/containers/dashboard/components/galleryDashPro/galleryDashProComponent";
import EditProfileProComponent from "./views/containers/dashboard/components/editProfilePro/editProfileProComponent";
import ConversationsProComponent
    from "./views/containers/dashboard/components/conversationsPro/conversationsProComponent";
import MetricsComponent from "./views/containers/dashboard/components/metrics/metricsComponent";
import ReviewsProComponent from "./views/containers/dashboard/components/reviewsPro/reviewsProComponent";
import {AuthActions} from './views/reducers/auth';
import {LocationsActions} from './views/reducers/locations';
import {usersActions} from './views/reducers/users';
import {RootsActions} from './views/reducers/roots';
import {CurrentUsersActions} from './views/reducers/currentUsers';
import {CONST} from "./config";
import {SocketServices} from './config/services';
import PubSubJs from 'pubsub-js';
import fullLogo from './assets/icons/final/full.png';
import {Button} from "@material-ui/core";
import {categoriesActions} from "./views/reducers/categories";
import {geolocated, geoPropTypes} from "react-geolocated";
import {prosActions} from "./views/reducers/pros";

const {getToken, reAuth} = AuthActions;
const {updateRoot} = RootsActions;
const {handleGeoCodingRequest} = LocationsActions;
const {updateUser} = usersActions;
const {checkIfCurrentIsPro, checkIfCurrentIsAdmin, getCurrentProfile, getCurrentUser} = CurrentUsersActions;
const {getCategories} = categoriesActions;
const {getProMe} = prosActions;


const {localStorageIndexes, pubSubConstants} = CONST;
const _SocketServices = SocketServices;

///////////////////////////////////////////////////////////////////////////
/////////////////////////// routes guards! ////////////////////////////////
///////////////////////////////////////////////////////////////////////////

class NotFoundRoute extends Component {

    render() {
        return (
            <div className="mainNotFound">
                <img src={fullLogo} alt={"Khydma.com logo"} className="mainNotFoundLogo"/>
                <h1 className="mainNotFoundPhrase">
                    Vous êtes peut-être trompé de chemin, retournez à la page d'accueil ?
                </h1>
                <Link to={"/"}>
                    <Button variant="contained" color="secondary">
                        Page d'accueil
                    </Button>
                </Link>
            </div>
        )
    }
}

class ProRoute extends Component {
    render() {
        const {isPro, children} = this.props;

        return isPro ? children : <Redirect to="/" noThrow/>;
    }
}

ProRoute.propTypes = {
    isPro: PropTypes.bool
};

class NotProRoute extends Component {
    render() {
        const {isPro, children} = this.props;

        return !isPro ? children : <Redirect to="/" noThrow/>;
    }
}

NotProRoute.propTypes = {
    isPro: PropTypes.bool
};

class AdminRoute extends Component {
    render() {
        const {isAdmin, children} = this.props;
        // console.log('AdminRoute = ', isAdmin);
        return isAdmin ? children : <Redirect to="/" noThrow/>;
    }
}

AdminRoute.propTypes = {
    isAdmin: PropTypes.bool
};

class LoggedInRoute extends Component {
    render() {
        const {isLoggedIn, children} = this.props;

        return isLoggedIn ? children : <Redirect to="/" noThrow/>;
    }
}

LoggedInRoute.propTypes = {
    isLoggedIn: PropTypes.bool
};

class NotLoggedInRoute extends Component {
    render() {
        const {isLoggedIn, children, pathToGetBackTo} = this.props;

        return !isLoggedIn ? children :
            <Redirect to={pathToGetBackTo ? pathToGetBackTo : '/'} noThrow/>;
    }
}

NotLoggedInRoute.propTypes = {
    isLoggedIn: PropTypes.bool,
    pathToGetBackTo: PropTypes.string
};


// we created this app container to redux all over the app and use
// the auth guards (LoggedIn|Admin|Pro)Route to handle user redirection based on its state

class AppContainer extends Component {

    constructor(props) {
        super(props);

        this.state = {
            loaded: false,
            proformTitle: '',
            proformLocString: '',
            proformLoc: [],
            updatedGeoLoc: false
        };

        PubSubJs.subscribe('loadedAll', this.setLoadedState);
    }

    isGetProMeSuccess(result) {
        // console.log(result);
    }

    isGetProMeError(err) {
        console.log(err)
    };

    getCurrentUserProfile() {
        this.props.getCurrentProfile((profile) => {
            console.log("app. get user profile = ", profile);
            if (profile.isPro)
                this.props.getProMe(this.isGetProMeSuccess, this.isGetProMeError, {userId: profile._id});
            // connect user to socket for real-time events
            _SocketServices.prototype.connectSocket(profile._id, () => {
                console.log(profile._id + " connected!");
                PubSubJs.publish(pubSubConstants.subscribeSockets);
            });
        }, (err) => {
            console.log("err getting current profile = ", err);
        })
    }

    componentDidCatch(error, info) {
        // You can also log the error to an error reporting service
        console.log(error, info);
    }

    componentDidMount(): void {
        this.props.getCategories(this.isGetCatsSuccess, this.isGetCatsError);
        // setTimeout(() => {console.log(this.props.geoCodingResults)}, 666);
    }

    isGetCatsSuccess = (succ) => {
        // console.log(succ);
    };

    isGetCatsError = (err) => {
        // console.log(succ);
    };

    setGeoResults(latLng: { lat: number, lng: number }, geoRes: {
        address: {
            city: string, country: string,
            country_code: string, county: string,
            postcode: number, state: string,
            state_district: string
        },
        addresstype: string,
        category: string, display_name: string,
        importance: string, name: any,
        osm_id: number, osm_type: string,
        place_id: number, place_rank: number,
        type: string
    }) {

        if (geoRes) {
            console.log(geoRes);
            let shortAddress = geoRes.address.state + ', ' + (geoRes.address.city ? geoRes.address.city : (geoRes.address.state_district ? geoRes.address.state_district : geoRes.address.county));
            this.setState({
                proformLoc: [Number(latLng.lng), Number(latLng.lat)],
                proformLocString: '',
                proformTitle: geoRes.address.state
            });
            if (this.props.user && (shortAddress !== this.props.user.locString)) {
                this.props.updateUser({
                    _id: this.props.user._id,
                    loc: [Number(latLng.lng), Number(latLng.lat)],
                    locString: shortAddress
                }, (res) => {
                    // console.log('update loc geores maincontainer = ', res);
                }, (err) => {
                    // console.log('update loc er = ', err)
                })
            }
        }
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        // if (nextProps.coords !== this.props.coords) {
        if (this.props.isGeolocationEnabled && this.props.isGeolocationAvailable && !this.state.updatedGeoLoc) {
            if (this.props.coords) {
                console.log('hello');

                const location = {
                    lat: this.props.coords.latitude,
                    lng: this.props.coords.longitude
                };
                this.props.handleGeoCodingRequest(location, (res) => {
                    this.setGeoResults(location, res);
                    this.setState({
                        updatedGeoLoc: true
                    })
                });
            }
            // }
        }
    }

    checkAdmin() {
        this.props.checkIfCurrentIsAdmin((result) => {
            // console.log(result);

            this.props.reAuth((res) => {
                return res;
            })
        }, (err: { message: string, errCode: number }) => {
            // console.log(err)
            if (err.errCode === 500 || err.errCode === 401) {
                console.log(err);
                return null;
            }
        });
    }

    checkLoggedIn() {
        this.props.getCurrentUser((res) => {
            if (res) {
                this.props.reAuth((user) => {
                    this.getCurrentUserProfile()
                });
            }
        }, (err: { message: string, errCode: number }) => {
            if (err.errCode === 401) {
                if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
                    this.props.getToken({refreshToken: JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.refreshToken},
                        (user) => {
                            this.getCurrentUserProfile()
                        },
                        (err) => {
                            console.log("getToken err = ", err);
                        })
            }
        })
    }

    componentWillMount(): void {
        this.props.checkIfCurrentIsPro((succ) => {
            // console.log('is pro = ', succ)
        }, (err) => {
            console.log(err);
            // this.openSnackBar(err, "error");
        });
        this.checkLoggedIn();
        this.checkAdmin();
    }

    setLoadedState = () => {
        this.setState({
            loaded: true
        });
    };

    render() {
        const {isLoggedIn, isAdmin, isPro, pathToGetBackTo} = this.props;
        const {proformLoc, proformLocString, proformTitle} = this.state;

        return this.state.loaded ? (
            <Router>
                <MainContainerComponent path="/">
                    <HomeComponent path="/"/>
                    <NotLoggedInRoute path="login" isLoggedIn={isLoggedIn}
                                      pathToGetBackTo={pathToGetBackTo}>
                        <LoginComponent path="/"/>
                    </NotLoggedInRoute>
                    <NotLoggedInRoute path="signup" isLoggedIn={isLoggedIn}
                                      pathToGetBackTo={pathToGetBackTo}>
                        <SignUpComponent path="/"/>
                    </NotLoggedInRoute>
                    <SearchResultsComponent path="search"/>
                    <CategoriesComponent path="categories"/>
                    <ProfileComponent path="profile/:proId" isLoggedIn={isLoggedIn}/>
                    <NotProRoute path="proform">
                        <ProFormComponent path="/" loc={proformLoc} locString={proformLocString}
                                          locationTitle={proformTitle}/>
                    </NotProRoute>
                    <DashboardComponent path="dashboard">
                        <LoggedInRoute path="/" isLoggedIn={isLoggedIn}>
                            <PersonalProfileComponent path="/"/>
                            <NotFoundRoute default/>
                        </LoggedInRoute>
                        <LoggedInRoute path="profile" isLoggedIn={isLoggedIn}>
                            <PersonalProfileComponent path="/"/>
                            <NotFoundRoute default/>
                        </LoggedInRoute>
                        <LoggedInRoute path="conversations" isLoggedIn={isLoggedIn}>
                            <ConversationsComponent path="/"/>
                            <NotFoundRoute default/>
                        </LoggedInRoute>
                        <LoggedInRoute path="notifications" isLoggedIn={isLoggedIn}>
                            <NotificationsComponent path="/"/>
                            <NotFoundRoute default/>
                        </LoggedInRoute>
                        <ProRoute path="gallery" isPro={isPro}>
                            <GalleryDashProComponent path="/"/>
                            <NotFoundRoute default/>
                        </ProRoute>
                        <ProRoute path="pro" isPro={isPro}>
                            <EditProfileProComponent path="/"/>
                            <NotFoundRoute default/>
                        </ProRoute>
                        <ProRoute path="conversations-pro" isPro={isPro}>
                            <ConversationsProComponent path="/"/>
                            <NotFoundRoute default/>
                        </ProRoute>
                        <ProRoute path="metrics" isPro={isPro}>
                            <MetricsComponent path="/"/>
                            <NotFoundRoute default/>
                        </ProRoute>
                        <ProRoute path="reviews" isPro={isPro}>
                            <ReviewsProComponent path="/"/>
                            <NotFoundRoute default/>
                        </ProRoute>
                        <NotFoundRoute default/>
                    </DashboardComponent>
                    <NotFoundRoute default/>
                </MainContainerComponent>
                <AdminDashboardComponent path="admin">
                    <AdminRoute path="/" isAdmin={isAdmin}>
                        <OverviewComponent path="/"/>
                        <NotFoundRoute default/>
                    </AdminRoute>
                    <AdminRoute path="users" isAdmin={isAdmin}>
                        <UsersListComponent path="/"/>
                        <NotFoundRoute default/>
                    </AdminRoute>
                    <AdminRoute path="pros" isAdmin={isAdmin}>
                        <ProsListComponent path="/"/>
                        <NotFoundRoute default/>
                    </AdminRoute>
                    <AdminRoute path="categories" isAdmin={isAdmin}>
                        <CategoriesListComponent path="/"/>
                        <NotFoundRoute default/>
                    </AdminRoute>
                    <AdminRoute path="params" isAdmin={isAdmin}>
                        <ParametersComponent path="/"/>
                        <NotFoundRoute default/>
                    </AdminRoute>
                </AdminDashboardComponent>
                <NotLoggedInRoute path="admin-login" isLoggedIn={isLoggedIn}>
                    <LoginAdminComponent path="/"/>
                    <NotFoundRoute default/>
                </NotLoggedInRoute>
            </Router>
        ) : <div>Loading...</div>;
    }
}

let setStatesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer && state.locationsReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            isAdmin: state.currentUsersReducer.isAdmin,
            isPro: state.currentUsersReducer.isPro,
            user: state.currentUsersReducer.user,
            pathToGetBackTo: state.rootsReducer.pathToGetBackTo,
            geoCodingResults: state.locationsReducer.geoCodingResults,
            osmAutocomplete: state.locationsReducer.osmAutocomplete
        }
    } else
        return {}
};

AppContainer.propTypes = {
    ...geoPropTypes
};


let ConnectedAppContainer = connect(setStatesToProps, {
    checkIfCurrentIsPro, checkIfCurrentIsAdmin,
    getCurrentProfile, getCurrentUser, getToken, updateRoot,
    reAuth, getCategories, updateUser, handleGeoCodingRequest,
    getProMe
})(geolocated({
    positionOptions: {
        enableHighAccuracy: false,
    },
    userDecisionTimeout: 5000,
})(AppContainer));


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            showProgressPage: false
        }
    }

    render() {
        return (
            <Provider store={reduxStore}>
                <ConnectedAppContainer/>
            </Provider>
        );
    }
}

export default App;
