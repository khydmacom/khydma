import {SIGNED_IN, SIGNED_OUT, TOKEN} from './actionTypes';
import {CONST} from "../../../config";
const {localStorageIndexes} = CONST;

let initialState = {isLoggedIn: false, user: null};

export const authReducer = (state = initialState, action) => {
    switch (action.type) {
        case SIGNED_IN:
            const user = action.data;
            localStorage.setItem(localStorageIndexes.user, JSON.stringify(user));

            return {...state, isLoggedIn: true, user};
        case SIGNED_OUT:
            localStorage.removeItem(localStorageIndexes.user);

            return {...state, isLoggedIn: false, user: null};
        case TOKEN:
            const _user = localStorage.getItem(localStorageIndexes.user) ? JSON.parse(localStorage.getItem(localStorageIndexes.user)) : null;

            if (_user) {
                _user.token.accessToken = action.data.token.accessToken;

                localStorage.setItem(localStorageIndexes.user, JSON.stringify(_user));
                return {...state, isLoggedIn: true, user: _user};
            } else return {...state, isLoggedIn: false, user: null};
        default:
            return state;
    }
};
