import {AuthService} from '../../../config/services';

const {signUp, rejectToken, login, getToken} = AuthService;

function signUpWithEmailAndPassword(body: { email: string, name: string, lastName: string, phone: number, password: string }, cb) {
    signUp(body).then((data: {data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object}) => {
        if (data.status === 204)
            cb(false, null, {message: 'S\'il vous plaît entrer tous les champs'});
        else if (data.status === 205)
            cb(false, null, {message: 'L\'email existe déjà'});
        else if (data.status === 207)
            cb(false, null, {message: 'Le numéro de téléphone existe déjà'});
        else cb(true, data.data, null);
    }).catch((err) => {
        cb(false, null, {message: err});
    });
}

function loginWithEmailAndPassword(body: { email: string, password: string }, cb) {
    login(body).then((data: {data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object}) => {
        if (data.status >= 204)
            cb(false, null, {message: data.data.message ? data.data.message : data.statusText});
        else cb(true, data.data, null);
    }).catch((err) => {
        cb(false, null, {message: err});
    });
}

function getUserToken(body: { refreshToken: string }, cb) {
    getToken(body).then((data: {data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object}) => {
        if (data.status >= 204)
            cb(false, null, {message: data.data.message ? data.data.message : data.statusText});
        else cb(true, data.data, null);
    }).catch((err) => {
        cb(false, null, {message: err});
    });
}

function rejectUserToken(body: { refreshToken: string }, cb) {
    rejectToken(body).then((data: {data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object}) => {
        if (data.status > 204)
            cb(false, null, {message: data.data.message ? data.data.message : data.statusText});
        else cb(true, data.data, null);
    }).catch((err) => {
        cb(false, null, {message: err});
    });
}

export default {
    signUpWithEmailAndPassword,
    loginWithEmailAndPassword,
    getUserToken,
    rejectUserToken
};
