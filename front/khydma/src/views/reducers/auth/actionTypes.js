export const SIGNED_IN = 'auth/SIGNED_IN';
export const SIGNED_OUT = 'auth/SIGNED_OUT';
export const TOKEN = 'auth/TOKEN';
