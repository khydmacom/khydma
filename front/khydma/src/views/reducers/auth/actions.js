import {SIGNED_IN, SIGNED_OUT, TOKEN} from './actionTypes';
import AuthApi from './authApi';
import {CONST} from '../../../config';
const {pubSubConstants, localStorageIndexes} = CONST;

const pubSubJs = require('pubsub-js');

export function signUp(body: { email: string, name: string, lastName: string, phone: number, password: string }, sucCb, errCb) {
    return (dispatch) => {
        AuthApi.signUpWithEmailAndPassword(body, (success, user, errMessage) => {
            if (success) {
                dispatch({type: SIGNED_IN, data: user});

                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function login(body: { email: string, password: string }, sucCb, errCb) {
    return (dispatch) => {
        AuthApi.loginWithEmailAndPassword(body, (success, user, errMessage) => {
            if (success) {
                dispatch({type: SIGNED_IN, data: user});

                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function getToken(body: { refreshToken: string }, sucCb, errCb) {
    return (dispatch) => {
        AuthApi.getUserToken(body, (success, data, errMessage) => {
            if (success) {
                dispatch({type: TOKEN, data: data});

                sucCb(data);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function rejectToken(body: { refreshToken: string }, sucCb, errCb) {
    return (dispatch) => {
        AuthApi.rejectUserToken(body, (success, data, errMessage) => {
            if (success) {
                dispatch({type: SIGNED_OUT, data: data});

                sucCb(data);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function reAuth(sucCb) {
    return (dispatch) => {
        const user = JSON.parse(localStorage.getItem(localStorageIndexes.user));

        dispatch({type: SIGNED_IN, data: user});
        sucCb(user);
    };
}
