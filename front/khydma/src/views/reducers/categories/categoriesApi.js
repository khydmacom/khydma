import {CategoriesServices} from '../../../config/services';

const {getOneCategory, updateCategory, getCategories, deleteCategory, addCategory, updateCategoryImage} = CategoriesServices;

function getCategoryById(_id, cb) {
    getOneCategory(_id).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}
function getAllCategories(cb) {
    getCategories().then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function addSingleCategory(body, cb) {
    addCategory(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function updateSingleCategory(body, cb) {
    updateCategory(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function updateSingleCategoryImage(body, progressCb, cb) {
    updateCategoryImage(body, (progress) => {
        progressCb(progress)
    }).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function removeCategory(body: {"_id": string}, cb) {
    deleteCategory(body._id).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}


export default {
    addSingleCategory,
    getAllCategories,
    getCategoryById,
    removeCategory,
    updateSingleCategory,
    updateSingleCategoryImage
};
