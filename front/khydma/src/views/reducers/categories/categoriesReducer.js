import {CATEGORY_DELETED, GOT_ALL_CATEGORIES, GOT_ID_CATEGORY, NEW_CATEGORY, UPDATED_CATEGORY} from './actionTypes';
import {CONST} from "../../../config";

let initialState = {categoryById: null, allCategories: []};

export const categoriesReducer = (state = initialState, action) => {
    switch (action.type) {
        case GOT_ALL_CATEGORIES:
            return {...state, allCategories: action.data};
        case GOT_ID_CATEGORY:
            const categoryById = action.data;

            return {...state, categoryById};
        case NEW_CATEGORY:
            let allCategories = state.allCategories;
            // console.log("NEW_CATEGORY = ", allCategories);
            allCategories.push(action.data);
            return {...state, allCategories: allCategories};
        case UPDATED_CATEGORY:
            let elIndex = state.allCategories.findIndex((x) => {return x._id === action.data._id});
            let cats = state.allCategories;
            cats.splice(elIndex, 1, action.data);
            return {...state, allCategories: cats};
        default:
            return state;
    }
};
