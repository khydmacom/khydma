import {categoriesReducer} from './categoriesReducer';
import * as categoriesActions from './actions';

export {
    categoriesReducer,
    categoriesActions
}
