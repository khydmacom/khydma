import {CATEGORY_DELETED, GOT_ALL_CATEGORIES, GOT_ID_CATEGORY, NEW_CATEGORY, UPDATED_CATEGORY} from './actionTypes';

import CategoriesApi from './categoriesApi';

export function getCategories(sucCb, errCb) {
    return (dispatch) => {
        CategoriesApi.getAllCategories((success, category, errMessage) => {
            if (success) {
                dispatch({type: GOT_ALL_CATEGORIES, data: category});
                sucCb(category);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function getCategoryById(_id, sucCb, errCb) {
    return (dispatch) => {
        CategoriesApi.getCategoryById(_id, (success, category, errMessage) => {
            if (success) {
                dispatch({type: GOT_ID_CATEGORY, data: category});
                sucCb(category);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function newCategory(body, sucCb, errCb) {
    return (dispatch) => {
        CategoriesApi.addSingleCategory(body,(success, category, errMessage) => {
            if (success) {
                dispatch({type: NEW_CATEGORY, data: category});
                sucCb(category);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function updateCategory(body, sucCb, errCb) {
    return (dispatch) => {
        CategoriesApi.updateSingleCategory(body,(success, category, errMessage) => {
            if (success) {
                dispatch({type: UPDATED_CATEGORY, data: category});
                sucCb(category);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function updateCategoryImage(body, prCb, sucCb, errCb) {
    return (dispatch) => {
        CategoriesApi.updateSingleCategoryImage(body,(progress) => {
            prCb(progress)
        }, (success, category, errMessage) => {
            if (success) {
                dispatch({type: UPDATED_CATEGORY, data: category});
                sucCb(category);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function removeCategory(body, sucCb, errCb) {
    return (dispatch) => {
        CategoriesApi.removeCategory(body,(success, category, errMessage) => {
            if (success) {
                dispatch({type: CATEGORY_DELETED, data: category});
                sucCb(category);
            } else if (errMessage) errCb(errMessage);
        });
    };
}
