export const GOT_ALL_CATEGORIES = 'categories/GOT_ALL_CATEGORIES';
export const UPDATED_CATEGORY = 'categories/UPDATED_CATEGORY';
export const NEW_CATEGORY = 'categories/NEW_CATEGORY';
export const GOT_ID_CATEGORY = 'categories/GOT_ID_CATEGORY';
export const CATEGORY_DELETED = 'categories/CATEGORY_DELETED';

// export const SIGNED_OUT = 'auth/SIGNED_OUT';
// export const TOKEN = 'auth/TOKEN';
