import {UPDATE_PASSWORD, USER_DELETED, GOT_ID_USER, NEW_USER_DATA, GOT_ALL_USERS} from './actionTypes';
import {CONST} from "../../../config";

let initialState = {userById: null, allUsers: null, user: null};

export const usersReducer = (state = initialState, action) => {
    switch (action.type) {
        case GOT_ALL_USERS:
            const allUsers = action.data;
            return {...state, allUsers};
        case GOT_ID_USER:
            const userById = action.data;
            return {...state, userById};
        case NEW_USER_DATA:
            return {...state, user: action.data};
        case UPDATE_PASSWORD:
            return {...state, user: action.data};
        default:
            return state;
    }
};
