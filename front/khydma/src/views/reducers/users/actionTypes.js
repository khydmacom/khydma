export const GOT_ALL_USERS = 'users/GOT_ALL_USERS';
export const NEW_USER_DATA = 'users/NEW_USER_DATA';
export const UPDATE_PASSWORD = 'users/UPDATE_PASSWORD';
export const GOT_ID_USER = 'users/GOT_ID_USER';
export const USER_DELETED = 'users/USER_DELETED';

// export const SIGNED_OUT = 'auth/SIGNED_OUT';
// export const TOKEN = 'auth/TOKEN';
