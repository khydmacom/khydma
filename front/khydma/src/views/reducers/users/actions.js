import {GOT_ALL_USERS, NEW_USER_DATA, UPDATE_PASSWORD, GOT_ID_USER, USER_DELETED} from './actionTypes';

import UsersApi from './userApi';

export function getUserById(_id, sucCb, errCb) {
    return (dispatch) => {
        UsersApi.getUserById(_id, (success, user, errMessage) => {
            if (success) {
                dispatch({type: GOT_ID_USER, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function updateUser(body, sucCb, errCb) {
    return (dispatch) => {
        UsersApi.updateUser(body,(success, user, errMessage) => {
            if (success) {
                dispatch({type: NEW_USER_DATA, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function updateUserPassword(body, sucCb, errCb) {
    return (dispatch) => {
        UsersApi.updateUserPassword(body,(success, user, errMessage) => {
            if (success) {
                dispatch({type: UPDATE_PASSWORD, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function updateUserImage(body, prCb, sucCb, errCb) {
    return (dispatch) => {
        UsersApi.updateSingleUsersImage(body, (progress) => {
            prCb(progress)
        }, (success, user, errMessage) => {
            if (success) {
                dispatch({type: NEW_USER_DATA, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function removeUser(body, sucCb, errCb) {
    return (dispatch) => {
        UsersApi.removeUser(body,(success, user, errMessage) => {
            if (success) {
                dispatch({type: USER_DELETED, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}
