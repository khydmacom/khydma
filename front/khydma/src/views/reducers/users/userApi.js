import {UsersServices} from '../../../config/services';

const {updateUserImage, remove, updatePassword, update, getUser} = UsersServices;

function getUserById(_id, cb) {
    getUser(_id).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function updateUser(body, cb) {
    update(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function updateUserPassword(body: {"email": string, "password": string, "newPassword": string, "_id": string}, cb) {
    updatePassword(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function removeUser(body: {"_id": string}, cb) {
    remove(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function updateSingleUsersImage(body, progressCb, cb) {
    updateUserImage(body, (progress) => {
        progressCb(progress)
    }).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}


export default {
    removeUser,
    getUserById,
    updateUser,
    updateUserPassword,
    updateSingleUsersImage
};
