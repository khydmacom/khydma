import {ProsServices, ReviewsServices} from '../../../config/services';

// const {getIndexedPros, getOnePro, updatePro, getPros, deletePro, addPro, updateProImage, searchPros, deleteGalleryImages, getOneGalleryImage, getGalleryImages, addProCategoryImage} = ProsServices;

function getIndexedProByQuery(query = {}, cb) {
    ProsServices.getIndexedPros(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service getUSer = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getProByQuery(query = {}, cb) {
    ProsServices.getOnePro(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service getUSer = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getReviewByQuery(query = {}, cb) {
    ReviewsServices.getOneReview(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service getUSer = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function searchAllPros(query = {}, cb) {
    console.log(query);
    ProsServices.searchPros(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service searchAll = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getAllProsByQuery(query = {}, cb) {
    ProsServices.getPros(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service getUSer = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getAllReviewsByQuery(query = {}, cb) {
    ReviewsServices.getReviews(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service getUSer = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getAllReviewsStatsByQuery(query = {}, cb) {
    ReviewsServices.getReviewsStats(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service getUSer = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function addSinglePro(body, cb) {
    ProsServices.addPro(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function addSingleReview(body, cb) {
    ReviewsServices.addReview(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function deleteGalleryImage(query, cb) {
    ProsServices.deleteGalleryImages(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function deleteReview(body, cb) {
    ReviewsServices.deleteReview(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getProGallery(query, cb) {
    ProsServices.getGalleryImages(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getProGalleryOneImage(query, cb) {
    ProsServices.getOneGalleryImage(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function updateSinglePro(body, cb) {
    ProsServices.updatePro(body).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function updateSingleProImage(body, progressCb, cb) {
    ProsServices.updateProImage(body, (progress) => {
        progressCb(progress)
    }).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function addGalleryImage(body, progressCb, cb) {
    ProsServices.addProGalleryImage(body, (progress) => {
        progressCb(progress)
    }).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePro = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function removePro(body: {"_id": string}, cb) {
    // console.log(body);
    ProsServices.deletePro(body._id).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        // console.log("pros service updatePass = ", data);
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}


export default {
    addSinglePro,
    getAllProsByQuery,
    getProByQuery,
    removePro,
    updateSinglePro,
    updateSingleProImage,
    searchAllPros,
    getIndexedProByQuery,
    addGalleryImage,
    getProGallery,
    getProGalleryOneImage,
    deleteGalleryImage,
    deleteReview,
    addSingleReview,
    getAllReviewsByQuery,
    getReviewByQuery,
    getAllReviewsStatsByQuery
};
