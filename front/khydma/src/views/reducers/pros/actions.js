import {PRO_DELETED, GOT_ALL_PROS, GOT_ID_PRO, NEW_PRO, UPDATED_PRO, GOT_PRO_ME, SEARCH_PROS_RES} from './actionTypes';

import ProsApi from './prosApi';

export function getPros(sucCb, errCb, query = {}) {
    return (dispatch) => {
        ProsApi.getAllProsByQuery(query, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: GOT_ALL_PROS, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function getIndexedPros(sucCb, errCb, query = {}) {
    return (dispatch) => {
        ProsApi.getIndexedProByQuery(query, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: GOT_ALL_PROS, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function searchPros(sucCb, errCb, query = {}) {
    return (dispatch) => {
        ProsApi.searchAllPros(query, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: SEARCH_PROS_RES, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function getProByAttribute(sucCb, errCb, query = {}) {
    return (dispatch) => {
        ProsApi.getProByQuery(query, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: GOT_ID_PRO, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function getProMe(sucCb, errCb, query: {userId: string}) {
    return (dispatch) => {
        ProsApi.getProByQuery(query, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: GOT_PRO_ME, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function newPro(body, sucCb, errCb) {
    return (dispatch) => {
        ProsApi.addSinglePro(body, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: NEW_PRO, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function updatePro(body, sucCb, errCb) {
    return (dispatch) => {
        ProsApi.updateSinglePro(body, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: UPDATED_PRO, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function updateProImage(body, prCb, sucCb, errCb) {
    return (dispatch) => {
        ProsApi.updateSingleProImage(body, (progress) => {
            prCb(progress)
        }, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: UPDATED_PRO, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function removeProById(body, sucCb, errCb) {
    return (dispatch) => {
        ProsApi.removePro(body, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: PRO_DELETED, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}
