export const GOT_ALL_PROS = 'pros/GOT_ALL_PROS';
export const SEARCH_PROS_RES = 'pros/SEARCH_PROS_RES';
export const UPDATED_PRO = 'pros/UPDATED_PRO';
export const NEW_PRO = 'pros/NEW_PRO';
export const GOT_ID_PRO = 'pros/GOT_ID_PRO';
export const GOT_PRO_ME = 'pros/GOT_PRO_ME';
export const PRO_DELETED = 'pros/PRO_DELETED';
