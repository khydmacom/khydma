import {prosReducer} from './prosReducer';
import prosApi from './prosApi';
import * as prosActions from './actions';

export {
    prosReducer,
    prosActions,
    prosApi
}
