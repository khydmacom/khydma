import {GOT_PRO_ME, GOT_ALL_PROS, GOT_ID_PRO, NEW_PRO, UPDATED_PRO, SEARCH_PROS_RES} from './actionTypes';
import {CONST} from "../../../config";

let initialState = {proByQuery: null, allPros: [], proMe: null, proSearchResults: []};

export const prosReducer = (state = initialState, action) => {
    switch (action.type) {
        case GOT_ALL_PROS:

            return {...state, allPros: action.data};
        case SEARCH_PROS_RES:
            // console.log('SEARCH_PROS_RES = ', action.data);
            return {...state, proSearchResults: action.data};
        case GOT_ID_PRO:
            const proByQuery = action.data;

            return {...state, proByQuery};
        case GOT_PRO_ME:
            const proMe = action.data;

            return {...state, proMe};
        case NEW_PRO:
            let allPros = state.allPros;
            // console.log("NEW_PRO = ", allPros);
            allPros.push(action.data);
            return {...state, allPros: allPros};
        case UPDATED_PRO:
            let elIndex = state.allPros.findIndex((x) => {return x._id === action.data._id});
            let cats = state.allPros;
            cats.splice(elIndex, 1, action.data);
            return {...state, allPros: cats};
        default:
            return state;
    }
};
