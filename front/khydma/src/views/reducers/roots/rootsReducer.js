import {ROOT_UPDATED, TO_GET_BACK_TO} from './actionTypes';

let initialState = {path: '/', uri: '/', pathname: '/', pathToGetBackTo: ''};

export const rootsReducer = (state = initialState, action) => {
    if (action.type === ROOT_UPDATED) {
        return {...state, path: action.data.path, uri: action.data.uri, pathname: action.data.location.pathname};
    } else if (action.type === TO_GET_BACK_TO) {
        // console.log(action);
        return {...state, pathToGetBackTo: action.data};
    } else {
        return state;
    }
};
