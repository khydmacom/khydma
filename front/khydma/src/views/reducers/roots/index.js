import {rootsReducer} from './rootsReducer';
import * as RootsActions from './actions';

export {
    rootsReducer,
    RootsActions
}
