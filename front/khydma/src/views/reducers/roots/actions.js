import {ROOT_UPDATED, TO_GET_BACK_TO} from './actionTypes';


export function updateRoot(root) {
    console.log(root);
    return (dispatch) => {
        dispatch({type: ROOT_UPDATED, data: root});
    };
}

export function updateToGetBack(root) {
    return (dispatch) => {
        dispatch({type: TO_GET_BACK_TO, data: root});
    };
}
