import {GOT_CURRENT_USER, CURRENT_PRO, CURRENT_ADMIN, GOT_CURRENT_PROFILE, EMPTYING} from './actionTypes';

import UsersApi from './currentUserApi';

export function getCurrentProfile(sucCb, errCb) {
    return (dispatch) => {
        UsersApi.getCurrentProfile((success, user, errMessage) => {
            if (success) {
                dispatch({type: GOT_CURRENT_PROFILE, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function getCurrentUser(sucCb, errCb) {
    return (dispatch) => {
        UsersApi.getCurrentUser((success, user, errMessage) => {
            if (success) {
                dispatch({type: GOT_CURRENT_USER, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function emptyCurrentUser() {
    return (dispatch) => {
        dispatch({type: EMPTYING, data: null});
    }
}

export function checkIfCurrentIsAdmin(sucCb, errCb) {
    return (dispatch) => {
        UsersApi.checkIfAdmin((success, user, errMessage) => {
            if (success) {
                dispatch({type: CURRENT_ADMIN, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function checkIfCurrentIsPro(sucCb, errCb) {
    return (dispatch) => {
        UsersApi.checkIfPro((success, user, errMessage) => {
            if (success) {
                dispatch({type: CURRENT_PRO, data: user});
                sucCb(user);
            } else if (errMessage) errCb(errMessage);
        });
    };
}
