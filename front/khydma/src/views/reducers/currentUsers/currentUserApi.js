import {CurrentUsersServices} from '../../../config/services';

const {getMe, checkAdmin, getMyProfile, checkPro} = CurrentUsersServices;

function getCurrentUser(cb) {
    getMe().then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        // console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getCurrentProfile(cb) {
    getMyProfile().then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        // console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function checkIfAdmin(cb) {
    checkAdmin().then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        // console.log("admin check err = ", err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function checkIfPro(cb) {
    checkPro().then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

export default {
    getCurrentUser,
    checkIfAdmin,
    getCurrentProfile,
    checkIfPro
};
