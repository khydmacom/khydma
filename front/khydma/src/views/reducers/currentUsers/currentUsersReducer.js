import {GOT_CURRENT_USER, CURRENT_PRO, CURRENT_ADMIN, GOT_CURRENT_PROFILE, EMPTYING} from './actionTypes';
import {CONST} from "../../../config";

let initialState = {user: null, isAdmin: false, isPro: false};

export const currentUsersReducer = (state = initialState, action) => {
    switch (action.type) {
        case GOT_CURRENT_USER:
            const user = action.data;
            return {...state, user};
        case CURRENT_PRO:
                return {...state, isPro: true};
        case CURRENT_ADMIN:
            return {...state, user: action.data, isAdmin: true};
        case GOT_CURRENT_PROFILE:
            return {...state, user: action.data};
        case EMPTYING:
            return {...state, user: null, isAdmin: false, isPro: false};
        default:
            return state;
    }
};
