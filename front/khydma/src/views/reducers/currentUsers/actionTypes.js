export const GOT_CURRENT_USER = 'current/GOT_CURRENT_USER';
export const GOT_CURRENT_PROFILE = 'current/GOT_CURRENT_PROFILE';
export const CURRENT_ADMIN = 'current/CURRENT_ADMIN';
export const CURRENT_PRO = 'current/CURRENT_PRO';
export const EMPTYING = 'current/EMPTYING';

// export const SIGNED_OUT = 'current/SIGNED_OUT';
// export const TOKEN = 'auth/TOKEN';
