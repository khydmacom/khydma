import {currentUsersReducer} from './currentUsersReducer';
import * as CurrentUsersActions from './actions';

export {
    currentUsersReducer,
    CurrentUsersActions
}
