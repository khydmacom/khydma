import {FORM_FILLING_STARTED} from './actionTypes';


export function handleFormFilling(form) {
    return (dispatch) => {
        dispatch({type: FORM_FILLING_STARTED, data: form});
    };
}
