import {FORM_FILLING_STARTED} from './actionTypes';

let initialState = {proForm: null};

export const proFormFillingReducer = (state = initialState, action) => {
    if (action.type === FORM_FILLING_STARTED) {
        return {...state, proForm: action.data};
    } else {
        return state;
    }
};
