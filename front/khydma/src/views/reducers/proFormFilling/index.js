import {proFormFillingReducer} from './proFormFillingReducer';
import * as ProFormFillingActions from './actions';

export {
    proFormFillingReducer,
    ProFormFillingActions
}
