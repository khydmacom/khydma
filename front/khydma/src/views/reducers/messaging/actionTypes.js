export const NEW_MESSAGE = 'messaging/NEW_MESSAGE';
export const NEW_CONVERSATION = 'messaging/NEW_CONVERSATION';
export const DELETE_CONVERSATION = 'messaging/DELETE_CONVERSATION';
export const UPDATE_CONVERSATION = 'messaging/UPDATE_CONVERSATION';

// export const SIGNED_OUT = 'auth/SIGNED_OUT';
// export const TOKEN = 'auth/TOKEN';
