import {NEW_CONVERSATION, DELETE_CONVERSATION, NEW_MESSAGE, UPDATE_CONVERSATION} from './actionTypes';
import {SocketServices} from '../../../config/services';
import MsgsApi from './messagingApi';
import {CONST} from '../../../config';

const _SocketServices = SocketServices;
const {ioTriggers} = CONST;

// here to get the history of convs and msgs

export function getConversationsByQuery(sucCb, errCb, query = {}) {
    return (dispatch) => {
        MsgsApi.getConvs(query, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: NEW_CONVERSATION, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

export function getMessagesByQuery(sucCb, errCb, query = {}) {
    return (dispatch) => {
        MsgsApi.getMsgs(query, (success, pro, errMessage) => {
            if (success) {
                dispatch({type: NEW_MESSAGE, data: pro});
                sucCb(pro);
            } else if (errMessage) errCb(errMessage);
        });
    };
}

// here to subscribe on all the new coming ones

export function newMessage(ioRes, cb) {
        return (dispatch) => {
            cb(ioRes);
            console.log(ioRes);
            if (ioRes)
            dispatch({type: NEW_MESSAGE, data: ioRes});
        };
}

export function newConversatoin(ioRes, cb) {
        return (dispatch) => {
            cb(ioRes);
            dispatch({type: NEW_CONVERSATION, data: ioRes});
        };
}

/*export function updateConversation(cb) {
    _SocketServices.prototype.socket.on(ioTriggers.updatedConversation, ioRes => {
        return (dispatch) => {
            cb(ioRes);
            dispatch({type: UPDATE_CONVERSATION, data: ioRes});
        };
    });
}

export function deleteConversation(cb) {
    _SocketServices.prototype.socket.on(ioTriggers.deleteConversation, ioRes => {
        return (dispatch) => {
            cb(ioRes);
            dispatch({type: DELETE_CONVERSATION, data: ioRes});
        };
    });
}*/
