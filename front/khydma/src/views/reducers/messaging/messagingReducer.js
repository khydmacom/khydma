import {NEW_CONVERSATION, DELETE_CONVERSATION, NEW_MESSAGE, UPDATE_CONVERSATION} from './actionTypes';

let initialState = {conversationMessages: [], conversations: []};
const pushToArray = (arr, obj) => {
    let existingIds = arr.map((obj) => obj._id);

    if (!existingIds.includes(obj._id)) {
        arr.push(obj);
        return arr;
    } else {
        return arr.forEach((element, index) => {
            if (element._id === obj._id) {
                arr[index] = obj;
            }
        });
    }
};

export const messagingReducer = (state = initialState, action) => {
    switch (action.type) {
        case NEW_CONVERSATION:
            if (typeof action.data.length === "undefined")
               return {...state, conversations: pushToArray(state.conversations.slice(), action.data)};
            else return {...state, conversations: action.data};
        case NEW_MESSAGE:
            let initMessages = state.conversationMessages;
            if (typeof action.data.length === "undefined")
                initMessages.push(action.data);
            else initMessages = action.data;

            return {...state, conversationMessages: initMessages};
        case DELETE_CONVERSATION:
            // here to check again
            return {...state};
        case UPDATE_CONVERSATION:
            // here to check again
            return {...state};
        default:
            return state;
    }
};
