import {MessagingServices, SocketServices} from '../../../config/services';
import {NEW_MESSAGE} from "./actionTypes";
import {CONST} from "../../../config";

const {getConversations, getMessages} = MessagingServices;
const _SocketServices = SocketServices;
const {ioTriggers} = CONST;

function getConvs(query = {}, cb) {
    getConversations(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function getMsgs(query = {}, cb) {
    getMessages(query).then((data: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
        if (data.status > 200)
            cb(false, null, {message: (data.status === 207 ? data.data.message : data.statusText)});
        else cb(true, data.data, null);
    }).catch((err) => {
        console.log(err);
        if (err && err.request && err.request.status) {
            cb(false, null, {message: err.request.statusText, errCode: err.request.status});
        } else cb(false, null, {message: JSON.stringify(err)});
    });
}

function onMessages(cb) {
    _SocketServices.prototype.socket.on(ioTriggers.newMessage, ioRes => {
        if (ioRes)
        cb(ioRes);
    });
}

function onConversation(cb) {
    _SocketServices.prototype.socket.on(ioTriggers.newConversation, ioRes => {
        console.log(ioRes)
        if (ioRes)
        cb(ioRes);
    });
}


export default {
    getConvs,
    getMsgs,
    onConversation,
    onMessages
};
