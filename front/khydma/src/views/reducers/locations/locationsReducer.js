import {OSM_SCRIPT_LOADED, OSM_GEOCODING_RESULT} from './actionTypes';

let initialState = {
    osmAutocomplete: null,
    geoCodingResults: null
};

export const locationsReducer = (state = initialState, action) => {
    if (action.type === OSM_SCRIPT_LOADED) {
        return {...state, osmAutocomplete: action.data};
        } else if (action.type === OSM_GEOCODING_RESULT) {
            return {...state, geoCodingResults: action.data};
    } else {
        return state;
    }
};
