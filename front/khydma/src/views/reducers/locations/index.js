import {locationsReducer} from './locationsReducer';
import * as LocationsActions from './actions';

export {
    locationsReducer,
    LocationsActions
}
