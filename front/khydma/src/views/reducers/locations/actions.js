import {OSM_SCRIPT_LOADED, OSM_GEOCODING_RESULT} from './actionTypes';
import {CONST} from '../../../config'
import axios from 'axios';
import {object} from "prop-types";

// declare var google: any;
declare var kt: any;

const {API_OSM_KEY, OSM_GEOCODER_BASE_LINK} = CONST;

// osm REST example: https://nominatim.openstreetmap.org/reverse?format=geojson&lat=44.50155&lon=11.33989
const CancelToken = axios.CancelToken;
let cancel;

const GetOsmGeoAddress = (latLng: { lat: number, lng: number }) => {
    return axios.get(`${OSM_GEOCODER_BASE_LINK}lat=${latLng.lat}&lon=${latLng.lng}`,
        {
            cancelToken: new CancelToken(
                function executor(c) {
                    cancel = c;
                })
        })
};

export function handleScriptLoad(script) {
    return (dispatch) => {
        // We load the new google.maps.places in here (in this case OSM)
        // then we will be using the places props and call what we need from there
        // example : this.props.places.Autocomplete(document.getElementById('searchInput'));
        // IMPORTANT!! : In this version we are using OpenStreetMaps due to google's billing system

        dispatch({type: OSM_SCRIPT_LOADED, data: kt});
    };
}

export function handleGeoCodingRequest(loc, cb) {
    return (dispatch) => {
        GetOsmGeoAddress(loc).then((result: { data: Object, status: number, statusText: string, headers: Object, config: Object, request: Object }) => {
            cb(result.data.features[0].properties);
            dispatch({type: OSM_GEOCODING_RESULT, data: {loc: loc, ...result.data.features[0].properties}});
        });
    };
}
