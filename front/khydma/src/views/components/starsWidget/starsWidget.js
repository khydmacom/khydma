import React, {Component, useState} from 'react';
import './starsWidget.css';
import * as PropTypes from 'prop-types';
import {IconButton} from "@material-ui/core";
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';


const Star = ({disabled = false, selected = false, onClick = f => f}) => (
    <IconButton disabled={disabled} onClick={onClick}>
        {selected ? <StarIcon style={{color: "#FFD700"}}/> : <StarBorderIcon/>}
    </IconButton>
);

export const StarsWidget = ({totalStars, rate, onStarsUpdate, disabled = false}) => {
    const [starsSelected, selectStar] = useState(rate ? rate : 1);
    return (
        <div className="star-rating">
            {[...Array(totalStars)].map((n, i) => (
                <Star
                    disabled={disabled}
                    key={i}
                    selected={i < starsSelected}
                    onClick={() => {
                        selectStar(i + 1);
                        onStarsUpdate(i + 1);
                    }}
                />
            ))}
        </div>
    );
};
