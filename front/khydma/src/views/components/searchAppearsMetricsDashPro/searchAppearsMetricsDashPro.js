import React, {Component} from 'react';
import './searchAppearsMetricsDashPro.css';
import * as PropTypes from 'prop-types';
import {
    LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
    {
        name: 'Dimanche', apparence: 40,
    },
    {
        name: 'Lundi', apparence: 30,
    },
    {
        name: 'Mardi', apparence: 20,
    },
    {
        name: 'Mercredi', apparence: 27,
    },
    {
        name: 'Jeudi', apparence: 18,
    },
    {
        name: 'Vendredi', apparence: 23,
    },
    {
        name: 'Samedi', apparence: 34,
    },
];


class SearchAppearsMetricsDashPro extends Component {

    constructor(props) {
        super(props);

        this.state = {
            text: "",
            textWords: []
        };
    }

    componentDidMount(): void {
        // this.cutIntroToShow();
    }

    cutIntroToShow = () => {
        let text = this.props.conversation.lastMessageId.text;
        let textWords = this.props.conversation.lastMessageId.text.split(" ");
        // console.log(this.state.textWords.length);

        if (textWords.length > 6) {
            let lastWord = textWords[6];
            let indexOfLastWord = text.indexOf(lastWord);
            this.setState({
                text: text.slice(0, indexOfLastWord) + "...",
            });
        } this.setState({
            text: text
        })
    };

    render() {
        const {isPro} = this.props;
        const {text} = this.state;
        // let image = isPro ? this.props.conversation.userId.profilePicture : this.props.conversation.proId.businessImagePath;
        return (
                <div className="metricsElement shadow">
                    <span className="insightTitle">Votre apparaît en recherche</span>
                    <LineChart
                        width={500}
                        height={300}
                        data={data}
                        margin={{
                            top: 5, right: 30, left: 20, bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Line type="monotone" dataKey="apparence" stroke="#8884d8" activeDot={{ r: 8 }} />
                        {/*<Line type="monotone" dataKey="affichagenumeroSeulement" stroke="#82ca9d" />*/}
                    </LineChart>
                </div>
        );
    }
}

SearchAppearsMetricsDashPro.propTypes = {
    conversation: PropTypes.object,
    isPro: PropTypes.bool
};

export default SearchAppearsMetricsDashPro;
