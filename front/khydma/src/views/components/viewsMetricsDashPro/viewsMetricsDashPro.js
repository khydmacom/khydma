import React, {Component} from 'react';
import './viewsMetricsDashPro.css';
import * as PropTypes from 'prop-types';
import {
    AreaChart, Area, XAxis, YAxis, CartesianGrid, Tooltip,
} from 'recharts';

const data = [
    {
        name: 'Dimanche', Vue: 12,
    },
    {
        name: 'Lundi', Vue: 30,
    },
    {
        name: 'Mardi', Vue: 20,
    },
    {
        name: 'Mercredi', Vue: 29,
    },
    {
        name: 'Jeudi', Vue: 12,
    },
    {
        name: 'Vendredi', Vue: 15,
    },
    {
        name: 'Samedi', Vue: 12,
    },
];


class ViewsMetricsDashPro extends Component {

    constructor(props) {
        super(props);

        this.state = {
            text: "",
            textWords: []
        };
    }

    componentDidMount(): void {
        // this.cutIntroToShow();
    }

    cutIntroToShow = () => {
        let text = this.props.conversation.lastMessageId.text;
        let textWords = this.props.conversation.lastMessageId.text.split(" ");
        // console.log(this.state.textWords.length);

        if (textWords.length > 6) {
            let lastWord = textWords[6];
            let indexOfLastWord = text.indexOf(lastWord);
            this.setState({
                text: text.slice(0, indexOfLastWord) + "...",
            });
        } this.setState({
            text: text
        })
    };

    render() {
        const {isPro} = this.props;
        const {text} = this.state;
        // let image = isPro ? this.props.conversation.userId.profilePicture : this.props.conversation.proId.businessImagePath;
        return (
                <div className="metricsElement shadow">
                    <span className="insightTitle">Nombre de personnes ayant vu votre profil</span>

                    <AreaChart
                        width={500}
                        height={400}
                        data={data}
                        margin={{
                            top: 10, right: 30, left: 0, bottom: 0,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Area type="monotone" dataKey="Vue" stroke="#8884d8" fill="#8884d8" />
                    </AreaChart>
                </div>
        );
    }
}

ViewsMetricsDashPro.propTypes = {
    conversation: PropTypes.object,
    isPro: PropTypes.bool
};

export default ViewsMetricsDashPro;
