import React, {Component} from 'react';
import './footerComponent.css';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome'
import {withStyles} from '@material-ui/core/styles'
import {faFacebook, faInstagram, faYoutube} from "@fortawesome/free-brands-svg-icons";
import {connect} from "react-redux";
import {categoriesActions} from "../../reducers/categories";
import {Misc} from '../../../config';
import {Link} from '@reach/router';
import {Button, Menu, MenuItem} from "@material-ui/core";
import ArrowDropDown from '@material-ui/icons/ArrowDropDown';


const {getCategories} = categoriesActions;
const {reorderCategories} = Misc;

const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    grow: {
        flexGrow: 1,
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    }
});


class FooterComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showFilter: true,
            categories: []
        }
    }

    componentDidMount(): void {
        let cats = reorderCategories(this.props.allCategories).newArray;
        // console.log(cats);

        this.setState({
            categories: cats
        });
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (nextProps !== this.props) {
            let cats = reorderCategories(nextProps.allCategories).newArray;
            // console.log(cats);

            this.setState({
                categories: cats
            });
        }
    }

    categoriesMenuClick = event => {
        this.setState({
            ['anchorEl' + event.currentTarget.id]: event.currentTarget,
            lastOpenedMenu: 'anchorEl' + event.currentTarget.id
        });
    };

    categoriesMenuClose = event => {
        console.log(event)
        this.setState({[this.state.lastOpenedMenu]: null});
    };

    render() {
        const {categories} = this.state;
        const {isLoggedIn, isPro} = this.props;

        return (
            <div className="mainFooterDiv">
                <div className="footerContent">
                    {
                        isLoggedIn ? <div/> : <section>
                            <ul style={{listStyle: 'none'}}>
                                <Button variant={"text"} id={`clientEtpros`} color={"primary"}
                                        onClick={this.categoriesMenuClick}>Clients et
                                    professionnels<ArrowDropDown/></Button>
                                <Menu
                                    id="simple-menu"
                                    anchorEl={this.state[`anchorElclientEtpros`]}
                                    open={Boolean(this.state[`anchorElclientEtpros`])}
                                    onClose={this.categoriesMenuClose}
                                    // style={{top: this.state.toolbarHeight + "px"}}
                                >
                                    <MenuItem onClick={this.categoriesMenuClose}><Link to={"/login"}><i>Se connecter</i></Link>
                                    </MenuItem>
                                    <MenuItem onClick={this.categoriesMenuClose}><Link to={"/signup"}><i>S'inscrire</i></Link>
                                    </MenuItem>
                                    <MenuItem onClick={this.categoriesMenuClose}><Link to={"/proform"}><i>S'inscrire
                                        comme Pro</i></Link>
                                    </MenuItem>
                                </Menu>
                            </ul>
                        </section>
                    }
                    {
                        categories.map((category, index) => {
                            return (
                                <section key={index}>
                                    <ul style={{listStyle: 'none'}}>
                                        <Button variant={"text"} id={`categoriesMenuButtonId${index}`} color={"primary"}
                                                onClick={this.categoriesMenuClick}>{category.label}<ArrowDropDown/></Button>
                                        <Menu
                                            id="simple-menu"
                                            anchorEl={this.state[`anchorEl${'categoriesMenuButtonId' + index}`]}
                                            open={Boolean(this.state[`anchorEl${'categoriesMenuButtonId' + index}`])}
                                            onClose={this.categoriesMenuClose}
                                            // style={{top: this.state.toolbarHeight + "px"}}
                                        >
                                            <MenuItem onClick={this.categoriesMenuClose}><Link
                                                to={`/categories?q=${category.label}`}>{category.label}</Link>
                                            </MenuItem>

                                            {
                                                category.categories.map((cat, idx) => {
                                                    return <MenuItem key={cat._id}
                                                                     onClick={this.categoriesMenuClose}><Link
                                                        to={`/search?q=${cat.label}`}><i>{cat.label}</i></Link></MenuItem>
                                                })
                                            }
                                        </Menu>
                                    </ul>
                                </section>
                            );
                        })
                    }
                </div>
                <div className="lineDevider"/>
                <div className="copyRight">
                    <p>Copyright &copy; <a href="#"> Khydma.com</a>.
                    </p>
                    <div className="socialContainer">
                        <a href="https://www.facebook.com/Khydmacom-428218431056616/" target="_blank">
                        <span>
                           <FontAwesomeIcon className="iconSocial" icon={faFacebook}/>
                        </span>
                        </a>
                        {/*<a href="https://www.facebook.com/Khydmacom-428218431056616/" target="_blank">
                        <span>
                           <FontAwesomeIcon className="iconSocial" icon={faInstagram}/>
                        </span>
                        </a>*/}
                    </div>
                </div>
            </div>
        );
    }
}

let statesToProps = (state, props) => {
    if (state.categoriesReducer && state.authReducer && state.currentUsersReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            allCategories: state.categoriesReducer.allCategories,
            isPro: state.currentUsersReducer
        }
    } else
        return {}
};

export default connect(statesToProps, {getCategories})(withStyles(styles)(FooterComponent));
