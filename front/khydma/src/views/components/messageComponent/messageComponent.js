import React, {Component} from 'react';
import './messageComponent.css';
import * as PropTypes from 'prop-types';
import profileFiller from "../../../assets/images/no.png";
import TimeAgo from "react-timeago";
import frenchStrings from 'react-timeago/lib/language-strings/fr'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'

const formatter = buildFormatter(frenchStrings);


/*
* "text": "Eonsectetur.\r\n",
                    "userId": "5d116fb09832a30bfa0db137",
                    "name": "Christie Conway",
                    "avatar": "http://placehold.it/32x32",
                    "creation_date": "2019-06-24T06:52:34 -01:00"
* */

class MessageComponent extends Component {

    render() {
        const {message, currentUser, previousUserId} = this.props;
        const senderId = (message.senderId ? message.senderId : message.senderIdPro);
        const avatar = (message.senderId ? message.senderId.profilePicture : message.senderIdPro.businessImagePath)
        return (
            <div
                className={senderId === currentUser._id ? 'messagesComponentMainDiv myMessageMainDiv' : 'messagesComponentMainDiv herMessageMainDiv'}>
                <div className="convoSpacer"/>
                {senderId !== currentUser._id && senderId !== previousUserId ?
                    <div style={{backgroundImage: `url("${avatar ? avatar : profileFiller}")`}}
                         className="senderAvatar"/> : <div className={message.userId !== currentUser._id ? 'fillerDivAvatar' : 'nothingHereEver'}/>}
                <div className={senderId === currentUser._id ? 'messageContentContainer myMessageMainDiv' : 'messageContentContainer herMessageMainDiv'}>
                    <span className="messageContent">{message.text}</span>
                    <span className={senderId === currentUser._id ? 'dateReview messageOverRide myMessageMainDiv' : 'dateReview messageOverRide herMessageMainDiv'}><TimeAgo date={message.creation_date}
                                                                          formatter={formatter}/></span>
                </div>
            </div>
        );
    }
}

MessageComponent.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string
};

export default MessageComponent;
