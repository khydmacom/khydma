import React, {Component} from 'react';
import ReactDom from 'react-dom';
import './headerComponent.css';
import {AppBar, Button, IconButton, Menu, MenuItem, Toolbar} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import FilterList from '@material-ui/icons/FilterList';
import {withStyles} from '@material-ui/core/styles'
import {Link} from "@reach/router";
import {CONST} from "../../../config";
import {connect} from "react-redux";
import {AuthActions} from "../../reducers/auth";
import {CurrentUsersActions} from "../../reducers/currentUsers";
import PropTypes from 'prop-types';
import pictureFiler from "../../../assets/images/no.png";
import logoFull from '../../../assets/icons/final/full.png';

const {getToken, reAuth, rejectToken} = AuthActions;
const {getCurrentUser, emptyCurrentUser} = CurrentUsersActions;

const {pubSubConstants, localStorageIndexes} = CONST;
const pubSubJs = require('pubsub-js');


const styles = theme => ({
    root: {
        justifyContent: 'center',
        alignItems: 'center',
        display: 'flex',
        flexDirection: 'row',
        width: '100vw',
        backgroundColor: "#fff",
        zIndex: 1220,
        position: 'fixed'
    },
    borderToHeader: {
        borderBottom: 'rgba(0, 0, 0, 0.1) solid 1px'
    },
    grow: {
        flexGrow: 1
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    }
});


class HeaderComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showFilter: false,
            toolbarHeight: 0,
            anchorEl: null,
            elevation: 0
        }
    }

    componentWillMount() {
        pubSubJs.subscribe(pubSubConstants.showFilter, () => {
            // console.log('lol');
            this.setState({
                showFilter: true
            })
        });
        pubSubJs.subscribe(pubSubConstants.hideFilter, () => {
            this.setState({
                showFilter: false
            })
        });
    }

    componentDidMount(): void {
        window.addEventListener('scroll', this.listenToScroll);
    }

    handleMenuClick = event => {
        console.log(event.currentTarget.id);
        this.setState({anchorEl: event.currentTarget});
    };

    handleMenuClose = () => {
        this.setState({anchorEl: null});
    };

    handleLogout = () => {
        if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))

            this.props.rejectToken({refreshToken: JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.refreshToken}, (res) => {
                this.props.emptyCurrentUser();
                // don't forget to pass the navigation prop to navogate to home
            }, (err) => {
                // console.log("logout err = ", err);
            });
        this.handleMenuClose();
    };

    listenToScroll = () => {
        const winScroll = document.body.scrollTop || document.documentElement.scrollTop;

        const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        const clientHeight = document.documentElement.clientHeight;

        if (winScroll >= 20) {
            this.setState({
                elevation: 4
            })
        } else {
            this.setState({
                elevation: 0
            })
        }
    };

    componentWillUnmount(): void {
        window.removeEventListener('scroll', this.listenToScroll);
    }

    render() {
        const {classes, pathname, user} = this.props;

        const renderHeaderProfile = (
            <div className="headerAvBtn">
                <div className="hideLogo">
                    <Link to="/dashboard/conversations">
                        <Button color="secondary" className={classes.button}>
                            Messages
                        </Button>
                    </Link>
                </div>
                <IconButton color="secondary" aria-label="Menu"
                            id="bbbbbbbb"
                            onClick={this.handleMenuClick}>
                    <div className="avatarHeaderPicture" style={{
                        backgroundImage: "url('" + ((user && user.profilePicture) ? user.profilePicture : pictureFiler) + "')",
                        height: 40 + 'px',
                        width: 40 + 'px',
                        borderRadius: (40 / 2) + 'px'
                    }}/>
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={this.state.anchorEl}
                    open={Boolean(this.state.anchorEl)}
                    onClose={this.handleMenuClose}
                    style={{top: this.state.toolbarHeight + "px"}}
                >
                    <Link to={'/dashboard/profile'}>
                        <MenuItem
                            onClick={this.handleMenuClose}>{this.props.user ? (this.props.user.lastName ? (this.props.user.name + " " + this.props.user.lastName) : this.props.user.name) : "Profile"}</MenuItem>
                    </Link>
                    <Link to={'/dashboard/notifications'}>
                        <MenuItem onClick={this.handleMenuClose}>Notifications</MenuItem>
                    </Link>
                    <MenuItem onClick={this.handleLogout}>Déconnexion</MenuItem>
                </Menu>
            </div>
        );

        return (
            <AppBar elevation={this.state.elevation}>
                <Toolbar
                    className={'alignCenterHeader' + (pathname.includes('dashboard') ? ' borderBottomHeader' : '')}>
                    <div className="rowAlign headerViewContainer">
                        <div className="menu">
                            <IconButton color="secondary" aria-label="Menu"
                                        onClick={this.props.toggleDrawer('left', !this.props.open)}>
                                <MenuIcon/>
                            </IconButton>
                        </div>
                        {
                            pathname.includes('dashboard') ? <div/> : <div className="hideLogo">
                                <Link to="/">
                                    <Button color="inherit" aria-label="Menu">
                                        <img src={logoFull} alt={"Khydma.com logo"} className="logoButton"/>
                                    </Button>
                                </Link>
                            </div>
                        }

                        <div className={classes.grow}/>
                        {this.state.showFilter ? <div className="menu">
                            <IconButton color="secondary" aria-label="Menu"
                                        onClick={this.props.toggleDrawer('right', !this.props.openFilter)}>
                                <FilterList/>
                            </IconButton>
                        </div> : ''}
                        {this.props.isLoggedIn ? renderHeaderProfile :
                            <>
                                <Link to="/proform">
                                    <Button variant="contained" color="secondary" className={classes.button}>
                                        S'inscrire comme Pro
                                    </Button>
                                </Link>
                                <div className="hideLogo">
                                    <Link to="/signup">
                                        <Button color="secondary" className={classes.button}>
                                            S'inscrire
                                        </Button>
                                    </Link>
                                    <Link to="/login">
                                        <Button color="secondary" className={classes.button}>
                                            Se connecter
                                        </Button>
                                    </Link>
                                </div>
                            </>}
                    </div>
                </Toolbar>
            </AppBar>
        );
    }
}

HeaderComponent.propTypes = {
    toggleDrawer: PropTypes.func
};

let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            isAdmin: state.currentUsersReducer.isAdmin,
            user: state.currentUsersReducer.user,
            pathname: state.rootsReducer.pathname
        }
    } else
        return {}
};

export default connect(statesToProps, {rejectToken, emptyCurrentUser})(withStyles(styles)(HeaderComponent));
