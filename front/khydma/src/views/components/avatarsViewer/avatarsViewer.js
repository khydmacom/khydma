import React, {Component} from 'react';
import './avatarsViewerStyles.css';
import * as PropTypes from 'prop-types';


class AvatarsViewerComponent extends Component {

    render() {
        const { avatars } = this.props;
        // console.log(avatars)
        return (
            <div className="smallAvatarsMainDiv">
                {
                    avatars.slice(0, 3).map((av, index) => {
                      return <div key={index} style={{backgroundImage: `url(${av.proId.businessImagePath})`, backgroundPosition: 'center', backgroundSize: 'cover'}} className="smallAvatar"/>
                    })
                }
                {avatars.length >= 4 ? <span className="smallAvatar fillerSpan">...</span> : <div/>}
            </div>
        );
    }
}

AvatarsViewerComponent.propTypes = {
    avatars: PropTypes.array
};

export default AvatarsViewerComponent;
