import React, {Component} from 'react';
import './messagesList.css';
import * as PropTypes from 'prop-types';
import {MessageComponent} from "../messageComponent";
import {Misc} from "../../../config";
import TextField from "@material-ui/core/TextField";
import {Button, IconButton, MuiThemeProvider} from "@material-ui/core";
import Send from "@material-ui/icons/Send";
import * as ReactDOM from 'react-dom';
import {MessagingApi} from '../../reducers/messaging';
import {Link} from "@reach/router";


class MessagesList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorMessage: '',
            text: '',
            messages: []
        }
    }

    componentDidMount(): void {
        this.getMessagesList(this.props)
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        // if (nextProps.parsed !== this.props.parsed) {
            if (nextProps.profile)
                this.getMessagesList(nextProps);

        /*if (nextProps.lastNewMessage !== this.props.lastNewMessage) {
            console.log('zzz prime')
            let copy = this.state.messages.slice();
            copy.push(nextProps.lastNewMessage);
            this.setState({messages: copy})
        }*/
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        if (prevProps.parsed !== this.props.parsed) {
            if (this.props.profile)
                this.getMessagesList(this.props);
        }

        if ((prevProps.lastNewMessage !== this.props.lastNewMessage) && this.props.profile) {
            let copy = this.state.messages.slice();
            copy.push(this.props.lastNewMessage);
            this.setState({messages: copy})
        }

        if (this.messagesHistory)
            this.scrollToBottom();
    }

    handleTextChange = (event) => {
        event.persist();
        this.setState({
            [event.target.name]: event.target.value
        });
        setTimeout(() => {
            if (event && event.target && event.target.value.length < 1)
                this.setState({
                    errorMessage: 'Vous ne pouvez pas envoyer un message vide!'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 333)
    };

    handleTextBlur = (event) => {
        setTimeout(() => {
            if (this.state.text < 1)
                this.setState({
                    errorMessage: 'Vous ne pouvez pas envoyer un message vide!'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 444)
    };

    onMessagesScroll = (event) => {
        const scrollTop = this.messagesHistory.scrollTop;
        if (scrollTop === 0) {
            console.log('on top!!!!')
        }
    };

    scrollToBottom = () => {
        const scrollHeight = this.messagesHistory.scrollHeight;
        const height = this.messagesHistory.clientHeight;
        const maxScrollTop = scrollHeight - height;
        ReactDOM.findDOMNode(this.messagesHistory).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    };

    getMessagesList = (props) => {
        // if (this.state.messages.length === 0)
            MessagingApi.getConvs((props.profile.businessName ?
                    {proId: props.profile._id, userId: props.user._id}
                    : {proId: props.user.proId, userId: props.profile._id}
            ), (_succ, _res, err) => {
                if (_res && _res.length > 0) {
                    MessagingApi.getMsgs({conversationId: _res[0]._id}, (succ, res, err) => {
                        if (res && res.length > 0) {
                            let copyMsgs = this.state.messages.slice();
                            console.log(copyMsgs)
                            for (let msg of res) {
                                if (copyMsgs.findIndex(x => x._id === msg._id) === -1)
                                    copyMsgs.push(msg);
                            }
                            this.setState({
                                messages: copyMsgs.sort((x, y) => x.creation_date - y.creation_date),
                                conversationId: _res[0]._id
                            });
                        } else {
                            console.log(err)
                        }
                    });
                    console.log(_res);
                }
            })
    };

    sortMessagesByDate = (a, b) => {
        return new Date(a.creation_date) - new Date(b.creation_date)
    };

    /*componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if (nextProps.lastNewMessage !== this.props.lastNewMessage) {
            if (nextProps.lastNewMessage && this.state.conversationId === nextProps.lastNewMessage.conversationId) {
                this.setState({
                    messages: [...this.state.messages, nextProps.lastNewMessage].sort(this.sortMessagesByDate)
                })
            }
        }
    }*/


    render() {
        const {sendMessage, profile, user} = this.props;
        const {errorMessage, text, messages} = this.state;

        return (
            <div className="messagesMainDiv shadow">
                {
                    profile._id ? <div className="cnvElementHolder">
                        <span className="sectionTitle">{profile.businessName ? profile.businessName :
                            `${profile.name} ${profile.lastName}`
                        }</span>
                    </div> : <div/>
                }
                <div className="sectionTitleSeparator"/>
                {
                    profile._id && messages && messages.length > 0 ?
                        <div className="messagesListContainer" onScroll={this.onMessagesScroll} ref={(ref) => (
                            this.messagesHistory = ref)
                        }>
                            {messages.map((msg, index) => {
                                return <div key={index} className="singleMessageContainer">
                                    <MessageComponent message={msg} currentUser={{_id: user._id}}
                                                      previousUserId={messages[index - 1] ? (messages[index - 1].senderIdPro ? messages[index - 1].senderIdPro : messages[index - 1].senderId) : null}/>
                                </div>
                            })}
                        </div> : <div className="CTANoMessages">
                            <span className="noConvosText">Commencer une nouvelle conversation en recherchant des professionnels</span>
                            <Link to={'/'}>
                                <Button variant={"contained"} color={"secondary"}>
                                    Chercher des professionnels
                                </Button>
                            </Link>
                        </div>
                }

                <div className="inputDivMessageMain">
                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <TextField
                            disabled={!profile._id}
                            className="textUpdate"
                            autoComplete="off"
                            onChange={this.handleTextChange}
                            onBlur={this.handleTextBlur}
                            error={!!(errorMessage)}
                            helperText={errorMessage}
                            value={text}
                            label="Message"
                            type="text"
                            name="text"
                            multiline={true}
                            rowsMax="6"
                            margin="normal"
                            variant="outlined"
                        />
                    </MuiThemeProvider>
                    <IconButton
                        color="inherit"
                        aria-label="Open drawer"
                        aria-haspopup="true"
                        className="sendButtonMain"
                        color="secondary"
                        onClick={() => {
                            sendMessage(text);
                            setTimeout(() => {
                                this.setState({
                                    text: ''
                                })
                            }, 455);
                        }}
                        disabled={!profile._id || Boolean(errorMessage) || text.length < 1}
                    >
                        <Send/>
                    </IconButton>
                </div>
            </div>
        );
    }
}

MessagesList.propTypes = {
    sendMessage: PropTypes.func,
    profile: PropTypes.object,
    user: PropTypes.object,
    lastNewMessage: PropTypes.object
};

export default MessagesList;
