import React, {Component} from 'react';
import './reviewElementDashboard.css';
import * as PropTypes from 'prop-types';
import profileFiller from "../../../assets/images/no.png";
import TimeAgo from "react-timeago";
import frenchStrings from 'react-timeago/lib/language-strings/fr'
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter'

const formatter = buildFormatter(frenchStrings);


class ReviewElementDashboard extends Component {

    constructor(props) {
        super(props);

        this.state = {
            text: "",
            textWords: []
        };
    }

    componentDidMount(): void {
        this.cutIntroToShow();
    }

    cutIntroToShow = () => {
        let text = this.props.conversation.lastMessageId.text;
        let textWords = this.props.conversation.lastMessageId.text.split(" ");
        // console.log(this.state.textWords.length);

        if (textWords.length > 6) {
            let lastWord = textWords[6];
            let indexOfLastWord = text.indexOf(lastWord);
            this.setState({
                text: text.slice(0, indexOfLastWord) + "...",
            });
        } this.setState({
            text: text
        })
    };

    render() {
        const {isPro} = this.props;
        const {text} = this.state;
        let image = isPro ? this.props.conversation.userId.profilePicture : this.props.conversation.proId.businessImagePath;
        return (
                <div className="conversationElementComponentMainDiv">
                    <div className="convoSpacer"/>
                    <div style={{backgroundImage: `url("${image ? image : profileFiller}")`}} className="convoAvatar"/>
                    <div className="convoData">
                        <span className="convoSender">{isPro ? this.props.conversation.userId.name + " " + this.props.conversation.userId.lastName : this.props.conversation.proId.businessName}</span>
                        <span className="convoLastMessage">{this.props.conversation.lastMessageId.text}</span>
                        <span className="dateReview convoOverRide"><TimeAgo
                            date={this.props.conversation.lastMessageId.creation_date} formatter={formatter}/></span>
                    </div>
                    {/*<div className="convoSpacer"/>*/}
                </div>
        );
    }
}

ReviewElementDashboard.propTypes = {
    conversation: PropTypes.object,
    isPro: PropTypes.bool
};

export default ReviewElementDashboard;
