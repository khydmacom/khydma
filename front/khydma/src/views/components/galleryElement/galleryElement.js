import React, {Component} from 'react';
import './galleryElement.css';
import * as PropTypes from 'prop-types';
import imageFiller from '../../../assets/images/no.png'
import Delete from '@material-ui/icons/Delete';
import {IconButton} from "@material-ui/core";

class GalleryElement extends Component {

    render() {
        const {image, deleteImage, onDelete} = this.props;
        return (
            <div className="gallerElementMainDiv shadow">
                <div style={{backgroundImage: `url("${image.imagePath}")`}} className="galleryImageDash">
                    {deleteImage ? <IconButton onClick={() => {
                        onDelete(image);
                    }
                    }>
                        <Delete className="danger"/>
                    </IconButton> : <></>}
                </div>
                {/*<div className="galleryElementSpacer"/>*/}
                <div className="galleryElementDescription">
                    <span className="descriptionGalleryElementPro">{image.description}</span>
                </div>
            </div>
        );
    }
}

GalleryElement.propTypes = {
    message: PropTypes.object,
    currentUser: PropTypes.object,
    previousUserId: PropTypes.string,
    deleteImage: PropTypes.bool,
    image: PropTypes.object,
    onDelete: PropTypes.func
};

export default GalleryElement;
