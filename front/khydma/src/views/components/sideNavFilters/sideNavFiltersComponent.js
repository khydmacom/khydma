import React, {Component} from 'react';
import './sideNavFiltersComponent.css';
import {Button, IconButton, Toolbar} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import {withStyles} from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer';
import CloseIcon from '@material-ui/icons/Close';

import eventsIcon from '../../../assets/icons/svg/event.svg';
import bookIcon from '../../../assets/icons/svg/hardbound-book-variant.svg';
import heartIcon from '../../../assets/icons/svg/heartbeat.svg';
import homeIcon from '../../../assets/icons/svg/home.svg';
import portfolioIcon from '../../../assets/icons/svg/portfolio.svg';
import SideFilterComponent from "../../containers/searchResults/components/sideFilterComponent/sideFilterComponent";
import * as PropTypes from 'prop-types';

const styles = {
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
};


const services = [
    {
        name: 'Evénements',
        icon: eventsIcon
    },
    {
        name: 'Cours',
        icon: bookIcon
    },
    {
        name: 'Maison',
        icon: homeIcon
    },
    {
        name: 'Santé',
        icon: heartIcon
    },
    {
        name: 'Affaires',
        icon: portfolioIcon
    }
];


class SideNavFiltersComponent extends Component {


    render() {
        const {classes, publishedParams} = this.props;

        return (
            <div>
                <Drawer anchor="right"  open={this.props.open} onClose={this.props.toggleDrawer('right', false)}>
                   <div className="rightSideNav">
                       <IconButton color="secondary" aria-label="Menu"
                                   onClick={this.props.toggleDrawer('right', false)}>
                           <CloseIcon/>
                       </IconButton>
                   </div>
                    <SideFilterComponent {...publishedParams}/>
                </Drawer>
            </div>
        );
    }
}

SideNavFiltersComponent.propTypes = {
    publishedParams: PropTypes.object
};

export default withStyles(styles)(SideNavFiltersComponent);
