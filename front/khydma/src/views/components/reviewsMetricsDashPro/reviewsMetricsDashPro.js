import React, {Component} from 'react';
import './reviewsMetricsDashPro.css';
import * as PropTypes from 'prop-types';
import { PieChart, Pie, Sector } from 'recharts';

const data = [
    { name: '1 étoile', value: 12 },
    { name: '2 étoiles', value: 26 },
    { name: '3 étoiles', value: 10 },
    { name: '4 étoiles', value: 2 },
    { name: '5 étoiles', value: 56 },
];

const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const {
        cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle,
        fill, payload, percent, value,
    } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill={fill}
            />
            <Sector
                cx={cx}
                cy={cy}
                startAngle={startAngle}
                endAngle={endAngle}
                innerRadius={outerRadius + 6}
                outerRadius={outerRadius + 10}
                fill={fill}
            />
            <path d={`M${sx},${sy}L${mx},${my}L${ex},${ey}`} stroke={fill} fill="none" />
            <circle cx={ex} cy={ey} r={2} fill={fill} stroke="none" />
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} textAnchor={textAnchor} fill="#2ba84a">{`Avis ${value}`}</text>
            <text x={ex + (cos >= 0 ? 1 : -1) * 12} y={ey} dy={18} textAnchor={textAnchor} fill="#EB4000">
                {`(Pourcentage ${(percent * 100).toFixed(2)}%)`}
            </text>
        </g>
    );
};


class ReviewsMetricsDashPro extends Component {

    state = {
        activeIndex: 0,
    };

    onPieEnter = (data, index) => {
        this.setState({
            activeIndex: index,
        });
    };

    componentDidMount(): void {
        // this.cutIntroToShow();
    }

    cutIntroToShow = () => {
        let text = this.props.conversation.lastMessageId.text;
        let textWords = this.props.conversation.lastMessageId.text.split(" ");
        // console.log(this.state.textWords.length);

        if (textWords.length > 6) {
            let lastWord = textWords[6];
            let indexOfLastWord = text.indexOf(lastWord);
            this.setState({
                text: text.slice(0, indexOfLastWord) + "...",
            });
        } this.setState({
            text: text
        })
    };

    render() {
        const {isPro} = this.props;
        const {text} = this.state;
        // let image = isPro ? this.props.conversation.userId.profilePicture : this.props.conversation.proId.businessImagePath;
        return (
                <div className="metricsElement shadow">
                    <span className="insightTitle">Vos avis</span>
                    <PieChart width={400} height={400}>
                        <Pie
                            activeIndex={this.state.activeIndex}
                            activeShape={renderActiveShape}
                            data={data}
                            cx={200}
                            cy={200}
                            innerRadius={60}
                            outerRadius={80}
                            fill="#FFD700"
                            dataKey="value"
                            onMouseEnter={this.onPieEnter}
                        />
                    </PieChart>
                </div>
        );
    }
}

ReviewsMetricsDashPro.propTypes = {
    conversation: PropTypes.object,
    isPro: PropTypes.bool
};

export default ReviewsMetricsDashPro;
