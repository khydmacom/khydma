import React, {Component} from 'react';
import './sideNavComponent.css';
import {IconButton} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles'
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ChevronRight from '@material-ui/icons/ChevronRight';

import eventsIcon from '../../../assets/icons/svg/event.svg';
import bookIcon from '../../../assets/icons/svg/hardbound-book-variant.svg';
import heartIcon from '../../../assets/icons/svg/heartbeat.svg';
import homeIcon from '../../../assets/icons/svg/home.svg';
import portfolioIcon from '../../../assets/icons/svg/portfolio.svg';
import CloseIcon from '@material-ui/icons/Close';
import {Link} from '@reach/router';

import {connect} from "react-redux";
import {Misc} from "../../../config";
import ProfileHeaderSideNavComponent from "../profileHeaderSideNav/profileHeaderSideNavComponent";
import logoFull from '../../../assets/icons/final/full.png';


const styles = {
    list: {
        width: 250,
    },
    fullList: {
        width: 'auto',
    },
};

const settings = [
    {
        name: 'Modifier profil',
        link: 'profile'
    },
    {
        name: 'Messages',
        link: 'conversations'
    },
    {
        name: 'Notifications',
        link: 'notifications'
    }
];

const proSettings = [
    {
        name: 'Modifier le profil pro',
        link: 'pro'
    },
    {
        name: 'Messages pro',
        link: 'conversations-pro'
    },
    {
        name: 'Métriques',
        link: 'metrics'
    },
    {
        name: 'Galerie',
        link: 'gallery'
    },
    {
        name: 'Avis',
        link: 'reviews'
    }
];


class SideNavComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isMobile: false
        }
    }

    componentWillMount() {
        this.setViewType()
    };

    componentDidMount() {
        window.addEventListener("resize", this.setViewType);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    isMobile: true
                });
                return;
            case 'sm':
                this.setState({
                    isMobile: true
                });
                return;
            case 'md':
                this.setState({
                    isMobile: false
                });
                return;
            case 'lg':
                this.setState({
                    isMobile: false
                });
                return;
            case 'xl':
                this.setState({
                    isMobile: false
                });
                return;
            default:
                return;
        }
    };


    render() {
        const {classes, pathname, isLoggedIn, isPro} = this.props;

        return (
            <div>
                <Drawer
                    variant={(pathname.includes('dashboard') && !this.state.isMobile) ? "permanent" : "temporary"}
                    open={this.props.open} onClose={this.props.toggleDrawer('left', false)}>
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.props.toggleDrawer('left', false)}
                        onKeyDown={this.props.toggleDrawer('left', false)}
                        className="sideList"
                        ref={this.props.drawerRef}
                    >
                        {
                            pathname.includes('dashboard') ? <div style={{height: '2vh'}}/> : <div className="rightSideNav">
                                <IconButton color="secondary" aria-label="Menu"
                                            onClick={this.props.toggleDrawer('left', false)}>
                                    <CloseIcon/>
                                </IconButton>
                            </div>
                        }


                        <Link to={'/'}>
                            <img src={logoFull} alt={"Khydma.com logo"} className="logoHeadSideNav"/>
                        </Link>

                        <div className={classes.fullList}>

                            {/*hiding the signing buttons so they don't appear when the user is logged in*/}
                            {/*and showing the dashboard dedicated parts*/}
                            {isLoggedIn ? <>
                                    <ProfileHeaderSideNavComponent/>
                                    <Divider/>
                                    <br/>
                                    <span className="subTitle">Paramètres</span>
                                    <br/>
                                    <List>
                                        {settings.map((text, index) => (
                                            <Link to={`dashboard/${text.link}`} key={index + text.link}>
                                                <ListItem button key={text.name} className="listBtn">
                                                    <ListItemText primary={text.name}/>
                                                    <ListItemIcon>
                                                        <ChevronRight color="secondary"/>
                                                    </ListItemIcon>
                                                </ListItem>
                                            </Link>
                                        ))}
                                    </List>
                                </> :
                                <List>
                                    {[{name: 'S\'inscrire comme Pro', link: '/proform'}, {
                                        name: 'S\'inscrire',
                                        link: '/signup'
                                    }, {name: 'Se connecter', link: '/login'}].map((text, index) => (
                                        <Link to={`${text.link}`} key={text.name}>
                                            <ListItem button className="listBtn">
                                                <ListItemText primary={text.name}/>
                                                <ListItemIcon>
                                                    <ChevronRight color="secondary"/>
                                                </ListItemIcon>
                                            </ListItem>
                                        </Link>
                                    ))}
                                </List>
                            }
                            {isPro ? <>
                                    <Divider/>
                                    <br/>
                                    <span className="subTitle">Professionnel</span>
                                    <br/>
                                    <List>
                                        {proSettings.map((text, index) => (
                                            <Link to={`dashboard/${text.link}`} key={index + text.link}>
                                                <ListItem button key={text.name} className="listBtn">
                                                    <ListItemText primary={text.name}/>
                                                    <ListItemIcon>
                                                        <ChevronRight color="secondary"/>
                                                    </ListItemIcon>
                                                </ListItem>
                                            </Link>
                                        ))}
                                    </List>
                                </> :
                                <div/>
                            }
                            <Divider/>
                            <br/>
                            <span className="subTitle">Nos services</span>
                            <br/>
                            <List>
                                {Misc.headerServices.sort((x, y) => x.order - y.order).map((text, index) => (
                                    <Link to={`/categories?q=${text.name}`} key={text.name}>
                                        <ListItem button className="listBtn">
                                            <ListItemIcon>
                                                <img src={text.icon}/>
                                            </ListItemIcon>
                                            <ListItemText primary={text.name}/>
                                            <ListItemIcon>
                                                <ChevronRight color="secondary"/>
                                            </ListItemIcon>
                                        </ListItem>
                                    </Link>
                                ))}
                            </List>
                        </div>
                    </div>
                </Drawer>
            </div>
        );
    }
}

let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            isPro: state.currentUsersReducer.isPro,
            user: state.currentUsersReducer.user,
            pathname: state.rootsReducer.pathname,
        }
    } else
        return {}
};

export default connect(statesToProps, {})(withStyles(styles)(SideNavComponent));
