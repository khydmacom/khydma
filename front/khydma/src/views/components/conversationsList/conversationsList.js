import React, {Component} from 'react';
import './conversationsListStyle.css';
import * as PropTypes from 'prop-types';
import {ConversationElementComponent} from "../conversationElementComponent";
import {ButtonBase} from "@material-ui/core";
import {Link} from "@reach/router";

class ConversationsList extends Component {

    constructor(props) {
        super(props);

        this.state = {
            _conversations: [],
            conversations: []
        }
    }

    render() {
        const {isPro, conversations} = this.props;
        console.log(conversations);
        // const {} = this.state;
        return (
            <div className="conversationsListMainDiv shadow">
                <div className="cnvElementHolder">
                    <span className="sectionTitle">Discussions</span>
                </div>
                <div className="sectionTitleSeparator"/>
                <div className="conversationsContainer">
                    {
                        conversations && conversations.length > 0 ?
                            conversations.map((cnv, index) => {
                                return <Link key={index} to={`/dashboard/conversations${isPro ? '-pro' : ''}?${isPro ? 'userId=' + cnv.userId._id : 'proId=' + cnv.proId._id}`}>
                                    <ButtonBase>
                                        <div className="cnvElementHolder"
                                             // style={!cnv.seen ? {backgroundColor: "rgba(174,255,158,0.37)"} : {}}
                                        >
                                            <ConversationElementComponent conversation={cnv} isPro={isPro}/>
                                        </div>
                                    </ButtonBase>
                                </Link>
                            }) : <div className="noConvos">
                                <span className="noConvosText">Vous n'avez pas de conversations pour le moment.</span>
                            </div>
                    }
                </div>
            </div>
        );
    }
}

ConversationsList.propTypes = {
    avatars: PropTypes.array,
    conversations: PropTypes.array,
    isPro: PropTypes.bool,
    pro: PropTypes.object,
    // fullScreen: PropTypes.bool
};

export default ConversationsList;
