import React, {Component} from 'react';
import './callsMetricsDashPro.css';
import * as PropTypes from 'prop-types';
import {
    BarChart, Bar, Cell, XAxis, YAxis, CartesianGrid, Tooltip, Legend,
} from 'recharts';

const data = [
    {
        name: 'Dimanche', Appels: 40, VueDeNumero: 24
    },
    {
        name: 'Lundi', Appels: 30, VueDeNumero: 13
    },
    {
        name: 'Mardi', Appels: 20, VueDeNumero: 98
    },
    {
        name: 'Mercredi', Appels: 20, VueDeNumero: 39
    },
    {
        name: 'Jeudi', Appels: 10, VueDeNumero: 48
    },
    {
        name: 'Vendredi', Appels: 90, VueDeNumero: 38
    },
    {
        name: 'Samedi', Appels: 30, VueDeNumero: 43
    },
];


class CallsMetricsDashPro extends Component {

    constructor(props) {
        super(props);

        this.state = {
            text: "",
            textWords: []
        };
    }

    componentDidMount(): void {
        // this.cutIntroToShow();
    }

    cutIntroToShow = () => {
        let text = this.props.conversation.lastMessageId.text;
        let textWords = this.props.conversation.lastMessageId.text.split(" ");
        // console.log(this.state.textWords.length);

        if (textWords.length > 6) {
            let lastWord = textWords[6];
            let indexOfLastWord = text.indexOf(lastWord);
            this.setState({
                text: text.slice(0, indexOfLastWord) + "...",
            });
        } this.setState({
            text: text
        })
    };

    render() {
        const {isPro} = this.props;
        const {text} = this.state;
        // let image = isPro ? this.props.conversation.userId.profilePicture : this.props.conversation.proId.businessImagePath;
        return (
                <div className="metricsElement shadow">
                    <span className="insightTitle">Vos appels et votre numéro de téléphone révèlent</span>

                    <BarChart
                        width={500}
                        height={300}
                        data={data}
                        margin={{
                            top: 5, right: 30, left: 20, bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip />
                        <Legend />
                        <Bar dataKey="VueDeNumero" fill="#8884d8" />
                        <Bar dataKey="Appels" fill="#82ca9d" />
                    </BarChart>
                </div>
        );
    }
}

CallsMetricsDashPro.propTypes = {
    conversation: PropTypes.object,
    isPro: PropTypes.bool
};

export default CallsMetricsDashPro;
