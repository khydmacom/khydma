import React, {Component} from 'react';
import './profileHeaderSideNavComponent.css';
import PropTypes from 'prop-types';
import pictureFiler from '../../../assets/images/no.png';
import {connect} from "react-redux";
import PersonPinCircle from '@material-ui/icons/PersonPinCircle';




class ProfileHeaderSideNavComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {classes, isLoggedIn, user} = this.props;

        return (
            <div className="profileHeaderNavMainDiv">
                <div className="vertSpacing"/>
                <div className="avatarHeaderPicture" style={{
                    backgroundImage: "url('" + (user.profilePicture ? user.profilePicture : pictureFiler) + "')",
                    height: 15 + 'vh',
                    width: 15 + 'vh',
                    borderRadius: (15 / 2) + 'vh'
                }}/>
                <div className="vertSpacing"/>
                <span className="sideNavName">{user ? (user.lastName ? (user.name + " " + user.lastName) : user.name) : "Profile"}</span>
                {/*<div className="vertSpacing"/>*/}
                {user.locString ? <span className="sideNavName loc"><PersonPinCircle className="sideNavName loc"/>{user.locString}</span> : <div/>}
                <div className="vertSpacing"/>

            </div>
        );
    }
}

ProfileHeaderSideNavComponent.propTypes = {
    parameters: PropTypes.any
};
let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            user: state.currentUsersReducer.user
        }
    } else
        return {}
};

export default connect(statesToProps, {}) (ProfileHeaderSideNavComponent);
