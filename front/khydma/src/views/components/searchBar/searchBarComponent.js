import React, {Component} from 'react';
import './searchBarComponent.css';
import * as PropTypes from 'prop-types';
import {Button, Fab, IconButton, MenuItem, Popper} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import * as Autosuggest from "react-autosuggest";
import InputBase from "@material-ui/core/InputBase";
import Divider from "@material-ui/core/Divider";
import {Link} from "@reach/router";
import Search from '@material-ui/icons/Search';
import match from "autosuggest-highlight/match";
import parse from "autosuggest-highlight/parse";


const classes = {
    container: {
        position: 'relative',
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        // marginTop: theme.spacing(1),
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    }
};

export default class SearchBarComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    getSuggestionValue = (value) => value.label;

    renderSuggestion = (suggestion, {query, isHighlighted}) => {
        const matches = match(suggestion.label, query);
        const parts = parse(suggestion.label, matches);

        return <MenuItem selected={isHighlighted} component="div">
            <div>
                {parts.map(part => (
                    <span key={part.text} style={{fontWeight: part.highlight ? 500 : 400}}>
            {part.text}
          </span>
                ))}
            </div>
        </MenuItem>
    };

    renderInputComponent = inputProps => (
        <Paper className={this.props.filter ? 'filterRoot' : 'root'} elevation={3}>
            <InputBase {...inputProps} className={`${this.props.filter ? 'inputFilter' : 'input'}`}
            />
            <Divider className="divider"/>
            <Link
                to={`/${this.props.categoriesPage ? 'categories' : 'search'}?q=${this.props.value ? this.props.value : " "}${this.props.lat ? '&lat=' + this.props.lat : ''}${this.props.lng ? '&lng=' + this.props.lng : ''}`}
                state={{selectedCat: this.props.selectedCat}}>
                {this.props.filter ? <IconButton color="secondary" aria-label="Search">
                    <Search/>
                </IconButton> : <>
                    <div className="searchIcon">
                        <Fab size={"small"} color="secondary"
                             aria-label="Search"
                        >
                            <Search/>
                        </Fab>
                    </div>
                    <div className="searchText">
                        <Button variant="contained" color="secondary">
                            Rechercher
                        </Button>
                    </div>
                </>}
            </Link>
        </Paper>
    );

    render() {
        const {suggestions, anchorEl, categoriesPage, value, onChange, setAnchorEl, onSuggestionsFetchRequested, onSuggestionsClearRequested} = this.props;
        const inputProps = {
            placeholder: `${categoriesPage ? 'Quel service recherchez-vous ?' : 'Tapez \"médecin\" ou \"plombier\" etc...'}`,
            value,
            onChange: onChange,
            inputRef: setAnchorEl
        };

        return (
            <Autosuggest
                renderInputComponent={this.renderInputComponent}
                suggestions={suggestions}
                onSuggestionsFetchRequested={onSuggestionsFetchRequested}
                onSuggestionsClearRequested={onSuggestionsClearRequested}
                getSuggestionValue={this.getSuggestionValue}
                renderSuggestion={this.renderSuggestion}
                inputProps={inputProps}
                theme={{
                    container: classes.container,
                    suggestionsContainerOpen: classes.suggestionsContainerOpen,
                    suggestionsList: classes.suggestionsList,
                    suggestion: classes.suggestion,
                }}
                renderSuggestionsContainer={options => (
                    <Popper anchorEl={anchorEl} open={Boolean(options.children)}
                            style={{backgroundColor: '#fff', zIndex: 10}}
                            className="manuItemsStyle">
                        <Paper
                            square
                            {...options.containerProps}
                            style={{width: anchorEl ? anchorEl.clientWidth : undefined}}
                        >
                            {options.children}
                        </Paper>
                    </Popper>

                )}
            />
        );
    }
}

SearchBarComponent.propTypes = {
    suggestions: PropTypes.array.isRequired,
    results: PropTypes.array.isRequired,
    open: PropTypes.bool.isRequired,
    anchorEl: PropTypes.any,
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    setAnchorEl: PropTypes.func.isRequired,
    onSuggestionsFetchRequested: PropTypes.func.isRequired,
    onSuggestionsClearRequested: PropTypes.func.isRequired,
    selectedCat: PropTypes.object,
    handleMenuClick: PropTypes.func.isRequired,
    categories: PropTypes.array.isRequired,
    filter: PropTypes.bool,
    locStringFilter: PropTypes.string,
    lat: PropTypes.string,
    lng: PropTypes.string,
    categoriesPage: PropTypes.bool
};
