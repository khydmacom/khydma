import React, {Component} from 'react';
import './loginComponent.css';
import classNames from 'classnames';
import {createMuiTheme, MuiThemeProvider, withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/es/Button/Button";
import {Formik} from "formik";
import {Misc} from "../../../config";
import {AuthActions} from '../../reducers/auth'
import {CurrentUsersActions} from '../../reducers/currentUsers'
import {connect} from "react-redux";
import {Link} from "@reach/router";
import {SnackBarContentWrapper} from "../../components";
import {Snackbar} from "@material-ui/core";
import {RootsActions} from "../../reducers/roots";

const {login} = AuthActions;
const {getCurrentProfile} = CurrentUsersActions;
const {updateRoot} = RootsActions;

class LoginComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            snackBarMessage: "",
            snackVariant: "success"
        }
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value
        });
    };

    setOpen = (open: boolean) => {
        this.setState(state => ({
            open: open
        }))
    };

    setSnackMessage = (snackBarMessage: string) => {
        this.setState(state => ({
            snackBarMessage: snackBarMessage
        }))
    };

    setSnackVariant = (snackVariant: string) => {
        this.setState(state => ({
            snackVariant: snackVariant
        }))
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setOpen(false);
    };

    openSnackBar = (snackBarMessage: string, variant: "success" | "warning" | "error" | "info") => {
        this.setSnackVariant(variant);
        this.setSnackMessage(snackBarMessage);
        this.handleClick();
    };

    handleClick = () => {
        this.setOpen(true);
    };

    goToSignup() {

    }

    forgotPass() {

    }

    render() {
        const {validateLoginForm} = Misc;
        const {open, snackBarMessage, snackVariant} = this.state;
        return (
            <div className="loginComponent">
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={open}
                    autoHideDuration={4000}
                    onClose={this.handleClose}
                >
                    <SnackBarContentWrapper
                        onClose={this.handleClose}
                        variant={snackVariant}
                        message={snackBarMessage}
                    />
                </Snackbar>
                <div className="fields">
                    <span className="signingTitle">Bienvenue</span>
                    <Formik
                        initialValues={{
                            email: undefined,
                            password: undefined
                        }}
                        validate={validateLoginForm}
                        onSubmit={(values, {setSubmitting}) => {
                            this.props.login(values, (res) => {
                                console.log("login succ = ", res);
                                this.props.getCurrentProfile((profile) => {
                                    console.log(profile);
                                }, (err) => {
                                    console.log("err = ", err);
                                });
                                console.log(this.props.pathToGetBackTo)
                                this.props.navigate(this.props.pathToGetBackTo ? this.props.pathToGetBackTo : (this.props.proForm ? '/proform' : '/'), {replace: true});
                                setSubmitting(false);
                            }, (err) => {
                                console.log("login err = ", err);
                                this.openSnackBar(err.message, "error");
                                setSubmitting(false);
                            })
                        }}
                    >
                        {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                          }) => (
                            <MuiThemeProvider theme={Misc.FORM_THEME}>
                                <form onSubmit={handleSubmit} className="fields noBorder">

                                    <TextField
                                        id="outlined-email-input"
                                        label="E-mail"
                                        className="text"
                                        type="email"
                                        name="email"
                                        // autoComplete="email"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.email && touched.email && errors.email)}
                                        helperText={errors.email && touched.email && errors.email}
                                        value={values.email || ''}
                                    />
                                    <TextField
                                        id="outlined-password-input"
                                        label="Mot de pass"
                                        className="text"
                                        type="password"
                                        name="password"
                                        color="secondary"
                                        // autoComplete="password"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.password || ''}
                                        error={!!(errors.password && touched.password && errors.password)}
                                        helperText={errors.password && touched.password && errors.password}
                                    />
                                    <Button onClick={this.forgotPass.bind(this)} color="primary">Mot de passe oublié
                                        ?</Button>
                                    <div className="buttonContainer">
                                        <Button type="submit" disabled={Object.keys(errors).length > 0 || isSubmitting}
                                                color="primary" variant="contained" className="signBtn">
                                            Se connecter
                                        </Button>
                                    </div>
                                </form>
                            </MuiThemeProvider>
                        )}
                    </Formik>

                    {/*<div className="loginSocialChoice">Ou connectez-vous avec</div>

                    <div className="buttonContainer">
                        <Button variant="contained" className="signBtn fb">
                            Facebook
                        </Button>
                    </div>
                    <div className="buttonContainer">
                        <Button variant="contained" className="signBtn gg">
                            Google
                        </Button>
                    </div>*/}
                </div>
                <div className="switchSign">
                    <span>Vous n'avez pas de compte?  </span>
                    <Link to="/signup">
                        <Button onClick={this.goToSignup.bind(this)} color="secondary">S'inscrire</Button>
                    </Link>
                </div>
            </div>
        );
    }
}

let statesToProps = (state, props) => {
    if (state.proFormFillingReducer && state.rootsReducer) {
        return {
            proForm: state.proFormFillingReducer.proForm,
            pathToGetBackTo: state.rootsReducer.pathToGetBackTo
        }
    } else
        return {}
};

export default connect(statesToProps, {login, getCurrentProfile, updateRoot})(LoginComponent);
