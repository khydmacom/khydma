import React, {Component} from 'react';
import './HomeComponent.css';
import {HomeMainHeadComponent, PlayStoreComponent, PopGridComponent, PopSliderComponent} from "./components";
import {Helmet} from 'react-helmet';
import {connect} from "react-redux";
import {RootsActions} from '../../reducers/roots';
import {prosActions} from '../../reducers/pros';
import {categoriesActions} from '../../reducers/categories';
import {Misc} from "../../../config";

const {updateRoot} = RootsActions;
const {getPros} = prosActions;

class HomeComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            filtredCatsAndPros: []
        }
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
        this.props.getPros(this.isGetProsSuccess, this.isGetCatsError, {limit: 10});
    }

    isGetProsSuccess = (succ) => {
        // console.log(succ);
        let _array = Misc.reorderProsForHome(succ);
        this.setState({
            filtredCatsAndPros: _array
        })
    };


    render() {
        const {allCategories, geoCodingResults} = this.props;
        const {filtredCatsAndPros} = this.state;
        // console.log(filtredCatsAndPros);
        return allCategories ?
            <div className="homeComponent">
                <Helmet>
                    <title>Khydma - Médecins, avocats, comptables et autres professionnels de Tunisie.</title>
                    <meta name="robots" content="index, follow"/>
                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
                    <meta name="description"
                          content="Médecins, avocats, comptables et autres professionnels de Tunisie."/>
                    <meta name="keywords"
                          content="Annuaire Tunisie, Entreprises, PAGES JAUNES, PAGES BLANCHES, ANNUAIRE INVERSE, ANNUAIRE,annuaire téléphonique,
                           REPERTOIRE, CATALOGUE, CLASSEMENT, CLASSIFICATION, LISTE, PLAN, CARTE, COORDONNEE, ADRESSE, E-MAIL, FAX, TELECOPIE,
                           TELEPHONE, TELEPHONIQUE, CONTACT, RENSEIGNEMENT, INFORMATION, REGION, VILLE, MARRAKECH, ADMINISTRATION, COMMUNICATION,
                            COMMERÇANT, COMMERCE, SERVICE, PUBLICITE, MAGASIN, INDUSTRIE, INDUSTRIEL, IMPORT, EXPORT, IMPORTATEUR, EXPORTATEUR, FABRICANT,
                             BOUTIQUE, USINE, TOURISME, HOTEL, HOTELLERIE, ECONOMIE, ECONOMIQUE, B2B, B2C, BUSINESS TO BUSINESS, BUSINESS TO CONSUMER, BANQUE DE DONNEES,
                              BASE DE DONNEES, ACTIVITE, PROFESSION, PROFESSIONEL, SOCIETE, ENTREPRISE, PARTICULIER, FIRME, FICHIER, AFFAIRE, WEB, SITE INTERNET, annuaire,adresse,telephone,coordonnee,page,jaune,tunisie,
                          maghreb,docteur,pharmacie,avocat,hotel,voyage,agence,banque,architecte,
                          cabinet,dentiste,auto,voiture,societe,entreprise,etablissement,organisme,administration,
                          publique,restaurant,municipalite,ambassade,consulat,informatique,marketing,bureautique,
                          clinique,comptable,meuble,export,immobilier,location,vente,materiel,construction,laboratoire,
                          transport,centre,tunisair,telecom,orange,ooredoo,mosaique,depannage,taxi,alarme,surveillance,fabricant"/>

                    <meta property="og:title" content="Khydma.com"/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:url" content="https://khydma.com"/>
                    <meta property="og:image" content="https://khydma.com/images/logo.png"/>
                    <meta property="og:description"
                          content="Médecins, avocats, comptables et autres professionnels de Tunisie."/>
                </Helmet>
                <HomeMainHeadComponent categories={allCategories} geoCodingResults={geoCodingResults}/>
                {/*to show when large screen*/}
                {
                    filtredCatsAndPros.length > 4 ?
                        <PopSliderComponent catgeories={filtredCatsAndPros}/> : <div/>
                }
                <div style={{height: '4vh'}}/>
                <PlayStoreComponent/>
                <div style={{height: '4vh'}}/>
            </div> : <div/>;

    }
}

let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.categoriesReducer && state.locationsReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            allCategories: state.categoriesReducer.allCategories,
            geoCodingResults: state.locationsReducer.geoCodingResults
        }
    } else
        return {}
};

export default connect(statesToProps, {updateRoot, getPros})(HomeComponent);
