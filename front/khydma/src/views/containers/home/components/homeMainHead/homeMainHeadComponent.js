import React, {Component} from 'react';
import './homeMainHead.css';
import * as PropTypes from 'prop-types';


import {SearchBarComponent} from "../../../../components";
import {Misc} from "../../../../../config";

const {ServicesRenderer} = Misc;


class HomeMainHeadComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            results: [],
            open: false,
            anchorEl: null,
            value: '',
            suggestions: [],
            selectedCat: null
        }
    }

    onChange = (event, {newValue}) => {
        this.setState({
            value: newValue
        });
    };

    setAnchorEl = newValue => {
        console.log(newValue)
        this.setState({
            anchorEl: newValue
        });
    };

    // method to get suggestions
    getSuggestions = (event) => {
        let value = event.target ? event.target.value : event;
        let results = value ? [...this.props.categories].filter((cat) => {
            let search = value;
            let exp = new RegExp(search.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
            return exp.test(cat.label.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
        }) : [];

        return results
    };

    handleMenuClick = (cat) => {
        this.setState({selectedCat: cat})
    };

    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };



    componentDidMount(): void {

    }

    render() {
        const {value, suggestions, anchorEl, results, open, selectedCat} = this.state;
        const {geoCodingResults} = this.props;

        return (
            <div className="mainHeadDiv">
                <div className="mainViewContainer columnAlign">
                    <div className="logoSpacer"/>
                    <div className="logoHead"/>
                    <h1 className="title">Trouvez le meilleur professionnel pour n'importe quel besoin</h1>
                    {/*// search bar component here*/}
                    <SearchBarComponent suggestions={suggestions} results={results} open={open}
                                        anchorEl={anchorEl} value={value} onChange={this.onChange}
                                        setAnchorEl={this.setAnchorEl} filter={false} lat={geoCodingResults ? geoCodingResults.loc.lat + '' : ''}
                                        lng={geoCodingResults ? geoCodingResults.loc.lng + '' : ''}
                                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                        selectedCat={selectedCat} handleMenuClick={this.handleMenuClick}
                                        categories={this.props.categories}/>
                    <div className="spacing"/>
                    {/*<div className="actionIconsWrapper">*/}
                        <div className="actionIcons">
                            <ServicesRenderer/>
                        </div>
                    {/*</div>*/}
                </div>
            </div>
        );
    }
}

HomeMainHeadComponent.propTypes = {
    categories: PropTypes.array,
    geoCodingResults: PropTypes.object
};

export default HomeMainHeadComponent;
