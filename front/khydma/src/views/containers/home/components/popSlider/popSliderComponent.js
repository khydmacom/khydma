import React, {Component} from 'react';
import './popSliderComponent.css';
import {PopSliderCardComponent} from "../index";
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import {Misc} from "../../../../../config";
import * as PropTypes from 'prop-types';
import {Link} from '@reach/router';

class PopSliderComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            carouselItem: 3
        }
    }

    componentWillMount() {
        this.carouselItemsNumber();
    };

    componentDidMount() {
        window.addEventListener("resize", this.carouselItemsNumber);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.carouselItemsNumber);
    }

    carouselItemsNumber = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    carouselItem: 2
                });
                return;
            case 'sm':
                this.setState({
                    carouselItem: 2
                });
                return;
            case 'md':
                this.setState({
                    carouselItem: 3
                });
                return;
            case 'lg':
                this.setState({
                    carouselItem: 3
                });
                return;
            case 'xl':
                this.setState({
                    carouselItem: 3
                });
                return;
            default:
                return;
        }
    };

    render() {
        const {catgeories} = this.props;
        // console.log(catgeories)
        return (
            <div className="mainSlidersDiv">
                <div className="mainViewContainer columnAlign">
                    <h1 className="popTitle title">Explorez nos services populaires</h1>

                    <OwlCarousel
                        id="recommended"
                        className="owl-theme"
                        center={true}
                        loop={true}
                        autoplay={true}
                        autoplayTimeout={2666}
                        autoplayHoverPause={false}
                        items={this.state.carouselItem}
                        dots={false}
                    >
                        {
                            catgeories ? catgeories.map((cat, idx) => {
                                return <Link to={`/search?q=${cat.label}`} key={idx}> <PopSliderCardComponent category={cat}/></Link>
                            }) : <div/>
                        }
                    </OwlCarousel>
                </div>
            </div>
        );
    }
}

PopSliderComponent.propTypes = {
    catgeories: PropTypes.array
};

export default PopSliderComponent;
