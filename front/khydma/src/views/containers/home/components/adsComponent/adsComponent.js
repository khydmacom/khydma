import React, {Component} from 'react';
import './adsComponent.css';
import {PopSliderCardComponent} from "../index";
import AdCardComponent from "../adCardComponent/adCardComponent";

class AdsComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            ads: [1, 1, 1]
        }
    }

    render() {
        return (
            <div className="adsMainDiv">
                {
                    this.state.ads.map((ad, index) => {
                        return <AdCardComponent key={index}/>
                    })
                }
            </div>
        );
    }
}

export default AdsComponent;
