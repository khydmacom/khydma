import {HomeMainHeadComponent} from './homeMainHead';
import {PopSliderComponent} from './popSlider';
import {PopSliderCardComponent} from './popSliderCard';
import {PopGridComponent} from './popGrid';
import {PlayStoreComponent} from './playStore';
import {AdsComponent} from './adsComponent';
import {AdCardComponent} from './adCardComponent';

export {
    HomeMainHeadComponent,
    PopSliderComponent,
    PopSliderCardComponent,
    PopGridComponent,
    PlayStoreComponent,
    AdsComponent,
    AdCardComponent
}
