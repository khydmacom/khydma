import React, {Component} from 'react';
import './popGridComponent.css';
import {PopSliderCardComponent} from "../index";

class PopGridComponent extends Component {
    render() {
        return (
            <div className="mainSlidersDiv">
                <h1 className="title small">Explorez nos services populaires</h1>
                <div className="cardsGid">
                    <div className="cardDiv">
                        <PopSliderCardComponent/>
                    </div>
                    <div className="cardDiv">
                        <PopSliderCardComponent/>
                    </div>
                    <div className="cardDiv">
                        <PopSliderCardComponent/>
                    </div>
                    <div className="cardDiv">
                        <PopSliderCardComponent/>
                    </div>
                    <div className="cardDiv">
                        <PopSliderCardComponent/>
                    </div>
                </div>
            </div>
        );
    }
}

export default PopGridComponent;
