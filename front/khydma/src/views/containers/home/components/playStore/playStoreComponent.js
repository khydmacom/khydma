import React, {Component} from 'react';
import './playStoreComponent.css';
import {PopSliderCardComponent} from "../index";

class PlayStoreComponent extends Component {
    render() {
        return (
            <div className="bannerDiv">
                <div className="mainViewContainer rowAlign">
                    <div className="bannerImage"/>
                    <div>
                        <span className="title small banner popTitle"><b>Téléchargez notre application</b></span>

                        <div className="bannerColumnFlex">
                            <span className="playstoreBtn"/>
                            {/*<span className="applestoreBtn"></span>*/}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default PlayStoreComponent;
