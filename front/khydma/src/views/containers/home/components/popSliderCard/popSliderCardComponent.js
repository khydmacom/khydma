import React, {Component} from 'react';
import './popSliderCardComponent.css';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import CardContent from '@material-ui/core/CardContent';
import {AvatarsViewerComponent} from "../../../../components";
import * as PropTypes from 'prop-types';
import ProsApi from '../../../../reducers/pros/prosApi';

const {getIndexedProByQuery} = ProsApi;

/*
*
* {
                    "name": "Miyah Myles",
                    "email": "miyah.myles@gmail.com",
                    "position": "Receptionist",
                    "photo": "https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=707b9c33066bf8808c934c8ab394dff6"
                },
                {
                    "name": "June Cha",
                    "email": "june.cha@gmail.com",
                    "position": "Product Designer",
                    "photo": "https://randomuser.me/api/portraits/women/44.jpg"
                },
                {
                    "name": "Iida Niskanen",
                    "email": "iida.niskanen@gmail.com",
                    "position": "Call Center Representative",
                    "photo": "https://randomuser.me/api/portraits/women/68.jpg"
                },
                {
                    "name": "Renee Sims",
                    "email": "renee.sims@gmail.com",
                    "position": "Sales Manager",
                    "photo": "https://randomuser.me/api/portraits/women/65.jpg"
                },
                {
                    "name": "Jonathan Nu\u00f1ez",
                    "email": "jonathan.nu\u00f1ez@gmail.com",
                    "position": "Sales",
                    "photo": "https://randomuser.me/api/portraits/men/43.jpg"
                },
                {
                    "name": "Sasha Ho",
                    "email": "sasha.ho@gmail.com",
                    "position": "Software Engineer",
                    "photo": "https://images.pexels.com/photos/415829/pexels-photo-415829.jpeg?h=350&auto=compress&cs=tinysrgb"
                },
                {
                    "name": "Abdullah Hadley",
                    "email": "abdullah.hadley@gmail.com",
                    "position": "Office Assistant",
                    "photo": "https://images.unsplash.com/photo-1507003211169-0a1dd7228f2d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=a72ca28288878f8404a795f39642a46f"
                },
                {
                    "name": "Veeti Seppanen",
                    "email": "veeti.seppanen@gmail.com",
                    "position": "Office Assistant",
                    "photo": "https://randomuser.me/api/portraits/men/97.jpg"
                },
                {
                    "name": "Bonnie Riley",
                    "email": "bonnie.riley@gmail.com",
                    "position": "Receptionist",
                    "photo": "https://randomuser.me/api/portraits/women/26.jpg"
                },
                {
                    "name": "Thomas Stock",
                    "email": "thomas.stock@gmail.com",
                    "position": "Senior Developer",
                    "photo": "https://tinyfac.es/data/avatars/B0298C36-9751-48EF-BE15-80FB9CD11143-500w.jpeg"
                }
* */


class PopSliderCardComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            pros: []
        }
    }

    componentDidMount(): void {
        getIndexedProByQuery({categoryName: this.props.category.label}, this.prosSucc)
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if (nextProps.category.label !== this.props.category.label) {
            getIndexedProByQuery({categoryName: nextProps.category.label}, this.prosSucc)
        }
    }

    prosSucc = (succ, data, err) => {
        if (succ)
            this.setState({
                pros: data
            });
        else this.prosErr(err);
    };

    prosErr = (err) => {
        console.log(err)
    };

    render() {
        const {category} = this.props;
        const {pros} = this.state;

        return category ? (
            <Card className="card" elevation={3}>
                {
                    category.imagePath ? <CardMedia
                        className="media"
                        image={category.imagePath}
                        title="CategoryImage"
                    /> : <div/>
                }

                <CardContent>
                    <div className="contentStyle">
                        <div>
                            <span className="cardTitle">{category.label}</span>
                        </div>
                        {pros && pros.length > 0 ? <div className="activeMembers">
                            <div>
                                <span className="counter">{pros.length} professionnel</span>
                            </div>
                            <div className="grow"/>
                            <div className="avatars">
                                <AvatarsViewerComponent avatars={pros}/>
                            </div>
                        </div> : <div/>}
                    </div>
                </CardContent>

            </Card>
        ) : <div/>;
    }
}

PopSliderCardComponent.propTypes = {
    category: PropTypes.object
};


export default PopSliderCardComponent;
