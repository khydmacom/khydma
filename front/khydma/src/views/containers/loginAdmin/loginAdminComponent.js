import React, {Component} from 'react';
import './loginAdminComponent.css';
import {MuiThemeProvider} from '@material-ui/core/styles';

import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/es/Button/Button";
import {Misc} from "../../../config";
import {Formik} from "formik";
import {AuthActions} from '../../reducers/auth'
import {CurrentUsersActions} from '../../reducers/currentUsers'
import {connect} from "react-redux";
import {Link} from "@reach/router";

const {login} = AuthActions;
const {checkIfCurrentIsAdmin} = CurrentUsersActions;


class LoginAdminComponent extends Component {
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    goToSignup() {

    }

    forgotPass() {

    }

    render() {
        const {validateLoginForm} = Misc;

        return (
            <div className="loginComponent">
                <div className="logo">
                    <Link to="/">
                        <Button color="inherit" aria-label="Menu">
                            <div className="logoButton"/>
                        </Button>
                    </Link>
                </div>
                <div className="fields">
                    <span className="signingTitle">Bienvenue</span>
                    <Formik
                        initialValues={{
                            email: undefined,
                            password: undefined
                        }}
                        validate={validateLoginForm}
                        onSubmit={(values, {setSubmitting}) => {
                            this.props.login(values, (res) => {
                                this.props.checkIfCurrentIsAdmin((_res) => {
                                    this.props.navigate('/admin');
                                });
                                setSubmitting(false);
                            }, (err) => {
                                console.log("login err = ", err);
                                setSubmitting(false);
                            })
                        }}
                    >
                        {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                          }) => (
                            <MuiThemeProvider theme={Misc.FORM_THEME}>
                                <form onSubmit={handleSubmit} className="fields noBorder">

                                    <TextField
                                        id="outlined-email-input"
                                        label="E-mail"
                                        className="text"
                                        type="email"
                                        name="email"
                                        // autoComplete="email"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.email && touched.email && errors.email)}
                                        helperText={errors.email && touched.email && errors.email}
                                        value={values.email || ''}
                                    />
                                    <TextField
                                        id="outlined-password-input"
                                        label="Mot de pass"
                                        className="text"
                                        type="password"
                                        name="password"
                                        color="secondary"
                                        // autoComplete="password"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.password || ''}
                                        error={!!(errors.password && touched.password && errors.password)}
                                        helperText={errors.password && touched.password && errors.password}
                                    />
                                    <div className="buttonContainer">
                                        <Button type="submit" disabled={Object.keys(errors).length > 0 || isSubmitting}
                                                color="primary" variant="contained" className="signBtn">
                                            Se connecter
                                        </Button>
                                    </div>
                                </form>
                            </MuiThemeProvider>
                        )}
                    </Formik>
                </div>
            </div>
        );
    }
}

let statesToProps = (state, props) => {
    /*if (state.touristsReducer.data && state.touristsReducer.allUserFetched) {
        return {
            usersKeysArray: Object.keys(state.touristsReducer.data),
            currentUsers: state.touristsReducer.data
        }
    } else */
    return {}
};

export default connect(statesToProps, {login, checkIfCurrentIsAdmin})(LoginAdminComponent);
