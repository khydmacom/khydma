import React, {Component} from 'react';
import './mainContainerRoutesNesterComponent.css';
import {Misc} from "../../../config";

class MainContainerRoutesNesterComponent extends Component {

    constructor(props) {
        super(props);

    }

    componentDidMount() {
    }

    render() {
        const {params} = this.props.match;
        const {nestedRoute} = params;

        return (
            <>
                {Misc.mainContainerRouter(nestedRoute, null)}
            </>
        );
    }
}

export default MainContainerRoutesNesterComponent;
