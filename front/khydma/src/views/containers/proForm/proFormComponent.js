import React, {Component} from 'react';
import './proFormComponent.css';
import {MuiThemeProvider, withStyles} from '@material-ui/core/styles';
import * as PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/es/Button/Button";
import {connect} from "react-redux";
import {prosActions} from '../../reducers/pros';
import {categoriesActions} from '../../reducers/categories';
import {LocationsActions} from '../../reducers/locations';
import {RootsActions} from '../../reducers/roots';
import {ButtonBase, FormControl, InputLabel, MenuItem, OutlinedInput, Select, Snackbar} from "@material-ui/core";
import ReactDOM from "react-dom";
import {CONST, Misc} from '../../../config'
import {SnackBarContentWrapper} from "../../components/snackBarContentWrapper";
import CircularProgress from "@material-ui/core/CircularProgress";
import {RequestLoginDialogWrapper} from "../../dialogs/requestLoginDialogWrapper";
import {CurrentUsersActions} from "../../reducers/currentUsers";

const {newPro, updateProImage, getProMe} = prosActions;
const {getCurrentProfile, checkIfCurrentIsPro} = CurrentUsersActions;
const {getCategories} = categoriesActions;
const {updateToGetBack, updateRoot} = RootsActions;
const {handleGeoCodingRequest} = LocationsActions;
const {API_OSM_KEY, OSM_AUTOCOMPLETE_BASE_API, COUNTRY_CODE} = CONST;

const searchId = "__choose_loc__";

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginTop: theme.spacing.unit,
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
});

class ProFormComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            componentClasses: ['fields', 'restOfForm'],
            componentClassesThree: ['fields', 'restOfFormTwo'],
            componentClassesTwo: ['fields'],
            labelWidth: 0,
            loc: [],
            locationTitle: null,
            reorderedCategories: [],
            openLogin: false
        };

        this.nextStep = this.nextStep.bind(this);
        this.prevStep = this.prevStep.bind(this);
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    componentDidMount(): void {
        this.props.updateRoot(this.props);
        this.setUserData();

        this.setState({
            labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
        });
        this.props.getCategories(this.successCb, this.errCb);
        if (this.props.loc) {
            this.setState({
                loc: this.props.loc,
                locString: this.props.locString,
                locationTitle: this.props.locationTitle
            })
        }
        if (localStorage.getItem('proformData')) {
            // updateToGetBack is used to save the current root in case
            // we are planning to get back to it after a specific action

            this.props.updateToGetBack('');
            this.setState(JSON.parse(localStorage.getItem('proformData')));
        }
    }

    initGeoAutoComplete = (props) => {
        const autoComplete = new props.osmAutocomplete.OsmNamesAutocomplete(searchId, `${OSM_AUTOCOMPLETE_BASE_API}${COUNTRY_CODE}/`, API_OSM_KEY);

        autoComplete.registerCallback((item: {
            alternative_names: string,
            display_name: string,
            lat: number, lon: number,
            name: string,
            name_suffix: string
        }) => {
            let shortAddress = item.name_suffix ? item.name_suffix : item.name ? item.name : item.display_name;

            this.handleCategoryChange({target: {name: 'locString', value: shortAddress}});
            this.handleCategoryChange({target: {name: 'loc', value: [item.lon, item.lat]}})
        });

    };

    componentWillUnmount(): void {
        localStorage.setItem('proformData', JSON.stringify(this.state));
        this.emptyState();
    }

    successCb = (res) => {
    };

    errCb = (err) => {
        // console.log("err", err);
    };

    setUserData = () => {
        if (this.props.user && this.props.user._id) {
            const name = this.props.user.name + " " + this.props.user.lastName;
            this.setState({
                businessName: name
            })
        }
    };

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (nextProps !== this.props) {
            this.setUserData();
            if (nextProps.allCategories !== this.props.allCategories) {
                let reordered = Misc.reorderCategories(nextProps.allCategories);
                let cats = reordered.newArray;
                let parents = reordered.parentsList;
                let subCatsArray = [];
                cats.forEach((_cat, idx) => {
                    _cat.categories.forEach((v, i) => {
                        let _i = cats.findIndex(x => v._id === x._id);
                        if (_i === -1)
                            subCatsArray.push(v);
                    })
                });
                this.setState({
                    parents: parents,
                    reorderedCategories: subCatsArray
                });
            }
        }
        if (nextProps.loc !== this.props.loc) {
            this.setState({
                loc: this.props.loc,
                locString: this.props.locString,
                locationTitle: this.props.locationTitle
            })
        }
        if (nextProps.osmAutocomplete && (typeof nextProps.osmAutocomplete.OsmNamesAutocomplete) === "function") {
            console.log('running autocomplete');
            this.initGeoAutoComplete(nextProps);
        }
    }

    // handling file select

    checkMimeType = (event) => {
        let files = event.target.files;
        let err = '';

        const types = ['image/png', 'image/jpeg', 'image/gif'];
        if (files)
            if (types.every(type => files[0].type !== type)) {
                err += 'Ce n\'est pas un format supporté\n';
                this.openSnackBar(err, "error");
            }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false;
        }
        return true;
    };

    checkFileSize = (event) => {
        let files = event.target.files;
        let size = 4500000;
        let err = "";
        if (files)
            if (files[0].size > size) {
                err += 'Fichier est trop grand, veuillez choisir un fichier plus petit\n';
                this.openSnackBar(err, "error");
            }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false
        }

        return true;
    };

    onFileSelect = (event) => {
        if (event) {
            if (this.checkMimeType(event) && this.checkFileSize(event))
                this.setState({file: event.target.files[0]});
        } else this.setState({file: null});
    };


    // navigating between steps

    nextStep(third) {
        let classes = this.state.componentClasses;
        let classesTwo = this.state.componentClassesTwo;
        let classesThree = this.state.componentClassesThree;
        let obj = {};

        if (!third) {
            classes.push('show');
            classesTwo.push('hide');
            obj = {
                componentClasses: classes,
                componentClassesTwo: classesTwo
            };
        } else {
            classesThree.push('show');
            classes.push('hide');
            obj = {
                componentClassesThree: classesThree,
                componentClasses: classes
            };
        }

        this.setState(obj);
        // this.componentClasses.push('show');
    }

    prevStep(third) {
        let classes = this.state.componentClasses;
        let classesTwo = this.state.componentClassesTwo;
        let classesThree = this.state.componentClassesThree;
        let obj = {};
        if (!third) {
            classes.pop();
            classesTwo.pop();
            obj = {
                componentClasses: classes,
                componentClassesTwo: classesTwo
            };
        } else {
            classesThree.pop();
            classes.pop();
            obj = {
                componentClasses: classes,
                componentClassesThree: classesThree
            };
        }

        this.setState(obj);
        // this.componentClasses.push('show');
    }

    // handling snack back show and hide

    setSnackMessage = (snackBarMessage: string) => {
        this.setState(state => ({
            snackBarMessage: snackBarMessage
        }))
    };

    setSnackVariant = (snackVariant: string) => {
        this.setState(state => ({
            snackVariant: snackVariant
        }))
    };

    handleCategoryChange = event => {
        // console.log(event)
        this.setState({[event.target.name]: event.target.value});
    };

    openSnackBar = (snackBarMessage: string, variant: "success" | "warning" | "error" | "info") => {
        this.setSnackVariant(variant);
        this.setSnackMessage(snackBarMessage);
        this.handleClick();
    };

    handleClick = () => {
        this.setOpen(true);
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setOpen(false);
    };

    setOpen = (open: boolean) => {
        this.setState(state => ({
            openSnack: open
        }))
    };

    // here we validate each step before passing the we submit in the final one 'thirdStepValidation'.

    firstStepValidation(next) {
        const {categoryId, loc, locString} = this.state;
        if (categoryId && loc && locString) {
            if (this.props.user) {
                this.nextStep(next);
            } else {
                this.props.updateToGetBack(this.props.location.pathname);
                this.setState({
                    loginRequestMessage: 'Pour continuer à remplir ce formulaire, vous devez vous identifier.'
                });
                this.handleRequestDialogOpen();
            }
        } else {
            this.openSnackBar(`${!categoryId ? 'Type de service' : 'Emplacement'} est requis!`, "warning");
        }
    }

    secondStepValidation(next) {
        const {introduction, businessPhone, businessName} = this.state;
        if (!businessName)
            this.openSnackBar(`Nom ou raison commercial est requis!`, "warning");
        else if (introduction && businessPhone && businessPhone.length === 8)
            this.nextStep(next); else {
            if (businessPhone && businessPhone.length < 8)
                this.openSnackBar(`Le numéro de téléphone doit comporter 8 caractères`, "warning");
            else this.openSnackBar(`${!introduction ? 'Introduction' : 'Numéro de téléphone'} est requis!`, "warning");
        }
    }

    emptyState = () => {
        this.setState({
            categoryId: undefined, loc: undefined, locString: undefined, file: null
        });
        localStorage.removeItem('proformData');
    };

    isGetProMeSuccess(props) {
        // console.log(result);
        props.checkIfCurrentIsPro(() => {}, () => {});
    }

    isGetProMeError(err) {
        console.log(err)
    };

    proAddSuccess = (res) => {
        if (res && this.state.file) {
            this.props.updateProImage({_id: res._id, file: this.state.file}, (prog) => {
                this.setState({
                    progress: prog.loaded
                })
            }, (suc) => {
                this.emptyState();
                this.props.getCurrentProfile((res) => {
                    this.props.getProMe(() => {this.isGetProMeSuccess(this.props)}, this.isGetProMeError, {userId: this.props.user._id});
                }, (err) => {});

                this.openSnackBar("Compte créer avec succées!", "success");
                this.props.navigate('/dashboard');
            }, this.proErrorCb)
        } else {
            this.emptyState();
            this.openSnackBar("Compte créer avec succées!", "success");
            this.props.navigate('/dashboard');
        }
    }

    proErrorCb = (err) => {
        // console.log("err", err);
        this.openSnackBar(err.message, "error");
    }

    thirdStepValidation(next) {
        const {file, businessName, categoryId, loc, locString, introduction, businessPhone, businessFee} = this.state;

        if (file) {
            if (this.props.user) {
                const proForm = {
                    businessName,
                    categoryId,
                    loc,
                    locString,
                    introduction,
                    businessPhone,
                    businessFee
                };
                this.props.newPro({userId: this.props.user._id, ...proForm}, this.proAddSuccess, this.proErrorCb)
            }
        } else {
            this.openSnackBar(`La photo est requis!`, "warning");
        }
    }

    handleRequestDialogClose = () => {
        this.setState({openLogin: false});
    };

    handleRequestDialogOpen = () => {
        this.setState({openLogin: true})
    };

    render() {
        const {classes, allCategories} = this.props;
        const {openSnack, snackBarMessage, snackVariant,
            reorderedCategories, parents, openLogin, loginRequestMessage} = this.state;


        return (
            <div className="loginComponent">
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={openSnack}
                    autoHideDuration={4000}
                    onClose={this.handleClose}
                >
                    <SnackBarContentWrapper
                        onClose={this.handleClose}
                        variant={snackVariant}
                        message={snackBarMessage}
                    />
                </Snackbar>
                {/*second step*/}
                <div className={this.state.componentClasses.join(' ')}>
                    <span className="signingTitle">Parlez de ce que vous faites</span>

                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <TextField
                            label="Nom ou raison commercial"
                            className={classes.textField + ' ' + 'text'}
                            type="text"
                            value={this.state.businessName || ''}
                            onChange={this.handleCategoryChange}
                            name="businessName"
                            multiline={true}
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            label="Introduction"
                            className={classes.textField + ' ' + 'text'}
                            type="text"
                            value={this.state.introduction || ''}
                            onChange={this.handleCategoryChange}
                            name="introduction"
                            multiline={true}
                            rowsMax="6"
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            label="Numéro de téléphone (12 123 123)"
                            className={classes.textField + ' ' + 'text'}
                            type="number"
                            inputProps={{
                                type: "tel",
                                pattern: "[0-9]{2}[0-9]{3}[0-9]{3}",
                                maxLength: "8",
                                minLength: "8"
                            }}
                            value={this.state.businessPhone || ''}
                            onChange={this.handleCategoryChange}
                            name="businessPhone"
                            margin="normal"
                            variant="outlined"
                        />
                        <TextField
                            label="Trif en TND par prestation"
                            className={classes.textField + ' ' + 'text'}
                            type="number"
                            inputProps={{
                                min: 1,
                                max: 1000
                            }}
                            value={this.state.businessFee || ''}
                            onChange={this.handleCategoryChange}
                            name="businessFee"
                            margin="normal"
                            variant="outlined"
                        />
                    </MuiThemeProvider>
                    <div className="buttonContainer">
                        <Button onClick={() => {
                            this.secondStepValidation(true)
                        }} color="secondary" variant="contained" className="signBtn">
                            Suivant
                        </Button>
                    </div>
                    <div className="buttonContainer">
                        <Button onClick={() => {
                            this.prevStep(false)
                        }} color="secondary" variant="contained" className="signBtn">
                            Précédent
                        </Button>
                    </div>
                </div>

                {/*third step*/}
                <div className={this.state.componentClassesThree.join(' ')}>
                    <span className="signingTitle">Sélectionnez une photo</span>

                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <div className="dateTime">
                            <input
                                id="___SelectProImageButton___"
                                type="file"
                                accept="image/*"
                                onChange={this.onFileSelect}
                                hidden
                            />
                            <ButtonBase onClick={() => {
                                global.document.getElementById('___SelectProImageButton___').click();
                            }}>
                                <div
                                    style={{backgroundImage: "url(" + (Boolean(this.state.file) ? window.URL.createObjectURL(this.state.file) : (this.state.imagePath ? this.state.imagePath : require('../../../assets/images/thumb.jpg'))) + ")"}}
                                    className="categoryImageUpload shadow">
                                    {this.state.progress ? <div className="progressCircleLayer">
                                        <CircularProgress variant="static" value={this.state.progress}/>
                                    </div> : <div/>}
                                </div>
                            </ButtonBase>
                        </div>
                    </MuiThemeProvider>
                    <div className="buttonContainer">
                        <Button color="secondary" variant="contained" className="signBtn" onClick={() => {
                            this.thirdStepValidation(false)
                        }}>
                            Valider
                        </Button>
                    </div>
                    <div className="buttonContainer">
                        <Button onClick={() => {
                            this.prevStep(true)
                        }} color="secondary" variant="contained" className="signBtn">
                            Précédent
                        </Button>
                    </div>
                </div>

                {/*first step*/}
                <div className={this.state.componentClassesTwo.join(" ")}>
                    <span
                        className="signingTitle">{"Trouvez de nouveaux clients " + (this.state.locationTitle ? "à " + this.state.locationTitle : "")}</span>

                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <FormControl variant="outlined" className={classes.formControl}>
                            <InputLabel
                                ref={ref => {
                                    this.InputLabelRef = ref;
                                }}
                                htmlFor="outlined-age-simple"
                            >
                                Quel service proposez-vous ?
                            </InputLabel>
                            <Select
                                value={this.state.categoryId || ''}
                                onChange={this.handleCategoryChange}
                                input={
                                    <OutlinedInput
                                        labelWidth={this.state.labelWidth}
                                        name="categoryId"
                                        className={classes.textField + ' ' + 'text'}
                                        id="outlined-age-simple"
                                        error={this.state.categoryIdUndefined}
                                    />
                                }
                            >
                                <MenuItem value={''}>
                                    <em>None</em>
                                </MenuItem>
                                {(parents && parents.length > 0) ?
                                    parents.map((cat, index) => {
                                        return [
                                            <MenuItem value={cat._id} className="parentCatDisabled"
                                                      disabled={true}>{cat.label}</MenuItem>,
                                            reorderedCategories.filter((x) => cat._id === x.parentId._id).map((_c, _i) => {
                                                return <MenuItem value={_c._id}
                                                                 key={_c._id + _i}>{_c.label}</MenuItem>
                                            })
                                        ]
                                    }) : ""
                                }
                            </Select>
                        </FormControl>
                        <TextField
                            value={this.state.locString || ''}
                            id={searchId}
                            label="Emplacement"
                            onChange={this.handleCategoryChange}
                            className={classes.textField + ' ' + 'text'}
                            type="text"
                            name="locString"
                            inputProps={
                                {
                                    autoComplete: "new-password",
                                    form: {
                                        autoComplete: "off",
                                    }
                                }}
                            margin="normal"
                            variant="outlined"
                        />

                    </MuiThemeProvider>
                    <div className="buttonContainer">
                        <Button onClick={() => {
                            this.firstStepValidation(false)
                        }} color="secondary" variant="contained"
                                className="signBtn">
                            Suivant
                        </Button>
                    </div>
                </div>
                <RequestLoginDialogWrapper open={openLogin} setOpen={this.handleRequestDialogOpen}
                                           message={loginRequestMessage}
                                           setClose={this.handleRequestDialogClose} navigate={this.props.navigate}
                />
            </div>
        );
    }
}

ProFormComponent.propTypes = {
    classes: PropTypes.object.isRequired,
    updateProImage: PropTypes.func,
    updateToGetBack: PropTypes.func,
    newPro: PropTypes.func,
    handleGeoCodingRequest: PropTypes.func,
    loc: PropTypes.array,
    locString: PropTypes.string,
    locationTitle: PropTypes.string
};

let statesToProps = (state, props) => {
    if (state.categoriesReducer && state.locationsReducer && state.currentUsersReducer && state.proFormFillingReducer) {
        return {
            allCategories: state.categoriesReducer.allCategories,
            osmAutocomplete: state.locationsReducer.osmAutocomplete,
            geoCodingResults: state.locationsReducer.geoCodingResults,
            proForm: state.proFormFillingReducer.proForm,
            user: state.currentUsersReducer.user
        }
    } else
        return {}
};

export default connect(statesToProps, {
    updateProImage,
    newPro,
    handleGeoCodingRequest,
    getCategories,
    updateToGetBack,
    updateRoot,
    getCurrentProfile,
    getProMe,
    checkIfCurrentIsPro
})(withStyles(styles)(ProFormComponent));
