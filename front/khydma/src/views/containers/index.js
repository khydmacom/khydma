import {HomeComponent} from './home';
import {CategoriesComponent} from './categories';
import {LoginComponent} from './login';
import {SignUpComponent} from './signup';
import {SearchResultsComponent} from './searchResults';
import {ProfileComponent} from './profile';
import {ProFormComponent} from './proForm';
import {AdminDashboardComponent} from './adminDashboard';
import {MainContainerComponent} from './mainContainer';
import {MainContainerRoutesNesterComponent} from './mainContainerRoutesNester';
import {LoginAdminComponent} from './loginAdmin';
import {LoadingScreen} from './loadingScreen';
import {DashboardComponent} from './dashboard';

export {
    HomeComponent,
    LoginComponent,
    SignUpComponent,
    SearchResultsComponent,
    CategoriesComponent,
    ProfileComponent,
    ProFormComponent,
    AdminDashboardComponent,
    MainContainerComponent,
    MainContainerRoutesNesterComponent,
    LoginAdminComponent,
    LoadingScreen,
    DashboardComponent
}
