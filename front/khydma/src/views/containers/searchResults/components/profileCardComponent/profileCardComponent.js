import React, {Component} from 'react';
import './profileCardComponent.css';
import Star from '@material-ui/icons/Star';
import Call from '@material-ui/icons/Call';
import * as PropTypes from 'prop-types';
import profileFiller from '../../../../../assets/images/no.png';
import {Link} from "@reach/router";
import {Misc} from '../../../../../config';
import ProsApi from '../../../../reducers/pros/prosApi';

class ProfileCardComponent extends Component {
    state = {
        checkedA: true,
        checkedB: true,
        checkedF: true,
        checkedG: true,
    };

    handleChange = name => event => {
        this.setState({[name]: event.target.checked});
    };

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if (nextProps !== this.props) {
            ProsApi.getAllReviewsStatsByQuery({proId: this.props.pro._id}, (succ, stats, err) => {
                // console.log(succ, stats, err)
                if (succ)
                    this.calculateAvgReviewers(stats)
            });
        }
    }

    calculateAvgReviewers = (stats) => {
        let totalReviewers = stats.total;
        let allReviewsNumber = 0;
        let stateObj = {};
        for (let rev of Object.keys(stats)) {
            if (rev !== 'total')
                allReviewsNumber += (Misc.rateParser(rev) * stats[rev]);

            stateObj['totalReviewers'] = totalReviewers;
            stateObj['allReviewsNumber'] = allReviewsNumber;
            stateObj['totalAvrg'] = (allReviewsNumber / totalReviewers).toFixed(0);
        }
        this.setState(stateObj);
        // console.log(stateObj);
        // console.log('ReviewsCounterComponent = >', this.state, totalReviewers, allReviewsNumber);
    };

    render() {
        const {pro} = this.props;

        return (
            <div className="profileCard">
                <div className="photoDiv">
                    <div className="avatar shadow"
                         style={{backgroundImage: `url("${(pro.businessImagePath ? pro.businessImagePath : profileFiller)}")`}}/>
                </div>
                <Link to={`/profile/${pro._id}`}>
                    <div className="dataDiv shadow">
                        <div className="cardFiller"/>
                        <div>
                            <section>
                                <span className="cardName">{pro.businessName}</span> <br/>
                                <span className="subTitleCard">{`${pro.categoryId.label} à ${pro.locString}`}.</span>
                            </section>
                            <div className="spacingCard"/>
                            <section className="metrics">
                                {
                                    this.state.totalReviewers ? <div className="meter">
                                        <span>Avis</span>
                                        <span className="counter">
                                            <Star className="star"/>
                                            <div>{this.state.totalAvrg}</div>
                                        </span>
                                    </div> : <div/>
                                }
                                {/*<div className="meter">
                                    <span>Appels</span>
                                    <span className="counter">
                                    <Call color="secondary"/>
                                    <div>5</div>
                                </span>
                                </div>*/}
                                {
                                    pro.businessFee ? <div className="meter">
                                        <span>Tarifs</span>
                                        <span className="counter">
                                        <div>{`${pro.businessFee}TND - Prestation`}</div>
                                    </span>
                                    </div> : <div/>
                                }
                            </section>
                        </div>

                    </div>
                </Link>
            </div>
        );
    }
}

ProfileCardComponent.protTypes = {
    pro: PropTypes.object
};


export default ProfileCardComponent;
