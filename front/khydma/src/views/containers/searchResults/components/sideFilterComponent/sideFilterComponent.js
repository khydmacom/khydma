import React, {Component} from 'react';
import './sideFilterComponent.css';
import Slider from '@material-ui/lab/Slider';
import TextField from '@material-ui/core/TextField';
import {withStyles, MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import * as PropTypes from 'prop-types';
import {CONST, Misc} from '../../../../../config'
import {connect} from 'react-redux'
import {SearchBarComponent} from "../../../../components/searchBar";

const {API_OSM_KEY, OSM_AUTOCOMPLETE_BASE_API, COUNTRY_CODE} = CONST;

const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
});

const searchId = "outlined-text-input-for-searchfilter";

const THEME = createMuiTheme({
    palette: {
        primary: {
            // light: '#fcfffc',
            main: '#2BA84A'
            // dark: '#b0b2b0',
            // contrastText: '#000',
        },
        // secondary: {
        // light: '#55b96e',
        // main: '#2BA84A',
        // dark: '#1e7533',
        // contrastText: '#fff',
        // }
    }, typography: {
        useNextVariants: true,
    }
});


class SideFilterComponent extends Component {
    state = {
        checkedA: true,
        checkedB: true,
        checkedF: true,
        checkedG: true,
        sliderPopMin: 1,
        sliderPriceMin: 50,
        sliderPopMax: 3,
        sliderPriceMax: 1000,
        pop: 3,
        price: 1000,
        mainClasses: ["mainFilterDiv"],
        results: [],
        open: false,
        anchorEl: null,
        searchInput: '',
        suggestions: [],
        selectedCat: null,
        lat: '',
        lon: ''
    };

    onChange = (event, {newValue}) => {
        this.setState({
            searchInput: newValue
        });
    };

    setAnchorEl = newValue => {
        this.setState({
            anchorEl: newValue
        });
    };

    handleChange = name => event => {
        this.setState({[name]: event.target.checked});
    };

    handlePriceSliderChange = (event, value) => {
        this.setState({price: value});
        setTimeout(() => {
            this.props.navigate(`/search?q=${this.state.searchInput ? this.state.searchInput : " "}${this.state.lat ? '&lat=' + this.state.lat : ''}${this.state.lon ? '&lng=' + this.state.lon : ''}&price=${value}`)
        }, 666)
    };

    handlePopSliderChange = (event, value) => {
        this.setState({pop: value});
    };

    componentWillMount() {
        this.changeClasses();
    };

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (nextProps.osmAutocomplete !== this.props.osmAutocomplete) {
            // setTimeout(this.initAutoComplete, 335)
            this.setState({
                searchInput: nextProps.searchKeyword,
            });
            // console.log(nextProps.osmAutocomplete, this.props.osmAutocomplete);
            if (Object.keys(nextProps.osmAutocomplete).length > 0)
                this.initAutoComplete(nextProps);
        }
    }

    componentDidMount() {
        // console.log(this.props.searchKeyword);

        if (this.props.searchKeyword) {
            this.setState({
                searchInput: this.props.searchKeyword
            });
        }
        if (this.props.lat && this.props.lng) {
            // get location display name here
            this.setState({
                locString: "",
                lat: this.props.lat,
                lon: this.props.lng,
            })
        } else if (this.props.geoCodingResults) {
            this.setState({
                locString: this.props.geoCodingResults.display_name,
                lat: this.props.geoCodingResults.loc.lng,
                lon: this.props.geoCodingResults.loc.lat
            })
        }

        if (this.props.osmAutocomplete && Object.keys(this.props.osmAutocomplete).length > 0) {
            // console.log(this.props.osmAutocomplete)
            this.initAutoComplete(this.props);

        }

        window.addEventListener("resize", this.changeClasses);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.changeClasses);
    }

    changeClasses = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    mainClasses: ["mainFilterDiv"]
                });
                return;
            case 'sm':
                this.setState({
                    mainClasses: ["mainFilterDiv"]
                });
                return;
            case 'md':
                this.setState({
                    mainClasses: ["mainFilterDiv", "shadow"]
                });
                return;
            case 'lg':
                this.setState({
                    mainClasses: ["mainFilterDiv", "shadow"]
                });
                return;
            case 'xl':
                this.setState({
                    mainClasses: ["mainFilterDiv", "shadow"]
                });
                return;
            default:
                return;
        }
    };

    handleMenuClick = (cat) => {
        this.setState({selectedCat: cat})
    };

    // method to get suggestions
    getSuggestions = (event) => {
        let value = event.target ? event.target.value : event;
        let results = value ? [...this.props.allCategories].filter((cat) => {
            let search = value;
            let exp = new RegExp(search.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
            return exp.test(cat.label.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
        }) : [];

        return results
    };

    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    initAutoComplete = (props) => {
        const autoComplete = new props.osmAutocomplete.OsmNamesAutocomplete(searchId, `${OSM_AUTOCOMPLETE_BASE_API}${COUNTRY_CODE}/`, API_OSM_KEY);
        autoComplete.registerCallback((item: {
            alternative_names: string,
            display_name: string,
            lat: number, lon: number, name_suffix: string
        }) => {
            this.handlePlaceChange({
                target: {
                    name: 'locString',
                    value: (item.name_suffix ? item.name_suffix : item.display_name)
                }
            });
            this.handlePlaceChange({target: {name: 'lat', value: item.lat}});
            this.handlePlaceChange({target: {name: 'lon', value: item.lon}});

            setTimeout(() => {
                props.navigate(`/search?q=${this.state.searchInput ? this.state.searchInput : " "}${item.lat ? '&lat=' + item.lat : ''}${item.lon ? '&lng=' + item.lon : ''}${this.state.price ? '&price=' + this.state.price : ''}`)
            }, 666)

        });
    };

    handlePlaceChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    render() {
        // selectedCat is all of the selected category attiributes from the search autofill
        // searchInput is the one from the searchbar value
        // results are the filtred categories
        // loc {number[]} and locString are the result from the location autoFill

        const {classes} = this.props;
        const {searchInput, price, lat, lon, locString, suggestions, anchorEl, results, open, selectedCat} = this.state;

        return (
            <div className={this.state.mainClasses.join(" ")}>
                <div className="searchSubTitle">
                    <span>Rechercher: </span>
                </div>
                <div className="inputDiv">
                    {/*// search bar here*/}
                    <SearchBarComponent suggestions={suggestions} results={results} open={open}
                                        anchorEl={anchorEl} value={searchInput} onChange={this.onChange}
                                        setAnchorEl={this.setAnchorEl} filter={true} lat={lat + ''} lng={lon + ''}
                                        onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                        onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                        selectedCat={selectedCat} handleMenuClick={this.handleMenuClick}
                                        categories={this.props.allCategories}/>
                </div>
                <div className="inputFilter">
                    <MuiThemeProvider theme={THEME}>
                        <TextField
                            id={searchId}
                            label="Emplacement"
                            className={classes.textField + ' ' + 'textFilter'}
                            type="text"
                            name="locString"
                            autoComplete="off"
                            value={locString || ''}
                            onChange={this.handlePlaceChange}
                            margin="normal"
                            variant="outlined"
                        />
                        <div className={classes.textField + ' ' + 'textFilter'}>
                            <span>Tarif max</span>
                            <div className="sliderValDiv">
                                <Slider
                                    className="slider"
                                    value={price}
                                    min={this.state.sliderPriceMin}
                                    max={this.state.sliderPriceMax}
                                    step={10}
                                    aria-labelledby="label"
                                    onChange={this.handlePriceSliderChange}
                                />
                                <span>{this.state.price}</span>
                            </div>
                        </div>

                        {/*disabled the popularity filter for later updates*/}
                        {/*<div className={classes.textField + ' ' + 'textFilter'}>
                            <span>Popularité</span>
                            <div className="sliderValDiv">
                                <Slider
                                    className="slider"
                                    value={pop}
                                    min={this.state.sliderPopMin}
                                    max={this.state.sliderPopMax}
                                    step={1}
                                    aria-labelledby="label"
                                    onChange={this.handlePopSliderChange}
                                />
                                <span>{this.state.pop === 1 ? 'Moins populaire' : (this.state.pop === 2 ? 'Populaire' : 'Très populaire')}</span>
                            </div>
                        </div>*/}
                    </MuiThemeProvider>
                </div>
            </div>
        );
    }
}

/*SideFilterComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};*/

SideFilterComponent.propTypes = {
    classes: PropTypes.object.isRequired,
    searchKeyword: PropTypes.string,
    osmAutocomplete: PropTypes.object,
    navigate: PropTypes.func,
    lat: PropTypes.string,
    lng: PropTypes.string
};


let statesToProps = (state, props) => {
    if (state.authReducer && state.categoriesReducer && state.locationsReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            allCategories: state.categoriesReducer.allCategories,
            osmAutocomplete: state.locationsReducer.osmAutocomplete
        }
    } else
        return {}
};

export default connect(statesToProps, {})(withStyles(styles)(SideFilterComponent));
