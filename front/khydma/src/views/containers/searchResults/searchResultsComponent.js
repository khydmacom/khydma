import React, {Component} from 'react';
import './searchResultsComponent.css';
import {withStyles} from '@material-ui/core/styles';
import * as PropTypes from 'prop-types';
import {SideFilterComponent, ProfileCardComponent} from "./components";
import {connect} from "react-redux";
import {RootsActions} from '../../reducers/roots';
import {prosActions} from '../../reducers/pros';
import {CONST} from '../../../config';
import * as queryString from 'query-string';
import {Helmet} from 'react-helmet';
import ProsApi from "../../reducers/pros/prosApi";
import PubSubJs from 'pubsub-js';

const {updateRoot} = RootsActions;
const {searchPros} = prosActions;
const {pubSubConstants} = CONST;

const pubSubJs = require('pubsub-js');


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
});

class SearchResultsComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            searchPartClasses: ['searchFilterSide'],
            resultsPartClasses: ['res'],
            searchKeyWordClasses: ['resultKeyword'],
            fillerClass: 'hidden',
            value: '',
            lat: '',
            lng: '',
            searchResults: []
        }
    }

    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    passQueryToSearch = (props) => {
        let parsed = queryString.parse(props.location.search);
        if (parsed.q) {
            this.setState({
                value: parsed.q
            })
        }

        if (parsed.lat && parsed.lng) {
            this.setState({
                lat: parsed.lat,
                lng: parsed.lng,
            })
        }

        this.searchForPros(props, parsed);
    };

    componentDidMount() {
        this.props.updateRoot(this.props);

        if (this.props.path === 'search') {
            pubSubJs.publish(pubSubConstants.showFilter);
        }
        window.addEventListener('scroll', this.listenToScroll);

        this.passQueryToSearch(this.props);
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        if (prevProps.location !== this.props.location) {
            this.passQueryToSearch(this.props);
        }
    }

    searchForPros = (props, query) => {
        let loc = {};
        if (query.lat && query.lng) {
            const {lat, lng} = query;
            loc = {lat, lng}
        }
        PubSubJs.publish('sidenavfilters', {lat: this.state.lat, lng: this.state.lng, navigate: this.props.navigate,
            searchKeyword: this.state.value});

        ProsApi.searchAllPros({q: query.q, ...loc, maxPrice: query.price ? query.price : 9999}, (success, pro, errMessage) => {
            if (success) {
                console.log('search results = ', pro);
                this.setState({
                    proSearchResults: pro,
                });
            } else if (errMessage) this.isProError(errMessage);
        });
        // props.searchPros(this.isProSuccess, this.isProError, {q: query.q, ...loc});
    };

    isProSuccess = (succ) => {
        // console.log(succ)
    };

    isProError = (err) => {
        // console.log(err);
    };

    componentWillUnmount() {
        pubSubJs.publish(pubSubConstants.hideFilter);
        window.removeEventListener('scroll', this.listenToScroll);
    }

    listenToScroll = () => {
        const winScroll = document.body.scrollTop || document.documentElement.scrollTop;

        const height = document.documentElement.scrollHeight - document.documentElement.clientHeight;
        const clientHeight = document.documentElement.clientHeight;

        const scrolled = winScroll / height;
        let currentSearchClasses = this.state.searchPartClasses;
        let currentKeywordClasse = this.state.searchKeyWordClasses;
        let filterMovingLeft = document.getElementById("movingFilter").offsetLeft;
        let filterMovingTop = document.getElementById("movingFilter").offsetTop;

        /*console.log('wiondow = ', winScroll, height, document.documentElement.clientHeight);
        console.log('pos = ',filterMovingLeft, filterMovingTop);*/

        /*if (winScroll >= 100 && winScroll <= clientHeight) {
            if (currentSearchClasses.length < 2 && currentKeywordClasse.length) {
                currentSearchClasses.push('fixedSearchFilter');
                // currentKeywordClasse.push('fixedKeywordFilter');
                this.setState({
                    searchPartClasses: currentSearchClasses,
                    fillerClass: 'fillerShown',
                    movingFilterStyles: {left: filterMovingLeft + 'px', top: filterMovingTop + 'px'}
                    // searchKeyWordClasses: currentKeywordClasse
                })
            }
        } else {
            if (currentSearchClasses.findIndex((x) => {
                return x === 'fixedSearchFilter';
            }) > -1) {
                currentSearchClasses.pop();
                // currentKeywordClasse.pop();
                this.setState({
                    searchPartClasses: currentSearchClasses,
                    fillerClass: 'hidden',
                    movingFilterStyles: (winScroll >= clientHeight ? {paddingTop: winScroll + 'px!important'} : {})
                    // searchKeyWordClasses: currentKeywordClasse
                })
            }
        }*/
    }

    renderNotFoundResult = () => {
        return <div className="profileCard">
            <span className="noRes">Aucun résultat trouvé</span>
        </div>
    }

    render() {
        // this.props.loc & this.props.searchKeyword are the ones passed in the URL Parameters.

        const {osmAutocomplete, geoCodingResults} = this.props;
        const {lat, lng, value, proSearchResults} = this.state;

        return (
            <div className="searchResultMainDiv">
                <Helmet>
                    <title>{`${value} ${geoCodingResults.display_name ? '| ' + geoCodingResults.display_name : ''} | Khydma.com `}</title>
                    <meta name="description" content={`${value}: Khydma.com vous aide à trouver tous les professionnels près de chez vous.`}/>

                </Helmet>
                <div className="mainViewContauiner rowAlign flexStartAlign" style={{minHeight: '90vh'}}>
                    <div id="movingFilter" className={this.state.searchPartClasses.join(" ")}
                         style={this.state.movingFilterStyles}>
                        {value ? <SideFilterComponent navigate={this.props.navigate} searchKeyword={value}
                                                      lat={lat} lng={lng}
                                                      osmAutocomplete={osmAutocomplete} geoCodingResults={geoCodingResults}/> : <div/>}
                    </div>
                    {/*<div className={this.state.fillerClass}/>*/}
                    <div className="searchFilterSide" style={{width: '4vw'}}/>
                    <div className={this.state.resultsPartClasses.join(" ")}>
                        {proSearchResults && proSearchResults.length > 0 ? proSearchResults.map((pro, index) => {
                            return <ProfileCardComponent key={index} pro={pro.proId}/>
                        }) : this.renderNotFoundResult()}
                        {/*<div style={{height: '20vh'}}/>*/}
                    </div>
                </div>
            </div>
        );
    }
}

SearchResultsComponent.propTypes = {
    classes: PropTypes.object.isRequired
};

let statesToProps = (state, props) => {
    // console.log(state);
    if (state.authReducer && state.categoriesReducer && state.prosReducer && state.locationsReducer) {
        return {
            proSearchResults: state.prosReducer.proSearchResults,
            osmAutocomplete: state.locationsReducer.osmAutocomplete,
            geoCodingResults: state.locationsReducer.geoCodingResults
        }
    } else
        return {}
};

export default connect(statesToProps, {
    updateRoot,
    searchPros
})(withStyles(styles)(SearchResultsComponent));
