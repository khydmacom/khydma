import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './loadingScreen.css';
import PropTypes from 'prop-types';
import {
    CircularProgress,
    Dialog,
    MuiThemeProvider
} from "@material-ui/core";
import {Misc} from "../../../config";

class LoadingScreen extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount() {
    }

    componentWillUnmount(): void {
        // console.log("bye")
    }


    handleClose = () => {
        this.props.onClose(this.props.selectedValue);
    };

    handleOnOn = value => {
        // console.log("its on")
    };

    render() {
        const {classes, onClose, selectedValue, ...other} = this.props;

        return (
            <Dialog fullScreen onEnter={this.handleOnOn} onClose={this.handleClose}
                    aria-labelledby="simple-dialog-title" {...other} className="allOfloadingScreen">
                <div className="loadingMain">
                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <CircularProgress className="progress" color="primary"/>
                    </MuiThemeProvider>
                </div>
            </Dialog>
        );
    }
}

LoadingScreen.propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool
    // selectedValue: PropTypes.string,
};

export default LoadingScreen;
