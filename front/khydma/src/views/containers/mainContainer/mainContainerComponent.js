import React, {Component} from 'react';
import './mainContainerComponent.css';
import {MuiThemeProvider} from '@material-ui/core/styles';
import {HeaderComponent} from "../../components/header";
import {SideNavComponent} from "../../components/sideNav";
import SideNavFiltersComponent from "../../components/sideNavFilters/sideNavFiltersComponent";
import {FooterComponent} from "../../components/footer";
import {Misc, CONST} from "../../../config";
import {connect} from "react-redux";
import * as PubSubJs from 'pubsub-js';

import Script from 'react-load-script';
import {LocationsActions} from "../../reducers/locations";

const {handleScriptLoad} = LocationsActions;
const {OSM_AUTOCOMPLETE_LINK} = CONST;

class MainContainerComponent extends Component {
    pubsubsubber;
    constructor(props) {
        super(props);
        this.state = {
            left: false,
            right: false,
            showHeader: true,
            drawerWidth: 0,
            publishedParams: {}
        };
        this.DrawerRef = React.createRef();

        this.pubsubsubber = PubSubJs.subscribe('sidenavfilters', (txt, vals) => {
            this.setPublishedParams(vals);
        })
    }

    toggleDrawer = (side, open) => () => {
        this.setState({
            [side]: open,
        });
    };

    setPublishedParams = (params) => {
        this.setState({
            publishedParams: params
        });
    };

    componentWillUnmount(): void {
        if (this.pubsubsubber) {
            PubSubJs.unsubscribe(this.pubsubsubber);
        }
    }

    render() {
        const {pathname, handleScriptLoad, children} = this.props;
        const {left, right, publishedParams} = this.state;

        return (
            <MuiThemeProvider theme={Misc.THEME}>
                <Script url={OSM_AUTOCOMPLETE_LINK} onLoad={handleScriptLoad} onError={(err) => {
                    // console.log(err)
                }}/>
                <HeaderComponent openFilter={right} open={left}
                                 toggleDrawer={this.toggleDrawer.bind(this)}/>
                <SideNavComponent open={left} toggleDrawer={this.toggleDrawer.bind(this)}
                                  drawerRef={this.DrawerRef}/>
                <SideNavFiltersComponent publishedParams={publishedParams} open={right} toggleDrawer={this.toggleDrawer.bind(this)}/>
                <div className="mainVertDiv"/>
                {children}
                {pathname.includes('dashboard') ? <></> : <FooterComponent/>}
            </MuiThemeProvider>
        );
    }
}

let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer && state.locationsReducer) {
        return {
            pathname: state.rootsReducer.pathname // stays
        }
    } else
        return {}
};

export default connect(statesToProps, {
    handleScriptLoad
})(MainContainerComponent);
