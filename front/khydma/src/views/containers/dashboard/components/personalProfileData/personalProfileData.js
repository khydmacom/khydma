import React, {Component} from 'react';
import './personalProfileData.css';
import PropTypes, {object} from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import Button from "@material-ui/core/es/Button";
import {IconButton, MuiThemeProvider, Snackbar} from "@material-ui/core";
import {Misc} from "../../../../../config";
import TextField from "@material-ui/core/TextField";
import {Formik} from "formik";
import {SnackBarContentWrapper} from "../../../../components";


class PersonalProfileData extends Component {
    constructor(props) {
        super(props);

        this.state = {
            passwordOn: false
        };
    }

    handlePassOnClick = () => {
        this.setState(state => ({
            passwordOn: !state.passwordOn
        }))
    };

    render() {
        const {user} = this.props;
        const {passwordOn} = this.state;
        const {validateUpdateForm, validateUpdatePass, isEqual} = Misc;

        const {email, name, lastName, ...others} = user;

        return (
            <>
                <div className="profileHeaderDashboardMainDiv shadow">
                    {user ? <Formik
                        initialValues={{email, name, lastName}}
                        validate={validateUpdateForm}
                        onSubmit={(values, {setSubmitting}) => {
                            this.props.updateData({_id: user._id, ...values}, (res) => {
                                this.props.openSnackBar("Données utilisateur mises à jour avec succès!", "success");
                                this.props.updateCurrentProfile((profile) => {
                                }, (err) => {
                                    console.log("err = ", err);
                                });
                                setSubmitting(false);
                            }, (err) => {
                                console.log("signup err = ", err);
                                this.props.openSnackBar("Il s'est passé quelque chose qui cloche.", "warning");
                                setSubmitting(false);
                            })
                        }}
                    >
                        {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                          }) => (
                            <MuiThemeProvider theme={Misc.FORM_THEME}>
                                <form className="formStyle" onSubmit={handleSubmit}>
                                    <TextField
                                        label="Nom"
                                        className="textUpdate"
                                        type="text"
                                        name="name"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        color="secondary"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.name && touched.name && errors.name)}
                                        helperText={errors.name && touched.name && errors.name}
                                        value={values.name}
                                    />
                                    <TextField
                                        label="Prénom"
                                        className="textUpdate"
                                        type="text"
                                        name="lastName"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.lastName && touched.lastName && errors.lastName)}
                                        helperText={errors.lastName && touched.lastName && errors.lastName}
                                        value={values.lastName}
                                    />
                                    <TextField
                                        label="E-mail"
                                        className="textUpdate"
                                        type="email"
                                        name="email"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.email && touched.email && errors.email)}
                                        helperText={errors.email && touched.email && errors.email}
                                        value={values.email}
                                    />
                                    {/*<TextField
                                        label="Numéro de téléphone"
                                        className="textUpdate"
                                        type="number"
                                        name="phone"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.phone && touched.phone && errors.phone)}
                                        helperText={errors.phone && touched.phone && errors.phone}
                                        value={values.phone}
                                    />*/}
                                    <div className="buttonContainer">
                                        <Button type="submit"
                                                disabled={isEqual(values, {
                                                    email,
                                                    name,
                                                    lastName
                                                }) || Object.keys(errors).length > 0 || isSubmitting}
                                                color="primary" variant="contained" className="updateConfirmButton">
                                            Confirmer
                                        </Button>
                                    </div>
                                </form>
                            </MuiThemeProvider>
                        )}
                    </Formik> : ""}
                </div>
                <br/>
                <div className="profileHeaderDashboardMainDiv shadow">
                    <div className="justifyBetween">
                        <span className="dropPass">Mettre à jour mot de passe</span>
                        <IconButton onClick={this.handlePassOnClick}>{passwordOn ?
                            <KeyboardArrowUp className="dropPass"/> :
                            <KeyboardArrowDown className="dropPass"/>}</IconButton>
                    </div>
                    {passwordOn ? <Formik
                        initialValues={{password: undefined, newPassword: undefined}}
                        validate={validateUpdatePass}
                        onSubmit={(values, {setSubmitting, setErrors, setStatus, resetForm}) => {
                            this.props.updatePassword({_id: user._id, email: user.email, ...values}, (res) => {
                                if (res.message) {
                                    this.props.openSnackBar(res.message, "error");
                                    setSubmitting(false)
                                } else {
                                    resetForm({});
                                    this.props.openSnackBar("Mot de pass mises à jour avec succès!", "success");
                                    setSubmitting(false);
                                }
                            }, (err) => {
                                console.log("signup err = ", err);
                                this.props.openSnackBar("Mot de passe incorrect!", "error");
                                setSubmitting(false);
                            })
                        }}
                    >
                        {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                          }) => (
                            <MuiThemeProvider theme={Misc.FORM_THEME}>
                                <form onSubmit={handleSubmit} style={{width: '100%'}}>
                                    <TextField
                                        label="Ancien mot de pass"
                                        className="textUpdate"
                                        type="password"
                                        name="password"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        color="secondary"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.password && touched.password && errors.password)}
                                        helperText={errors.password && touched.password && errors.password}
                                        value={values.password || ''}
                                    />
                                    <TextField
                                        label="Nouvelle mot de pass"
                                        className="textUpdate"
                                        type="password"
                                        name="newPassword"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.newPassword && touched.newPassword && errors.newPassword)}
                                        helperText={errors.newPassword && touched.newPassword && errors.newPassword}
                                        value={values.newPassword || ''}
                                    />
                                    <div className="buttonContainer">
                                        <Button type="submit" disabled={isEqual(values, {
                                            password: undefined,
                                            newPassword: undefined
                                        }) || Object.keys(errors).length > 0 || isSubmitting}
                                                color="primary" variant="contained" className="updateConfirmButton">
                                            Confirmer
                                        </Button>
                                    </div>
                                </form>
                            </MuiThemeProvider>
                        )}
                    </Formik> : ""}
                </div>
            </>
        );
    }
}

PersonalProfileData.propTypes = {
    parameters: PropTypes.any,
    user: PropTypes.object,
    updateData: PropTypes.func,
    updatePassword: PropTypes.func,
    updateCurrentProfile: PropTypes.func,
    openSnackBar: PropTypes.func
};

export default PersonalProfileData;
