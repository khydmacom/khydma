import React, {Component} from 'react';
import './galleryDashProComponent.css';
import {connect} from "react-redux";
import * as PropTypes from 'prop-types';
import Button from "@material-ui/core/es/Button/Button";
import {GalleryElement, SnackBarContentWrapper} from "../../../../components";
import {ButtonBase, Snackbar} from "@material-ui/core";
import {ConfirmDelete, FullImageGalleryDialog} from "../../../../dialogs";
import {ToggleButton, ToggleButtonGroup} from "@material-ui/lab";
import AddPhotoGalleryDialog from "../../../../dialogs/addPhotoGalleryDialog/addPhotoGalleryDialog";
import {prosActions} from "../../../../reducers/pros";
import ProApi from "../../../../reducers/pros/prosApi";
import {RootsActions} from "../../../../reducers/roots";

const {getProMe} = prosActions;
const {updateRoot} = RootsActions;

class GalleryDashProComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            openAdditionDialog: false,
            deleteImages: false,
            openSnack: false,
            openConfirm: false,
            snackBarMessage: "",
            snackVariant: "success",
            selectedFile: null,
            galleryImages: [],
            imageToShow: null
        }
    }

    setOpen = (open: boolean) => {
        this.setState(state => ({
            openSnack: open
        }))
    };

    setSnackMessage = (snackBarMessage: string) => {
        this.setState(state => ({
            snackBarMessage: snackBarMessage
        }))
    };

    setSnackVariant = (snackVariant: string) => {
        this.setState(state => ({
            snackVariant: snackVariant
        }))
    };

    openSnackBar = (snackBarMessage: string, variant: "success" | "warning" | "error" | "info") => {
        this.setSnackVariant(variant);
        this.setSnackMessage(snackBarMessage);
        this.handleClick();
    };

    handleClick = () => {
        this.setOpen(true);
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setOpen(false);
    };

    handleDialogOpen = (image) => {
        this.setState({open: true, imageToShow: image});
    };

    handleDialogClose = () => {
        this.setState({open: false})
    };

    handleDeleteConfirmDialogOpen = (image) => {
        this.setState({openConfirm: true, imageToShow: image});
    };

    handleDeleteConfirmDialogClose = () => {
        this.setState({openConfirm: false})
    };

    handleDeleteConfirmDialogConfirmation = () => {
        this.deleteGalleryImage(this.state.imageToShow);
        this.setState({openConfirm: false})
    };

    componentDidMount(): void {
        this.refreshGallery();
        this.props.updateRoot(this.props);

    }

    refreshGallery = () => {
        ProApi.getProGallery({proId: this.props.proMe._id}, (succ, res, err) => {
            if (succ)
                this.setState({
                    galleryImages: res
                });
            else this.openSnackBar(err.message, "warning");
            // console.log(res);
        })
    }

    setDeleteState = () => {
        this.setState({
            deleteImages: !this.state.deleteImages
        })
    };

    checkMimeType = (event) => {
        let files = event.target.files;
        let err = '';

        const types = ['image/png', 'image/jpeg', 'image/gif'];

        if (types.every(type => files[0].type !== type)) {
            err += 'Ce n\'est pas un format supporté\n';
            this.openSnackBar(err, "error");
        }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false;
        }
        return true;
    };

    checkFileSize = (event) => {
        let files = event.target.files;
        let size = 4500000;
        let err = "";
        if (files[0].size > size) {
            err += 'Fichier est trop grand, veuillez choisir un fichier plus petit\n';
            this.openSnackBar(err, "error");
        }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false
        }

        return true;
    };

    handleFileSelect = (event) => {
        if (event) {
            if (this.checkMimeType(event) && this.checkFileSize(event))
                this.setState({selectedFile: window.URL.createObjectURL(event.target.files[0]), fileObj: event.target.files[0]});
        } else this.setState({selectedFile: null});
    };

    handleAddDialogClickOpen = () => {
        this.setState({
            openAdditionDialog: true,
        });
    };

    handleAddDialogClickClose = () => {
        this.setState({
            openAdditionDialog: false,
        });
    };

    addNewGalleryImage = (vals) => {
        ProApi.addGalleryImage({
            ...vals,
            proId: this.props.proMe._id,
        }, (re) => {
            // console.log(re)
        }, (succ, res, err) => {
            if (succ) {
                this.openSnackBar("Image ajoutée avec succès!", "success");
                this.handleAddDialogClickClose();
                this.refreshGallery()

            } else this.openSnackBar(err.message, "warning");
        })
    };

    deleteGalleryImage = (image) => {
        ProApi.deleteGalleryImage(image, (succ, res, err) => {
            if (succ) {
                this.openSnackBar("Image supprimée avec succès", "success");
                this.refreshGallery();
            } else this.openSnackBar(err.message, "warning");
        });
    };

    render() {
        const {proMe} = this.props;
        const {
            open, openAdditionDialog, deleteImages, selectedFile, openConfirm,
            openSnack, snackBarMessage, snackVariant, galleryImages, imageToShow, fileObj
        } = this.state;

        return (
            <div className="galleryMainDiv">
                <div className="buttonContainerGalleryPro"/>
                <div className="galleryTitlePro shadow">
                    <span>Montrez à vos clients potentiels les différents aspects de votre domaine d'activité, à travers la galerie</span>
                </div>
                <div className="buttonContainerGalleryPro buttonsAlignFullImage">
                    <Button variant={"contained"} color={"secondary"} onClick={this.handleAddDialogClickOpen}>Ajouter
                        une nouvelle photo</Button>
                    <ToggleButtonGroup value={deleteImages} exclusive onChange={this.setDeleteState}>
                        <ToggleButton value={true}>
                            Supprimer une photo
                        </ToggleButton>
                    </ToggleButtonGroup>
                    {/*<Button variant={"contained"} onClick={this.setDeleteState} className="dangerButton">Supprimer une photo</Button>*/}
                </div>
                <div className="galleryProElements">
                    {galleryImages.length > 0 ? galleryImages.map((e, index) => {
                        return <div key={index} className="elementWrapperLoop">
                            {
                                deleteImages ? <GalleryElement image={e} deleteImage={deleteImages} onDelete={() => {
                                        this.handleDeleteConfirmDialogOpen(e);
                                    }
                                    }/> :
                                    <ButtonBase onClick={() => {
                                        this.handleDialogOpen(e)
                                    }}>
                                        <GalleryElement image={e} deleteImage={deleteImages}/>
                                    </ButtonBase>
                            }
                        </div>
                    }) : <div className="noConvosText">Commencez par ajouter une photo</div>}
                </div>

                {/*snack component*/}
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={openSnack}
                    autoHideDuration={4000}
                    onClose={this.handleClose}
                >
                    <SnackBarContentWrapper
                        onClose={this.handleClose}
                        variant={snackVariant}
                        message={snackBarMessage}
                    />
                </Snackbar>
                {/*the dialog to add an image*/}
                <AddPhotoGalleryDialog onFileSelect={this.handleFileSelect} open={openAdditionDialog}
                                       file={selectedFile} showSnackBar={this.openSnackBar}
                                       onClose={this.handleAddDialogClickClose}
                                       newCategory={this.addNewGalleryImage} fileObj={fileObj}
                />
                {/*the dialog to show a fullSize image*/}
                <FullImageGalleryDialog image={imageToShow} open={open} setClose={this.handleDialogClose}
                                        setOpen={this.handleDialogOpen}/>
                <ConfirmDelete image={imageToShow} open={openConfirm} setClose={this.handleDeleteConfirmDialogClose}
                               setOpen={this.handleDeleteConfirmDialogOpen} setConfirm={this.handleDeleteConfirmDialogConfirmation}/>
            </div>
        );
    }
}

GalleryDashProComponent.propTypes = {
    parameters: PropTypes.any
};
let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer && state.prosReducer) {
        return {
            proMe: state.prosReducer.proMe
        }
    } else
        return {}
};

export default connect(statesToProps, {
    getProMe, updateRoot
})(GalleryDashProComponent);
