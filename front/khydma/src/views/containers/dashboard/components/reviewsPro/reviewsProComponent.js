import React, {Component} from 'react';
import './reviewsProComponent.css';
import {connect} from "react-redux";
import * as PropTypes from 'prop-types';
import {RootsActions} from "../../../../reducers/roots";
import {ReviewsContentComponent} from "../../../profile/components/reviewsContent";
import {IconButton} from "@material-ui/core";
import Delete from '@material-ui/icons/Delete';
import ProApi from "../../../../reducers/pros/prosApi";
import {prosActions} from "../../../../reducers/pros";


const {updateRoot} = RootsActions;
const {getProMe} = prosActions;


class ReviewsProComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            reviews: []
        };
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
        this.fetchReviews();
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if (nextProps.user !== this.props.user)
            this.props.getProMe(this.isSuccess, this.isError, {userId: nextProps.user._id});
    }

    isSuccess(result) {
        this.fetchReviews();
    }

    isError(err) {
        console.log(err)
    }

    fetchReviews = () => {
        ProApi.getAllReviewsByQuery({proId: this.props.proMe._id}, (suc, allRevs, err) => {
            if (suc) {
                // updating the reviews list
                if (allRevs || Object.keys(allRevs).length > 0)
                    this.setState({
                        reviews: allRevs.reviews
                    });
                else this.setState({
                    reviews: null
                });
            }
        })
    };

    deleteReview = (id) => {
        ProApi.deleteReview({_id: id}, (succ, res, err) => {
            // console.log(succ, res, err);
            if (succ)
                this.fetchReviews();
        })
    };

    render() {
        const {classes} = this.props;
        const {reviews} = this.state;

        return (
            <div className="revProMainDiv">
                <div className="buttonContainerGalleryPro"/>
                <div className="galleryTitlePro shadow">
                    <span>Consulter et gérer les avis sur votre profil</span>
                </div>
                <div className="buttonContainerGalleryPro"/>

                <div className="reviewsOnDash shadow">
                    {
                       reviews && reviews.length > 0 ? reviews.map((rev, idx) => {
                            return <div key={idx} className="myReviewHolder revsDashHolder">
                                <ReviewsContentComponent review={rev}/>
                                <IconButton onClick={() => {this.deleteReview(rev._id)}}>
                                    <Delete/>
                                </IconButton>
                            </div>
                        }) : <span className="myReviewHolder revsDashHolder noConvosText">Vous n'avez pas des avis pour le moment.</span>
                    }
                </div>

            </div>
        );
    }
}

ReviewsProComponent.propTypes = {
    parameters: PropTypes.any
};
let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer && state.prosReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            isAdmin: state.currentUsersReducer.isAdmin,
            user: state.currentUsersReducer.user,
            path: state.rootsReducer.path,
            pathname: state.rootsReducer.pathname,
            uri: state.rootsReducer.uri,
            proMe: state.prosReducer.proMe,
        }
    } else
        return {}
};

export default connect(statesToProps, {
    updateRoot, getProMe
})(ReviewsProComponent);
