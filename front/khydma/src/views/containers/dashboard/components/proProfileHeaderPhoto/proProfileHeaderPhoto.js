import React, {Component} from 'react';
import './proProfileHeaderPhoto.css';
import PropTypes from 'prop-types';
import pictureFiler from "../../../../../assets/images/no.png";
import PersonPinCircle from '@material-ui/icons/PersonPinCircle';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Button from "@material-ui/core/es/Button";


class ProProfileHeaderPhoto extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {classes, pro} = this.props;

        return (
            <>
                <div className="profileHeaderDashboardMainDiv shadow">
                    <input
                        id="___SelectUpdateProImageButton___"
                        type="file"
                        accept="image/*"
                        onChange={this.props.onFileSelect}
                        hidden
                    />
                    <div className="avatarHeaderPicture" style={{
                        backgroundImage: "url('" + ((pro && pro.businessImagePath) ? pro.businessImagePath : pictureFiler) + "')",
                        height: 17 + 'vh',
                        width: 17 + 'vh',
                        borderRadius: (17 / 2) + 'vh'
                    }}/>
                    <div className="proDataProfileHeader">
                        <span
                            className="profileHeaderName">{pro && pro.businessName ? (pro.businessName ? pro.businessName : pro.name) : ((pro && pro.userId) ? (pro.userId.lastName ? (pro.userId.name + " " + pro.userId.lastName) : pro.userId.name) : "")}</span>
                        {/*<div className="vertSpacing"/>*/}
                        {pro && pro.categoryId ? <span className="profileHeaderName profileHeaderLoc proGategory">{pro.categoryId.label}</span> : <div/>}
                        {pro && pro.locString ? <span className="profileHeaderName profileHeaderLoc"><PersonPinCircle
                            className="profileHeaderName"/>{pro.locString}</span> : <div/>}

                    </div>
                </div>
                <Button color="secondary" className="editPictureButton"
                        onClick={() => {
                            global.document.getElementById('___SelectUpdateProImageButton___').click();
                        }}
                ><AccountCircle className="iconEditPhoto"/>
                    <span>Modifier photo de profile</span></Button>
            </>
        );
    }
}

ProProfileHeaderPhoto.propTypes = {
    parameters: PropTypes.any,
    pro: PropTypes.object,
    onFileSelect: PropTypes.func
};

export default ProProfileHeaderPhoto;
