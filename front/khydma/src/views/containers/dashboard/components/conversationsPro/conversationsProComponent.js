import React, {Component} from 'react';
import './conversationsProComponent.css';
import {connect} from "react-redux";
import PropTypes from 'prop-types';
import {Misc, CONST} from "../../../../../config";
import {ConversationsList} from "../../../../components/conversationsList";
import {MessagesList} from "../../../../components/messagesList";
import {MessagingApi} from "../../../../reducers/messaging";
import {userApi} from "../../../../reducers/users";
import PubSubJs from 'pubsub-js';
import * as queryString from "query-string";
import {SocketServices} from "../../../../../config/services";
import {RootsActions} from "../../../../reducers/roots";
import {SingleConversationDialogWrapper} from "../../../../dialogs/singleConversationDialogWrapper";

const {onConversation, onMessages} = MessagingApi;
const {updateRoot} = RootsActions;
const {pubSubConstants, ioTriggers} = CONST;
const _SocketServices = SocketServices;


class ConversationsProComponent extends Component {
    pubSubUnsubscriber;

    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            conversations: [],
            receiver: {}
        };
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
        MessagingApi.getConvs({proId: this.props.user.proId}, this.convByQuerySuccess);
        if (_SocketServices.prototype.socket)
            this.runConvSubber();

        this.pubSubUnsubscriber = PubSubJs.subscribe(pubSubConstants.subscribeSockets, () => {
            this.runConvSubber();
        });

        window.addEventListener("resize", this.setViewType);
        this.passQueryToSingleMessage(this.props);
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        if (prevProps.location !== this.props.location) {
            this.passQueryToSingleMessage(this.props);
        }
    }

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
        if (this.pubSubUnsubscriber) {
            PubSubJs.unsubscribe(this.pubSubUnsubscriber);
        }
        if (_SocketServices.prototype.socket)
            _SocketServices.prototype.socket.off(ioTriggers.newConversation);
    }

    sendMessage = (messageData) => {
        let socketMessageBody = {
            senderId: this.props.user._id,
            text: messageData,
            userId: this.state.receiver._id,
            proId: this.props.user.proId,
            receiverId: this.state.receiver._id
        };
        _SocketServices.prototype.socket.emit(ioTriggers.newMessage, socketMessageBody);
        console.log('sendMessage = ', socketMessageBody);
    };

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };



    passQueryToSingleMessage = (props) => {
        let parsed = queryString.parse(props.location.search);
        this.setState({parsed})

        if (parsed.userId)
            this.setMessageWindow(props, parsed);
    };

    sortConversationByDate = (a, b) => {
        return new Date(a.lastMessageId.creation_date) - new Date(b.lastMessageId.creation_date)
    };

    setMessageWindow = (props, parsed) => {
        userApi.getUserById(parsed.userId, (succ, res, err) => {
            if (succ) {
                this.setState({
                    receiver: res
                });
                if (this.state.fullScreen)
                    this.handleShowMsgsDialog();
            } else {
                console.log(err);
            }
        });
    };


    runConvSubber = () => {
        MessagingApi.onConversation((resultIo) => {
            let resIndexInCnvs = this.state.conversations.findIndex(x => x._id === resultIo._id)

            if (resultIo.proId._id === this.props.user.proId)
                if (resIndexInCnvs === -1)
                    this.setState({
                        lastReceivedMessage: resultIo.lastMessageId,
                        conversations: [...this.state.conversations, resultIo]
                    });
                else {
                    let cnvClone = this.state.conversations.slice();
                    cnvClone[resIndexInCnvs] = resultIo;
                    this.setState({
                        lastReceivedMessage: resultIo.lastMessageId,
                        conversations: cnvClone
                    })
                }
        })
    };



    convIoResults = (cnvRes) => {
        console.log('convIoResults = ', cnvRes);
        if (cnvRes.proId === this.props.user.proId)
            this.setState({
                conversations: [cnvRes, ...this.state.conversations]
            })
    };

    convByQuerySuccess = (succ, res, err) => {
        if (succ && res && res.length > 0) {
            if (this.state.conversations.length > 0)
                [...res].forEach((cnv, idx) => {
                    if (this.state.conversations.findIndex((x) => x._id === cnv._id) === -1)
                        this.setState({
                            conversations: [...this.state.conversations, res]
                        })
                });
            else this.setState({
                conversations: res
            })
        }
    };

    convByQueryError = (err) => {
        console.log('convByQueryError = ', err)
    };

    handleShowMsgsDialog = () => {
        this.setState({
            showMessagesDialog: true
        })
    };

    handleHideMsgsDialog = () => {
        this.setState({
            showMessagesDialog: false
        })
    };


    render() {
        const {user} = this.props;
        const {fullScreen, conversations, showMessagesDialog, receiver,parsed, lastReceivedMessage} = this.state;

        return (
            <div className="conversationComponentMainDiv">
                <div className="proConversationsHeader shadow">
                    <span className="convProHeaderTitle">Ces conversations se déroulent entre vous en tant que professionnel et les clients.</span>
                </div>
                <div className="convProDivider"/>
                <div className="fullScreenConvoPage">
                    <div className="conversationsPart">
                        <ConversationsList conversations={conversations.sort(this.sortConversationByDate).reverse()}
                                           isPro={true}
                        />
                    </div>
                    {
                        !fullScreen ?
                            <div className="messagesList">
                                <MessagesList user={user} lastNewMessage={lastReceivedMessage}
                                              parsed={parsed}
                                              sendMessage={this.sendMessage} profile={receiver}/>
                            </div> : <div/>
                    }
                </div>
                <SingleConversationDialogWrapper sendMessage={this.sendMessage} open={showMessagesDialog}
                                                 setClose={this.handleHideMsgsDialog}
                                                 setOpen={this.handleShowMsgsDialog}
                                                 user={user} lastNewMessage={lastReceivedMessage}
                                                 parsed={parsed} profile={receiver}
                />
                {/*<span className="revsDashHolder myReviewHolder noConvosText">Cette version ne supporte pas la messagerie</span>*/}
            </div>
        );
    }
}

ConversationsProComponent.propTypes = {
    parameters: PropTypes.any
};
let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer && state.messagingReducer) {
        return {
            user: state.currentUsersReducer.user
        }
    } else
        return {}
};

export default connect(statesToProps, {updateRoot})(ConversationsProComponent);
