import React, {Component} from 'react';
import './editProfileProComponent.css';
import {connect} from "react-redux";
import * as PropTypes from 'prop-types';
import {prosActions} from "../../../../reducers/pros";
import {categoriesActions} from "../../../../reducers/categories";
import {ProProfileHeaderPhoto} from "../proProfileHeaderPhoto";
import {ProProfileData} from "../proProfileData";
import {SnackBarContentWrapper} from "../../../../components/snackBarContentWrapper";
import {Snackbar} from "@material-ui/core";
import {Misc} from "../../../../../config";
import {RootsActions} from "../../../../reducers/roots";

const {getCategories} = categoriesActions;
const {updatePro, getProMe, updateProImage} = prosActions;
const {updateRoot} = RootsActions;


class EditProfileProComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            snackBarMessage: "",
            snackVariant: "success",
            reorderedCategories: []
        };
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (nextProps.allCategories !== this.props.allCategories) {
            let reordered = Misc.reorderCategories(nextProps.allCategories);
            let cats = reordered.newArray;
            let parents = reordered.parentsList;
            let subCatsArray = [];
            cats.forEach((_cat, idx) => {
                _cat.categories.forEach((v, i) => {
                    let _i = cats.findIndex(x => v._id === x._id);
                    if (_i === -1)
                        subCatsArray.push(v);
                })
            });
            this.setState({
                parents: parents,
                reorderedCategories: subCatsArray
            });
        }
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if (nextProps.user.isPro !== this.props.user.isPro)
            this.props.getProMe(this.isSuccess, this.isError, {userId: nextProps.user._id});
    }

    isSuccess(result) {
        // console.log(result);
    }

    isError(err) {
        console.log(err)
    };

    componentDidMount(): void {
        this.props.getCategories(this.isSuccess, this.isError);
        this.props.updateRoot(this.props);
    }

    setOpen = (open: boolean) => {
        this.setState(state => ({
            open: open
        }))
    };

    setSnackMessage = (snackBarMessage: string) => {
        this.setState(state => ({
            snackBarMessage: snackBarMessage
        }))
    };

    setSnackVariant = (snackVariant: string) => {
        this.setState(state => ({
            snackVariant: snackVariant
        }))
    };

    openSnackBar = (snackBarMessage: string, variant: "success" | "warning" | "error" | "info") => {
        this.setSnackVariant(variant);
        this.setSnackMessage(snackBarMessage);
        this.handleClick();
    };

    handleClick = () => {
        this.setOpen(true);
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setOpen(false);
    };

    // handling file select

    checkMimeType = (event) => {
        let files = event.target.files;
        let err = '';

        const types = ['image/png', 'image/jpeg', 'image/gif'];
        if (files)
            if (types.every(type => files[0].type !== type)) {
                err += 'Ce n\'est pas un format supporté\n';
                this.openSnackBar(err, "error");
            }

        if (err !== '') {
            event.target.value = null;
            console.log(err);
            return false;
        }
        return true;
    };

    checkFileSize = (event) => {
        let files = event.target.files;
        let size = 4500000;
        let err = "";
        if (files)
            if (files[0].size > size) {
                err += 'Fichier est trop grand, veuillez choisir un fichier plus petit\n';
                this.openSnackBar(err, "error");
            }

        if (err !== '') {
            event.target.value = null;
            console.log(err);
            return false
        }

        return true;
    };

    onFileSelect = (event) => {
        if (event) {
            if (this.checkMimeType(event) && this.checkFileSize(event)) {
                this.props.updateProImage({file: event.target.files[0], _id: this.props.proMe._id}, (value) => {
                    this.setState({
                        progress: value
                    });
                    console.log(value);
                }, (res) => {
                    console.log('onFileSelect = ', res);
                    this.props.getProMe((profile) => {
                        console.log("updateCurrentProfile = ", profile);
                    }, (err) => {
                        console.log("err = ", err);
                    }, {_id: this.props.proMe._id});
                }, (err) => {console.log(err)});
            }
        } else {
            this.setState({file: null});
        }
    };

    render() {
        const {snackVariant, snackBarMessage, open, reorderedCategories, parents} = this.state;
        const {classes, proMe} = this.props;

        return (
            <div className="profileDashboardMainDiv">
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={open}
                    autoHideDuration={4000}
                    onClose={this.handleClose}
                >
                    <SnackBarContentWrapper
                        onClose={this.handleClose}
                        variant={snackVariant}
                        message={snackBarMessage}
                    />
                </Snackbar>
                <ProProfileHeaderPhoto pro={proMe} onFileSelect={this.onFileSelect}/>
                <div className="vertSpacing"/>
                {proMe ? <ProProfileData proMe={proMe} updatePro={this.props.updatePro} categories={reorderedCategories} parents={parents}
                                         osmAutocomplete={this.props.osmAutocomplete} openSnackBar={this.openSnackBar}
                                         getProMe={this.props.getProMe} updateProImage={this.props.updateProImage}/>  : ""}
            </div>
        );
    }
}

EditProfileProComponent.propTypes = {
    parameters: PropTypes.any,
    user: PropTypes.object
};
let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer && state.categoriesReducer) {
        return {
            user: state.currentUsersReducer.user,
            proMe: state.prosReducer.proMe,
            allCategories: state.categoriesReducer.allCategories,
            osmAutocomplete: state.locationsReducer.osmAutocomplete,
        }
    } else
        return {}
};

export default connect(statesToProps, {
    updatePro, getProMe, updateProImage, getCategories, updateRoot
})(EditProfileProComponent);
