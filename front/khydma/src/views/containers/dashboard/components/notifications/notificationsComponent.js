import React, {Component} from 'react';
import './notificationsComponent.css';
import * as PropTypes from 'prop-types';
import {connect} from "react-redux";
import {RootsActions} from "../../../../reducers/roots";


const {updateRoot} = RootsActions;


class NotificationsComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
    }

    render() {
        const {classes} = this.props;

        return (
            <div className="prosMainDiv">
                <span className="revsDashHolder myReviewHolder noConvosText">Cette version ne supporte pas les notifications</span>
            </div>
        );
    }
}

NotificationsComponent.propTypes = {
    parameters: PropTypes.any
};
let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer) {
        return {
        }
    } else
        return {}
};

export default connect(statesToProps, {updateRoot}) (NotificationsComponent);
