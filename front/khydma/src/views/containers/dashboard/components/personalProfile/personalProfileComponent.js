import React, {Component} from 'react';
import './personalProfileComponent.css';
import * as PropTypes from 'prop-types';
import {PersonalProfileHeaderPhoto} from "../personalProfileHeaderPhoto";
import {connect} from "react-redux";
import {CurrentUsersActions} from "../../../../reducers/currentUsers";
import {usersActions} from "../../../../reducers/users";
import {PersonalProfileData} from "../personalProfileData";
import {SnackBarContentWrapper} from "../../../../components/snackBarContentWrapper";
import {Snackbar} from "@material-ui/core";
import {RootsActions} from "../../../../reducers/roots";

const {getCurrentProfile} = CurrentUsersActions;
const {updateRoot} = RootsActions;
const {updateUserPassword, updateUser, updateUserImage} = usersActions;


class PersonalProfileComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            snackBarMessage: "",
            snackVariant: "success"
        };
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
    }

    setOpen = (open: boolean) => {
        this.setState(state => ({
            open: open
        }))
    };

    setSnackMessage = (snackBarMessage: string) => {
        this.setState(state => ({
            snackBarMessage: snackBarMessage
        }))
    };

    setSnackVariant = (snackVariant: string) => {
        this.setState(state => ({
            snackVariant: snackVariant
        }))
    };

    openSnackBar = (snackBarMessage: string, variant: "success" | "warning" | "error" | "info") => {
        this.setSnackVariant(variant);
        this.setSnackMessage(snackBarMessage);
        this.handleClick();
    };

    handleClick = () => {
        this.setOpen(true);
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setOpen(false);
    };

    // handling file select

    checkMimeType = (event) => {
        let files = event.target.files;
        let err = '';

        const types = ['image/png', 'image/jpeg', 'image/gif'];
        if (files)
            if (types.every(type => files[0].type !== type)) {
                err += 'Ce n\'est pas un format supporté\n';
                this.openSnackBar(err, "error");
            }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false;
        }
        return true;
    };

    checkFileSize = (event) => {
        let files = event.target.files;
        let size = 4500000;
        let err = "";
        if (files)
            if (files[0].size > size) {
                err += 'Fichier est trop grand, veuillez choisir un fichier plus petit\n';
                this.openSnackBar(err, "error");
            }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false
        }

        return true;
    };

    onFileSelect = (event) => {
        if (event) {
            if (this.checkMimeType(event) && this.checkFileSize(event)) {
                this.props.updateUserImage({file: event.target.files[0]}, (value) => {
                    this.setState({
                        progress: value
                    });
                }, (res) => {
                    this.props.getCurrentProfile((profile) => {
                        // console.log("updateCurrentProfile = ", profile);
                    }, (err) => {
                        console.log("err = ", err);
                    });
                }, (err) => {console.log(err)});
            }
        } else {
            this.setState({file: null});
        }
    };

    render() {
        const {snackVariant, snackBarMessage, open, file} = this.state;
        const {user} = this.props;
        return (
            <div className="profileDashboardMainDiv">
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={open}
                    autoHideDuration={4000}
                    onClose={this.handleClose}
                >
                    <SnackBarContentWrapper
                        onClose={this.handleClose}
                        variant={snackVariant}
                        message={snackBarMessage}
                    />
                </Snackbar>
                <PersonalProfileHeaderPhoto user={user} onFileSelect={this.onFileSelect}
                                            file={file}/>
                <div className="vertSpacing"/>
                {user ? <PersonalProfileData user={user} updatePassword={this.props.updateUserPassword}
                                             updateData={this.props.updateUser}
                                             updateCurrentProfile={this.props.getCurrentProfile}
                                             openSnackBar={this.openSnackBar}
                /> : ""}
            </div>
        );
    }
}

PersonalProfileComponent.propTypes = {
    parameters: PropTypes.any
};

let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer) {
        return {
            user: state.currentUsersReducer.user
        }
    } else
        return {}
};

export default connect(statesToProps, {
    getCurrentProfile,
    updateUserPassword, updateUser, updateUserImage,
    updateRoot
})(PersonalProfileComponent);
