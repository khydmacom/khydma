import React, {Component} from 'react';
import './personalProfileHeaderPhoto.css';
import PropTypes from 'prop-types';
import pictureFiler from "../../../../../assets/images/no.png";
import PersonPinCircle from '@material-ui/icons/PersonPinCircle';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Button from "@material-ui/core/es/Button";


class PersonalProfileHeaderPhoto extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    render() {
        const {classes, user} = this.props;

        return (
            <>
                <div className="profileHeaderDashboardMainDiv shadow">
                    <input
                        id="___SelectUserImageButton___"
                        type="file"
                        accept="image/*"
                        onChange={this.props.onFileSelect}
                        hidden
                    />
                    <div className="avatarHeaderPicture" style={{
                        backgroundImage: "url('" + ((user && user.profilePicture) ? user.profilePicture : pictureFiler) + "')",
                        height: 17 + 'vh',
                        width: 17 + 'vh',
                        borderRadius: (17 / 2) + 'vh'
                    }}/>
                    <div className="userDataProfileHeader">
                        <span
                            className="profileHeaderName">{user ? (user.lastName ? (user.name + " " + user.lastName) : user.name) : ""}</span>
                        {/*<div className="vertSpacing"/>*/}
                        <br/>
                        {user && user.locString ? <span className="profileHeaderName profileHeaderLoc"><PersonPinCircle
                            className="profileHeaderName"/>{user.locString}</span> : <div/>}

                    </div>
                </div>
                <Button color="secondary" className="editPictureButton"
                        onClick={() => {
                            global.document.getElementById('___SelectUserImageButton___').click();
                        }}
                ><AccountCircle className="iconEditPhoto"/>  <span>Modifier photo de profile</span></Button>
            </>
        );
    }
}

PersonalProfileHeaderPhoto.propTypes = {
    parameters: PropTypes.any,
    user: PropTypes.object,
    onFileSelect: PropTypes.func
};

export default PersonalProfileHeaderPhoto;
