import React, {Component} from 'react';
import './metricsComponent.css';
import {connect} from "react-redux";
import classNames from 'classnames';
import {createMuiTheme, MuiThemeProvider, withStyles} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/es/Button/Button";
import {RootsActions} from "../../../../reducers/roots";
import {
    CallsMetricsDashPro,
    ReviewsMetricsDashPro,
    SearchAppearsMetricsDashPro,
    ViewsMetricsDashPro
} from "../../../../components";

const {updateRoot} = RootsActions;


class MetricsComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
    }

    render() {
        const {classes} = this.props;

        return (
            <div className="galleryMainDiv">
                {/*<div className="buttonContainerGalleryPro"/>
                <div className="galleryTitlePro shadow">
                    <span>Obtenez une vue globale de tous les métriques de votre profil.</span>
                </div>
                <div className="buttonContainerGalleryPro"/>
                <div className="galleryProElements">
                    <CallsMetricsDashPro/>
                    <ReviewsMetricsDashPro/>
                    <ViewsMetricsDashPro/>
                    <SearchAppearsMetricsDashPro/>
                </div>*/}
                <span className="revsDashHolder myReviewHolder noConvosText">Cette version ne supporte pas les métriques</span>
            </div>
        );
    }
}

MetricsComponent.propTypes = {
    parameters: PropTypes.any
};
let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer && state.rootsReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            isAdmin: state.currentUsersReducer.isAdmin,
            path: state.rootsReducer.path,
            pathname: state.rootsReducer.pathname,
            uri: state.rootsReducer.uri
        }
    } else
        return {}
};

export default connect(statesToProps, {
    updateRoot
})(MetricsComponent);
