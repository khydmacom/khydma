import React, {Component} from 'react';
import './proProfileData.css';
import * as PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import Button from "@material-ui/core/es/Button";
import {
    FormControl,
    IconButton,
    InputLabel, MenuItem,
    MuiThemeProvider,
    OutlinedInput,
    Select,
    Snackbar
} from "@material-ui/core";
import {CONST, Misc} from "../../../../../config";
import TextField from "@material-ui/core/TextField";
import {Formik} from "formik";
import {SnackBarContentWrapper} from "../../../../components";
import ReactDOM from "react-dom";

const {API_OSM_KEY, OSM_AUTOCOMPLETE_BASE_API, COUNTRY_CODE} = CONST;

const searchId = "__choose_loc__update__";

class ProProfileData extends Component {
    constructor(props) {
        super(props);
        // Lundi , Mardi , Mercredi , Jeudi , Vendredi , Samedi and Dimanche
        this.state = {
            locationOn: false,
            weekDays: "Lundi,Mardi,Mercredi,Jeudi,Vendredi,Samedi,Dimanche",

            labelWidth2: 0,
            labelWidth3: 0,
            labelWidth4: 0,
        };
    }


    initAutoComplete = () => {
        const autoComplete = new this.props.osmAutocomplete.OsmNamesAutocomplete(searchId, `${OSM_AUTOCOMPLETE_BASE_API}${COUNTRY_CODE}/`, API_OSM_KEY);
        autoComplete.registerCallback((item: {
            alternative_names: string,
            display_name: string,
            lat: number, lon: number, name_suffix: string
        }) => {
            this.handleCategoryChange({
                target: {
                    name: 'locString',
                    value: (item.name_suffix ? item.name_suffix : item.display_name)
                }
            });
            this.handleCategoryChange({target: {name: 'loc', value: [item.lon, item.lat]}})
        });
    };

    handleCategoryChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    handleLocationBlur = event => {
        if (event.target.value.length === 0)
            this.setState({locationError: "Emplacement est requis."});
        else
            this.setState({locationError: ""});
    };

    handleLocationSubmit = (event) => {
        this.setState({
            isSubmittingLocation: true
        });
        this.props.updatePro({_id: this.props.proMe, loc: this.state.loc, locString: this.state.locString}, (res) => {
            // console.log("signup succ = ", res);
            this.props.openSnackBar("Emplacement mises à jour avec succès!", "success");
            this.props.getProMe((profile) => {
                // console.log("updateCurrentProProfile = ", profile);
                this.setState({
                    loc: this.props.proMe.loc,
                    locString: this.props.proMe.locString
                })
            }, (err) => {
                // console.log("err = ", err);
            }, {_id: this.props.proMe._id});
            this.setState({
                isSubmittingLocation: false
            });
        }, (err) => {
            // console.log("signup err = ", err);
            this.props.openSnackBar("Il s'est passé quelque chose qui cloche.", "warning");
            this.setState({
                isSubmittingLocation: false
            });
        })
    };

    handleLocOnClick = () => {
        this.setState(state => ({
            locationOn: !state.locationOn
        }));
        setTimeout(this.initAutoComplete, 335);
    };

    componentDidMount(): void {
        this.setState({
            labelWidth2: ReactDOM.findDOMNode(this.InputLabelRef2).offsetWidth,
            labelWidth3: ReactDOM.findDOMNode(this.InputLabelRef3).offsetWidth,
            labelWidth4: ReactDOM.findDOMNode(this.InputLabelRef4).offsetWidth,
        });

        // to overcome the location submission issue²
        // here we set the initial location in states
        this.setState({
            loc: this.props.proMe.loc,
            locString: this.props.proMe.locString
        })
    }

    render() {
        const {proMe, categories, parents} = this.props;
        const {validateProForm, isEqual} = Misc;

        const {
            businessName,
            workingDaysStarting,
            workingDaysEnding,
            workingHoursStarting,
            workingHoursEnding,
            businessFee,
            businessFeeRate,
            businessImagePath,
            internetLinks,
            businessPhone,
            introduction,
            categoryId, ...others
        } = proMe;

        const {open, snackBarMessage, snackVariant} = this.state;

        return (
            <>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={open}
                    autoHideDuration={4000}
                    onClose={this.handleClose}
                >
                    <SnackBarContentWrapper
                        onClose={this.handleClose}
                        variant={snackVariant}
                        message={snackBarMessage}
                    />
                </Snackbar>

                <div className="profileHeaderDashboardMainDiv shadow">
                    {proMe ? <Formik
                        initialValues={{
                            businessName,
                            workingDaysStarting,
                            workingDaysEnding,
                            workingHoursStarting,
                            workingHoursEnding,
                            businessFee,
                            businessImagePath,
                            businessPhone,
                            introduction,
                            categoryId,
                            internetLinks
                        }}
                        validate={validateProForm}
                        onSubmit={(values, {setSubmitting}) => {
                            this.props.updatePro({_id: proMe._id, ...values}, (res) => {
                                // console.log("signup succ = ", res);
                                this.props.openSnackBar("Données utilisateur mises à jour avec succès!", "success");
                                this.props.getProMe((profile) => {
                                    // console.log("updateCurrentProProfile = ", profile);
                                }, (err) => {
                                    // console.log("err = ", err);
                                }, {_id: proMe._id});
                                setSubmitting(false);
                            }, (err) => {
                                // console.log("signup err = ", err);
                                this.props.openSnackBar("Il s'est passé quelque chose qui cloche.", "warning");
                                setSubmitting(false);
                            })
                        }}
                    >
                        {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting
                          }) => (
                            <MuiThemeProvider theme={Misc.FORM_THEME}>
                                <form className="formStyle" onSubmit={handleSubmit}>
                                    <TextField
                                        label="Nom ou raison commercial"
                                        type="text"
                                        className="textUpdate"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.businessName && touched.businessName && errors.businessName)}
                                        helperText={errors.businessName && touched.businessName && errors.businessName}
                                        value={values.businessName || ''}
                                        name="businessName"
                                    />
                                    <TextField
                                        className="textUpdate"
                                        autoComplete="off"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.introduction && touched.introduction && errors.introduction)}
                                        helperText={errors.introduction && touched.introduction && errors.introduction}
                                        value={values.introduction || ''}
                                        label="Introduction"
                                        type="text"
                                        name="introduction"
                                        multiline={true}
                                        rowsMax="6"
                                        margin="normal"
                                        variant="outlined"
                                    />
                                    <TextField
                                        className="textUpdate"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.businessPhone && touched.businessPhone && errors.businessPhone)}
                                        helperText={errors.businessPhone && touched.businessPhone && errors.businessPhone}
                                        value={values.businessPhone || ''}
                                        label="Numéro de téléphone (12 123 123)"
                                        type="number"
                                        inputProps={{
                                            type: "tel",
                                            pattern: "[0-9]{2}[0-9]{3}[0-9]{3}",
                                            maxLength: "8",
                                            minLength: "8"
                                        }}
                                        name="businessPhone"
                                    />
                                    <TextField
                                        label="Trif en TND par prestation"
                                        className="textUpdate"
                                        type="number"
                                        inputProps={{
                                            min: 1,
                                            max: 1000
                                        }}
                                        error={!!(errors.businessFee && touched.businessFee && errors.businessFee)}
                                        value={values.businessFee || ''}
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        helperText={errors.businessFee && touched.businessFee && errors.businessFee}
                                        name="businessFee"
                                        margin="normal"
                                        variant="outlined"
                                    />

                                    <div className="dateTime textUpdate">
                                        <TextField
                                            label="Heures de travail à partir du"
                                            type="time"
                                            variant="outlined"
                                            value={values.workingHoursStarting || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!(errors.workingHoursStarting && touched.workingHoursStarting && errors.workingHoursStarting)}
                                            helperText={errors.workingHoursStarting && touched.workingHoursStarting && errors.workingHoursStarting}
                                            name="workingHoursStarting"
                                            className="textUpdate"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{
                                                step: 300, // 5 min
                                            }}
                                        />
                                        <TextField
                                            label="Heures de travail finissant le"
                                            type="time"
                                            value={values.workingHoursEnding || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            error={!!(errors.workingHoursEnding && touched.workingHoursEnding && errors.workingHoursEnding)}
                                            helperText={errors.workingHoursEnding && touched.workingHoursEnding && errors.workingHoursEnding}
                                            name="workingHoursEnding"
                                            variant="outlined"
                                            className="textUpdate"
                                            InputLabelProps={{
                                                shrink: true,
                                            }}
                                            inputProps={{
                                                step: 300, // 5 min
                                            }}
                                        />
                                    </div>
                                    <FormControl variant="outlined" className="textUpdate formctrl">
                                        <InputLabel
                                            ref={ref => {
                                                this.InputLabelRef3 = ref;
                                            }}
                                            htmlFor="outlined-age-simple"
                                        >
                                            Jours ouvrables à partir de
                                        </InputLabel>
                                        <Select
                                            value={values.workingDaysStarting || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            input={
                                                <OutlinedInput
                                                    labelWidth={this.state.labelWidth3}
                                                    name="workingDaysStarting"
                                                    className="textUpdate"
                                                    id="outlined-age-simple"
                                                    error={!!(errors.workingDaysStarting && touched.workingDaysStarting && errors.workingDaysStarting)}
                                                />
                                            }
                                        >
                                            <MenuItem value={null}>
                                                <em>None</em>
                                            </MenuItem>
                                            {this.state.weekDays.split(',').map((day, index) => {
                                                return <MenuItem value={day} key={index}>
                                                    <em>{day}</em>
                                                </MenuItem>
                                            })}
                                        </Select>
                                    </FormControl>

                                    <FormControl variant="outlined" className="textUpdate formctrl">
                                        <InputLabel
                                            ref={ref => {
                                                this.InputLabelRef4 = ref;
                                            }}
                                            htmlFor="outlined-age-simple"
                                        >
                                            Jours ouvrables se terminant
                                        </InputLabel>
                                        <Select
                                            value={values.workingDaysEnding || ''}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            input={
                                                <OutlinedInput
                                                    labelWidth={this.state.labelWidth4}
                                                    name="workingDaysEnding"
                                                    className="textUpdate"
                                                    id="outlined-age-simple"
                                                    error={!!(errors.workingDaysEnding && touched.workingDaysEnding && errors.workingDaysEnding)}
                                                />
                                            }
                                        >
                                            <MenuItem value={null}>
                                                <em>None</em>
                                            </MenuItem>
                                            {this.state.weekDays.split(',').map((day, index) => {
                                                return <MenuItem value={day} key={index}>
                                                    <em>{day}</em>
                                                </MenuItem>
                                            })}
                                        </Select>
                                    </FormControl>

                                    <FormControl variant="outlined" className="textUpdate formctrl">
                                        <InputLabel
                                            ref={ref => {
                                                this.InputLabelRef2 = ref;
                                            }}
                                            htmlFor="outlined-age-simple"
                                        >
                                            Quel service proposez-vous ?
                                        </InputLabel>
                                        <Select
                                            value={(values.categoryId && values.categoryId._id ? values.categoryId._id : values.categoryId)}
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                            input={
                                                <OutlinedInput
                                                    labelWidth={this.state.labelWidth2}
                                                    name="categoryId"
                                                    className="textUpdate"
                                                    id="outlined-age-simple"
                                                    error={!!(errors.categoryId && touched.categoryId && errors.categoryId)}
                                                />
                                            }
                                        >
                                            <MenuItem value={undefined}>
                                                <em>None</em>
                                            </MenuItem>
                                            {
                                                (parents && parents.length > 0) ?
                                                parents.map((cat, index) => {
                                                    return [
                                                        <MenuItem value={cat._id}
                                                                  disabled={true}>{cat.label}</MenuItem>,
                                                        categories.filter((x) => cat._id === x.parentId._id).map((_c, _i) => {
                                                            return <MenuItem value={_c._id}
                                                                             key={_c._id + _i}>{_c.label}</MenuItem>
                                                        })
                                                    ]
                                                }) : ""
                                            }
                                        </Select>
                                    </FormControl>

                                    <TextField
                                        className="textUpdate"
                                        autoComplete="off"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.internetLinks && touched.internetLinks && errors.internetLinks)}
                                        helperText={errors.internetLinks && touched.internetLinks && errors.internetLinks}
                                        value={values.internetLinks || ''}
                                        label="Liens Internet, (veuillez entrer les liens séparés par des virgules)"
                                        type="text"
                                        name="internetLinks"
                                        multiline={true}
                                        rowsMax="6"
                                        margin="normal"
                                        variant="outlined"
                                    />
                                    <div className="buttonContainer">
                                        <Button type="submit"
                                                disabled={isEqual(values, {
                                                    businessName,
                                                    workingDaysStarting,
                                                    workingDaysEnding,
                                                    workingHoursStarting,
                                                    workingHoursEnding,
                                                    businessFee,
                                                    businessImagePath,
                                                    businessPhone,
                                                    introduction,
                                                    categoryId
                                                }) || Object.keys(errors).length > 0 || isSubmitting}
                                                color="primary" variant="contained" className="updateConfirmButton">
                                            Confirmer
                                        </Button>
                                    </div>
                                </form>
                            </MuiThemeProvider>
                        )}
                    </Formik> : ""}
                </div>
                <br/>
                <div className="profileHeaderDashboardMainDiv shadow">
                    <div className="justifyBetween">
                        <span className="dropPass">Mettre à jour l'emplacement</span>
                        <IconButton onClick={this.handleLocOnClick}>{this.state.locationOn ?
                            <KeyboardArrowUp className="dropPass"/> :
                            <KeyboardArrowDown className="dropPass"/>}</IconButton>
                    </div>
                    {this.state.locationOn ?
                        <MuiThemeProvider theme={Misc.FORM_THEME}>
                            <form onSubmit={this.handleLocationSubmit} style={{width: '100%'}}>
                                <TextField
                                    label="Emplacement"
                                    className="textUpdate"
                                    id={searchId}
                                    type="text"
                                    name="locString"
                                    autoComplete="off"
                                    margin="normal"
                                    variant="outlined"
                                    color="secondary"
                                    onChange={this.handleCategoryChange}
                                    onBlur={this.handleLocationBlur}
                                    error={!!(this.state.locationError)}
                                    helperText={this.state.locationError}
                                    value={this.state.locString || ''}
                                />
                                <div className="buttonContainer">
                                    <Button type="submit" disabled={isEqual({
                                        loc: this.state.loc,
                                        locString: this.state.locString
                                    }, {loc: proMe.loc, locString: proMe.locString})
                                    || this.state.locationError && this.state.locationError.length > 0 || this.state.isSubmittingLocation}
                                            color="primary" variant="contained" className="updateConfirmButton">
                                        Confirmer
                                    </Button>
                                </div>
                            </form>
                        </MuiThemeProvider> : ""
                    }
                </div>
            </>
        );
    }
}

ProProfileData.propTypes = {
    parameters: PropTypes.any,
    proMe: PropTypes.object,
    getProMe: PropTypes.func,
    updatePro: PropTypes.func,
    updateProImage: PropTypes.func,
    openSnackBar: PropTypes.func,
    categories: PropTypes.array,
    osmAutocomplete: PropTypes.object,
    parents: PropTypes.array
};

export default ProProfileData;
