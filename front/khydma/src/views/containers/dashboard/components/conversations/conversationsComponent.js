import React, {Component} from 'react';
import './conversationsComponent.css';
import {connect} from "react-redux";
import * as PropTypes from 'prop-types';
import {prosApi} from "../../../../reducers/pros";
import {MessagingApi} from "../../../../reducers/messaging";
import {CONST, Misc} from "../../../../../config";
import {ConversationsList, MessagesList} from "../../../../components";
import PubSubJs from "pubsub-js";
import * as queryString from 'query-string';
import {SocketServices} from "../../../../../config/services";
import {SingleConversationDialogWrapper} from "../../../../dialogs";
import {RootsActions} from "../../../../reducers/roots";

const {getProByQuery} = prosApi;
const {updateRoot} = RootsActions;
const {pubSubConstants, ioTriggers} = CONST;
const _SocketServices = SocketServices;

class ConversationsComponent extends Component {
    pubSubUnsubscriber;

    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            cnvSubbed: false,
            conversations: [],
            showMessagesDialog: false
        };
    }

    handleShowMsgsDialog = () => {
        this.setState({
            showMessagesDialog: true
        })
    };

    handleHideMsgsDialog = () => {
        this.setState({
            showMessagesDialog: false
        })
    };

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };

    componentDidMount(): void {
        this.setViewType();
        this.props.updateRoot(this.props);

        window.addEventListener("resize", this.setViewType);
        // getting conversations list

        MessagingApi.getConvs({userId: this.props.user._id}, this.convByQuerySuccess);
        if (_SocketServices.prototype.socket)
            this.convSubber();
        // subscribing to the socket io connection event to get rid of the _Socket undefined error
        this.pubSubUnsubscriber = PubSubJs.subscribe(pubSubConstants.subscribeSockets, () => {
            // getting to realtime socket updates
            this.convSubber();
        });
        this.passQueryToSingleMessage(this.props);
    }

    convSubber = () => {
        MessagingApi.onConversation((resultIo) => {
            let resIndexInCnvs = this.state.conversations.findIndex(x => x._id === resultIo._id);

            if (resultIo.userId._id === this.props.user._id)
                if (resIndexInCnvs === -1)
                    this.setState({
                        lastReceivedMessage: resultIo.lastMessageId,
                        conversations: [...this.state.conversations, resultIo]
                    });
                else {
                    let cnvClone = this.state.conversations.slice();
                    cnvClone[resIndexInCnvs] = resultIo;
                    console.log(resultIo.lastMessageId)
                    this.setState({
                        lastReceivedMessage: resultIo.lastMessageId,
                        conversations: cnvClone
                    })
                }
        })

    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        if (prevProps.location !== this.props.location) {
            this.passQueryToSingleMessage(this.props);
        }
    }

    passQueryToSingleMessage = (props) => {
        let parsed = queryString.parse(props.location.search);
        this.setState({parsed})
        if (parsed.proId)
            this.setMessageWindow(props, parsed);
    };

    sortConversationByDate = (a, b) => {
        return new Date(a.lastMessageId.creation_date) - new Date(b.lastMessageId.creation_date)
    };

    setMessageWindow = (props, parsed) => {
        getProByQuery({_id: parsed.proId}, (succ, res, err) => {
            if (succ) {
                this.setState({
                    pro: res
                });
                if (this.state.fullScreen)
                    this.handleShowMsgsDialog();
            } else {
                console.log(err);
            }
        });
    };


    convByQuerySuccess = (succ, res, err) => {
        if (succ && res && res.length > 0) {
            if (this.state.conversations.length > 0)
                [...res].forEach((cnv, idx) => {
                    if (this.state.conversations.findIndex((x) => x._id === cnv._id) === -1)
                        this.setState({
                            conversations: [...this.state.conversations, res]
                        })
                });
            else this.setState({
                conversations: res
            })
        }
    };


    convByQueryError = (err) => {
        console.log('convByQueryError = ', err)
    };

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
        if (this.pubSubUnsubscriber) {
            PubSubJs.unsubscribe(this.pubSubUnsubscriber);
        }
    }

    sendMessage = (messageData) => {
        let socketMessageBody = {
            senderId: this.props.user._id,
            text: messageData,
            userId: this.props.user._id,
            proId: this.state.pro._id,
            receiverId: this.state.pro.userId._id
        };
        _SocketServices.prototype.socket.emit(ioTriggers.newMessage, socketMessageBody);
        console.log('sendMessage = ', socketMessageBody);
    };

    render() {
        const {user} = this.props;
        const {fullScreen, conversations, pro, lastReceivedMessage, showMessagesDialog, parsed} = this.state;

        return (
            <div className="conversationComponentMainDiv">
                <div className="mainConvSpacer"/>
                <div className="fullScreenConvoPage">
                    <div className="conversationsPart">
                        <ConversationsList conversations={conversations.sort(this.sortConversationByDate).reverse()}
                                           isPro={false} pro={pro}/>
                    </div>
                    {
                        !fullScreen ?
                            <div className="messagesList">
                                <MessagesList sendMessage={this.sendMessage} parsed={parsed}
                                              profile={pro ? pro : {}} user={user} lastNewMessage={lastReceivedMessage}
                                />
                            </div> : <div/>
                    }
                    {/*<span className="revsDashHolder myReviewHolder noConvosText">Cette version ne supporte pas la messagerie</span>*/}
                </div>
                <SingleConversationDialogWrapper sendMessage={this.sendMessage} open={showMessagesDialog}
                                                 setClose={this.handleHideMsgsDialog}
                                                 setOpen={this.handleShowMsgsDialog}
                                                 profile={pro ? pro : {}} user={user}
                                                 lastNewMessage={lastReceivedMessage}
                                                 parsed={parsed}
                />
            </div>
        );
    }
}

ConversationsComponent.propTypes = {
    parameters: PropTypes.any
};

let statesToProps = (state, props) => {
    if (state.currentUsersReducer) {
        return {
            user: state.currentUsersReducer.user
        }
    } else
        return {}
};

export default connect(statesToProps, {updateRoot})(ConversationsComponent);
