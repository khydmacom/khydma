import React, {Component} from 'react';
import './dashboardComponent.css';
import {Helmet} from 'react-helmet';
import { CssBaseline, MuiThemeProvider} from "@material-ui/core";
import {withStyles} from '@material-ui/core/styles';
import {Misc} from "../../../config";

const styles = theme => ({
    content: {
        flexGrow: 1,
        paddingTop: theme.spacing.unit,
        minHeight: '90vh'
        // padding: theme.spacing.unit * 3,
    },
});


class DashboardComponent extends Component {
    state = {
        drawerWidth: 0
    };

    componentDidMount() {
        this.setViewType();

        window.addEventListener("resize", this.setViewType);
    }


    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({drawerWidth: 0});
                return;
            case 'sm':
                this.setState({drawerWidth: 0});

                return;
            case 'md':
                this.setState({drawerWidth: 300});

                return;
            case 'lg':
                this.setState({drawerWidth: 300});

                return;
            case 'xl':
                this.setState({drawerWidth: 300});

                return;
            default:
                return;
        }
    };

    render() {
        const {classes} = this.props;

        return (
            <MuiThemeProvider theme={Misc.THEME}>

                <Helmet>
                    <title>{`Tableau de bord | Khydma.com`}</title>
                </Helmet>
                <CssBaseline/>
                <main className={classes.content} style={{paddingLeft: this.state.drawerWidth}}>
                    {this.props.children}
                </main>
            </MuiThemeProvider>
        );
    }
}

export default withStyles(styles, {withTheme: true})(DashboardComponent);
