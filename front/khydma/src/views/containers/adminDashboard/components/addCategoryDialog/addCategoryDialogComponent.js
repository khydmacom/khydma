import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './addCategoryDialogComponent.css';
import * as PropTypes from 'prop-types';
import {
    Button,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel, MenuItem,
    MuiThemeProvider,
    OutlinedInput,
    Select, withStyles
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {Misc} from "../../../../../config";
import CircularProgress from '@material-ui/core/CircularProgress';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        width: '100%'
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    formControl: {
        margin: theme.spacing.unit,
        width: '100%',
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class AddCategoryDialogComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            labelWidth: 0,
            order: 0,
            parentId: null,
            label: '',
            progress: 0,
            imagePath: null
        };
    }

    componentDidMount() {
        /*setTimeout(() => {
            this.setState({
                labelWidth: document.getElementById("inputLabel").offsetWidth
            });
        }, 666);*/
    }


    handleClose = () => {
        this.props.onClose(this.props.selectedValue);
    };

    handleInput = (e) => {
        this.setState({label: e.target.value});
    };

    handleOnOn = value => {
        this.setState({
            labelWidth: ReactDOM.findDOMNode(this.InputLabelRef).offsetWidth,
        });
        if (this.props.categoryToUpdate)
            this.setState({
                label: this.props.categoryToUpdate.label,
                order: this.props.categoryToUpdate.order,
                imagePath: (this.props.categoryToUpdate.imagePath ? this.props.categoryToUpdate.imagePath : ""),
                parentId: ((this.props.categoryToUpdate.parentId && this.props.categoryToUpdate.parentId._id) ? this.props.categoryToUpdate.parentId._id : null)
            });
    };

    handleCategoryChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    successCb = (res) => {
        // console.log(res);
        if (res && this.props.file) {
            this.props.updateCategoryImage({_id: res._id, file: this.props.fileObj}, (prog) => {
                this.setState({
                    progress: prog.loaded
                })
            }, (suc) => {
                this.emptyState();
                this.props.showSnackBar((this.props.categoryToUpdate ? "Catégorie modifier avec succées!" : "Catégorie ajouter avec succées!"), "success");
                this.handleClose();
            }, this.errCb)
        } else {
            this.emptyState();
            this.props.showSnackBar((this.props.categoryToUpdate ? "Catégorie modifier avec succées!" : "Catégorie ajouter avec succées!"), "success");
            this.handleClose();
        }
    };

    emptyState = () => {
        this.props.onFileSelect(null);
        this.setState({
            parentId: null,
            label: '',
            progress: 0,
            order: 0
        });
    }

    errCb = (err) => {
        // console.log("err", err);
        this.props.showSnackBar(err.message, "error");
    };

    handleAddition = () => {
        let body = {label: this.state.label, order: this.state.order};
        if (!this.props.categoryToUpdate && this.state.parentId && this.state.parentId.length > 0)
            body['parentId'] = this.state.parentId;
        else if (this.props.categoryToUpdate) body['parentId'] = this.state.parentId;

        if (this.props.categoryToUpdate)
            this.props.updateCategory({_id: this.props.categoryToUpdate._id, ...body}, this.successCb, this.errCb);
        else this.props.newCategory(body, this.successCb, this.errCb);
    };

    render() {
        const {
            showSnackBar, newCategory, updateCategory, reloadCategories,
            classes, onClose, selectedValue, allCategories, categoryToUpdate, file,
            onFileSelect, updateCategoryImage, fileObj, ...other
        } = this.props;

        return (
            <Dialog onEnter={this.handleOnOn} onClose={this.handleClose}
                    aria-labelledby="simple-dialog-title" {...other}>
                <DialogTitle
                    id="simple-dialog-title">{categoryToUpdate ? "Modifier la catégorie" : "Ajouter une nouvelle catégorie"}</DialogTitle>
                <DialogContent>
                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <div className="columnAlign">
                            <div
                                style={{backgroundImage: "url(" + (file ? file : (this.state.imagePath ? this.state.imagePath : require('../../../../../assets/images/thumb.jpg'))) + ")"}}
                                className="categoryImageUpload shadow">
                                {this.state.progress ? <div className="progressCircleLayer">
                                    <CircularProgress variant="static" value={this.state.progress}/>
                                </div> : <div/>}
                            </div>
                            <input
                                id="___SelectFileButton___"
                                type="file"
                                accept="image/*"
                                onChange={onFileSelect}
                                hidden
                            />
                            <Button color="primary" className={classes.button} onClick={() => {
                                global.document.getElementById('___SelectFileButton___').click();
                            }}>
                                Choisissez une image
                            </Button>
                            <form className="addcategoryform" autoComplete="off">
                                <FormControl variant="outlined" color="primary" className={classes.formControl}>
                                    <TextField
                                        id="outlined-email-input"
                                        label="Nom de catégorie"
                                        className={classes.textField}
                                        type="text"
                                        name="label"
                                        color="primary"
                                        margin="normal"
                                        variant="outlined"
                                        value={this.state.label || ''}
                                        onChange={this.handleInput}
                                    />
                                </FormControl>
                                <FormControl variant="outlined" color="primary" className={classes.formControl}>
                                    <TextField
                                        label="Numéro d'ordre'"
                                        className={classes.textField}
                                        type="number"
                                        inputProps={{
                                            maxLength: "1"
                                        }}
                                        value={this.state.order || ''}
                                        onChange={this.handleCategoryChange}
                                        name="order"
                                        margin="normal"
                                        variant="outlined"
                                    />
                                </FormControl>
                                <div>
                                    <FormControl variant="outlined" className={classes.formControl}>
                                        <InputLabel
                                            ref={ref => {
                                                this.InputLabelRef = ref;
                                            }}
                                            htmlFor="outlined-age-simple"
                                        >
                                            Parent de catégorie
                                        </InputLabel>
                                        <Select
                                            value={this.state.parentId || ''}
                                            onChange={this.handleCategoryChange}
                                            input={
                                                <OutlinedInput
                                                    labelWidth={this.state.labelWidth}
                                                    name="parentId"
                                                    className={classes.textField + " " + "text"}
                                                    id="outlined-age-simple"
                                                />
                                            }
                                        >
                                            <MenuItem value={undefined}>
                                                <em>None</em>
                                            </MenuItem>
                                            {(allCategories && allCategories.length > 0) ?
                                                allCategories.map((cat, index) => {
                                                    return <MenuItem value={cat._id} key={index}>{cat.label}</MenuItem>
                                                }) : ""
                                            }
                                        </Select>
                                    </FormControl>
                                </div>
                            </form>

                            <Button disabled={this.state.label.length === 0}
                                    color="primary" variant="contained"
                                    onClick={this.handleAddition}
                                    className="confirmation"
                            >
                                <span>{categoryToUpdate ? "Modifier" : "Ajouter"}</span>
                            </Button>
                        </div>
                    </MuiThemeProvider>
                </DialogContent>
            </Dialog>
        );
    }
}

AddCategoryDialogComponent.propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool,
    allCategories: PropTypes.array,
    newCategory: PropTypes.func,
    showSnackBar: PropTypes.func,
    categoryToUpdate: PropTypes.object,
    file: PropTypes.object,
    updateCategory: PropTypes.func,
    updateCategoryImage: PropTypes.func,
    onFileSelect: PropTypes.func,
    reloadCategories: PropTypes.func,
    fileObj: PropTypes.object
};
export default withStyles(styles)(AddCategoryDialogComponent);
