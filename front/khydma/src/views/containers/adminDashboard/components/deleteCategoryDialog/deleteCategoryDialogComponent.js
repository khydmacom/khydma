import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './deleteCategoryDialogComponent.css';
import PropTypes from 'prop-types';
import {
    Button,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel, MenuItem,
    MuiThemeProvider,
    OutlinedInput,
    Select, withStyles
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {Misc} from "../../../../../config";


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class DeleteCategoryDialogComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            _id: '',
            label: ''
        };
    }

    componentDidMount() {
    }


    handleClose = () => {
        this.props.onClose(this.props.selectedValue);
    };

    handleOnOn = value => {
        if (this.props.categoryToDelete)
            this.setState({
                label: this.props.categoryToDelete.label,
                _id: this.props.categoryToDelete._id
            });
    };

    successCb = (res) => {
        this.props.showSnackBar("Catégorie supprimer avec succées!", "success");
        this.props.reloadCategories();
        this.handleClose();
    };

    errCb = (err) => {
        console.log("err", err);
        this.props.showSnackBar(err, "error");
    };

    handleDeletion = () => {
        let body = {_id: this.state._id};
        this.props.deleteCategory(body, this.successCb, this.errCb);
    };

    render() {
        const {classes, onClose, selectedValue, allCategories, categoryToDelete, showSnackBar, reloadCategories, deleteCategory, ...other} = this.props;

        return (
            <Dialog onEnter={this.handleOnOn} onClose={this.handleClose}
                    aria-labelledby="simple-dialog-title" {...other}>
                <DialogTitle
                    id="simple-dialog-title">{"Êtes-vous sûr de vouloir supprimer " + this.state.label + "?"}</DialogTitle>
                <DialogContent>
                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <div className="rowAlign">
                            <Button
                                    color="primary" variant="contained"
                                    onClick={this.handleDeletion}
                                    className="confirmation"
                            >
                                <span>Supprimer</span>
                            </Button>
                            <Button
                                    color="secondary" variant="contained"
                                    onClick={this.handleClose}
                                    className="confirmation"
                            >
                                <span>Annuler</span>
                            </Button>
                        </div>
                    </MuiThemeProvider>
                </DialogContent>
            </Dialog>
        );
    }
}

DeleteCategoryDialogComponent.propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool,
    showSnackBar: PropTypes.func,
    deleteCategory: PropTypes.func,
    categoryToDelete: PropTypes.object,
    reloadCategories: PropTypes.func
};
export default withStyles(styles)(DeleteCategoryDialogComponent);
