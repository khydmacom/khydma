import React, {Component} from 'react';
import './categoriesListComponent.css';
import classNames from 'classnames';
import {createMuiTheme, MuiThemeProvider, withStyles} from '@material-ui/core/styles';
import * as PropTypes from 'prop-types';
import AddIcon from '@material-ui/icons/Add';
import Button from "@material-ui/core/es/Button/Button";
import Fab from '@material-ui/core/Fab';
import {
    IconButton,
    Menu,
    MenuItem,
    Paper,
    Snackbar,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow
} from "@material-ui/core";
import image from '../../../../../assets/Affaires/Avocats.jpg'
import MoreVert from '@material-ui/icons/MoreVert';
import {AddCategoryDialogComponent} from "../addCategoryDialog";
import {connect} from "react-redux";
import {categoriesActions} from '../../../../reducers/categories';
import {SnackBarContentWrapper} from "../../../../components/snackBarContentWrapper";
import DeleteCategoryDialogComponent from "../deleteCategoryDialog/deleteCategoryDialogComponent";

const {getCategories, newCategory, updateCategory, removeCategory, updateCategoryImage} = categoriesActions;

const CustomTableCell = withStyles(theme => ({
    head: {
        backgroundColor: theme.palette.secondary.main,
        color: theme.palette.common.white,
    },
    body: {
        fontSize: 14,
    },
}))(TableCell);

const styles = theme => ({
    root: {
        // width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        overflowY: 'auto',
        maxHeight: '90vh'
    },
    table: {
        minWidth: 700,
    },
    row: {
        '&:nth-of-type(odd)': {
            backgroundColor: theme.palette.background.default,
        },
    },
});


class CategoriesListComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            anchorEl: null,
            open: false,
            openDel: false,
            openSnack: false,
            snackBarMessage: "",
            snackVariant: "success",
            categoryToEdit: null,
            categoryToDelete: null,
            selectedFile: null,
        };
    }

    setOpen = (open: boolean) => {
        this.setState(state => ({
            openSnack: open
        }))
    };

    setSnackMessage = (snackBarMessage: string) => {
        this.setState(state => ({
            snackBarMessage: snackBarMessage
        }))
    };

    setSnackVariant = (snackVariant: string) => {
        this.setState(state => ({
            snackVariant: snackVariant
        }))
    };

    openSnackBar = (snackBarMessage: string, variant: "success" | "warning" | "error" | "info") => {
        this.setSnackVariant(variant);
        this.setSnackMessage(snackBarMessage);
        this.handleClick();
    };

    handleClick = () => {
        this.setOpen(true);
    };

    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
            return;
        }

        this.setOpen(false);
    };

    componentDidMount(): void {
        this.loadCategories();
    };

    loadCategories = () => {
        this.props.getCategories(this.successCb, this.errCb);
    };

    handleAddDialogClickOpen = () => {
        this.setState({
            open: true,
        });
    };

    handleDeleteDialogClickOpen = () => {
        this.setState({
            openDel: true,
        });
    };

    handleDeleteDialogClose = value => {
        this.setState({openDel: false});
    };

    handleAddDialogClose = value => {
        this.setState({categoryToEdit: null, open: false});
    };

    handleMenuClick = (index, event) => {
        this.setState({['menu' + index]: event.currentTarget});
    };

    handleMenuClose = (index) => {
        this.setState({['menu' + index]: null});
    };

    setCategoryToUpdate = (row) => {
        this.setState({categoryToEdit: row})
    };

    setCategoryToDelete = (row) => {
        this.setState({categoryToDelete: row})
    };

    handleOpenEditCategoryDialog = (index, row) => {
        this.setCategoryToUpdate(row);
        this.handleMenuClose(index);
        this.handleAddDialogClickOpen();
    };

    handleOpenDeleteCategoryDialog = (index, row) => {
        this.setCategoryToDelete(row);
        this.handleMenuClose(index);
        this.handleDeleteDialogClickOpen();
    };

    successCb = (res) => {
        // console.log("succ categories list", res);
    };

    errCb = (err) => {
        // console.log("err", err);
    };

    checkMimeType = (event) => {
        let files = event.target.files;
        let err = '';

        const types = ['image/png', 'image/jpeg', 'image/gif'];

        if (types.every(type => files[0].type !== type)) {
            err += 'Ce n\'est pas un format supporté\n';
            this.openSnackBar(err, "error");
        }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false;
        }
        return true;
    };

    checkFileSize = (event) => {
        let files = event.target.files;
        let size = 4500000;
        let err = "";
        if (files[0].size > size) {
            err += 'Fichier est trop grand, veuillez choisir un fichier plus petit\n';
            this.openSnackBar(err, "error");
        }

        if (err !== '') {
            event.target.value = null;
            // console.log(err);
            return false
        }

        return true;
    };

    handleFileSelect = (event) => {
        if (event) {
            if (this.checkMimeType(event) && this.checkFileSize(event))
                this.setState({selectedFile: window.URL.createObjectURL(event.target.files[0]), fileObj: event.target.files[0]});
        } else this.setState({selectedFile: null});
    };

    render() {
        const {classes, allCategories, newCategory, updateCategory, removeCategory, updateCategoryImage} = this.props;
        const {openSnack, snackBarMessage, snackVariant, categoryToEdit, categoryToDelete, selectedFile, fileObj} = this.state;

        const ActionBtns = (row, index) => {
            return (<div>
                <IconButton
                    color="inherit"
                    aria-label="Open drawer"
                    aria-haspopup="true"
                    className={classes.menuButton}
                    onClick={this.handleMenuClick.bind(this, index)}
                >
                    <MoreVert/>
                </IconButton>
                <Menu
                    id="simple-menu"
                    anchorEl={this.state['menu' + index]}
                    open={Boolean(this.state['menu' + index])}
                    onClose={this.handleMenuClose.bind(this, index)}
                >
                    <MenuItem onClick={this.handleOpenEditCategoryDialog.bind(this, index, row)}>Modifier</MenuItem>
                    <MenuItem onClick={this.handleOpenDeleteCategoryDialog.bind(this, index, row)}>Supprimer</MenuItem>
                </Menu>
            </div>);
        };

        return (
            <div>
                <Snackbar
                    anchorOrigin={{
                        vertical: 'bottom',
                        horizontal: 'left',
                    }}
                    open={openSnack}
                    autoHideDuration={4000}
                    onClose={this.handleClose}
                >
                    <SnackBarContentWrapper
                        onClose={this.handleClose}
                        variant={snackVariant}
                        message={snackBarMessage}
                    />
                </Snackbar>
                <div className="categoriesMainDiv">
                    {(allCategories && allCategories.length > 0) ?
                        <Paper className={'mainViewContainer' + " " + classes.root}>
                            <Table className={classes.table}>
                                <TableHead>
                                    <TableRow>
                                        <CustomTableCell>Image</CustomTableCell>
                                        <CustomTableCell align="right">Nom</CustomTableCell>
                                        <CustomTableCell align="right">Ordre</CustomTableCell>
                                        <CustomTableCell align="right">Catégorie parent</CustomTableCell>
                                        <CustomTableCell align="right">Action</CustomTableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {allCategories.map((row, index) => (
                                        <TableRow className={classes.row} key={index}>
                                            <CustomTableCell component="th" scope="row">
                                                {row.imagePath ?
                                                    <img src={row.imagePath} width={100} height={70}/> : ""}
                                            </CustomTableCell>
                                            <CustomTableCell align="right">{row.label}</CustomTableCell>
                                            <CustomTableCell align="right">{row.order}</CustomTableCell>
                                            <CustomTableCell
                                                align="right">{row.parentId ? row.parentId.label : "Aucun parent"}</CustomTableCell>
                                            <CustomTableCell align="right">
                                                {ActionBtns(row, index)}
                                            </CustomTableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </Paper> : <div/>}

                </div>
                <div className="fabButtonDiv">
                    <Fab color="secondary" onClick={this.handleAddDialogClickOpen}>
                        <AddIcon/>
                    </Fab>
                </div>
                <AddCategoryDialogComponent onClose={this.handleAddDialogClose} open={this.state.open}
                                            allCategories={allCategories} newCategory={newCategory}
                                            showSnackBar={this.openSnackBar} reloadCategories={this.loadCategories}
                                            categoryToUpdate={categoryToEdit} updateCategory={updateCategory}
                                            onFileSelect={this.handleFileSelect} file={selectedFile}
                                            updateCategoryImage={updateCategoryImage} fileObj={fileObj}
                />
                <DeleteCategoryDialogComponent reloadCategories={this.loadCategories} open={this.state.openDel}
                                               onClose={this.handleDeleteDialogClose}
                                               categoryToDelete={categoryToDelete}
                                               deleteCategory={removeCategory} showSnackBar={this.openSnackBar}
                />
            </div>
        );
    }
}

CategoriesListComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

let statesToProps = (state, props) => {
    if (state.categoriesReducer) {
        return {
            allCategories: state.categoriesReducer.allCategories,
        }
    } else
        return {}
};

export default connect(statesToProps, {
    newCategory,
    getCategories,
    updateCategory,
    removeCategory,
    updateCategoryImage
})(withStyles(styles)(CategoriesListComponent));

// export default CategoriesListComponent;
