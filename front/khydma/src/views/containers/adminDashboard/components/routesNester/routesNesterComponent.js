import React, {Component} from 'react';
import './routesNesterComponent.css';
import {Misc} from "../../../../../config";

class RoutesNesterComponent extends Component {
    render() {
        const {params} = this.props.match;
        const {nestedRoute} = params;

        return (
            <>
                {Misc.adminRouter(nestedRoute, null)}
            </>
        );
    }
}

export default RoutesNesterComponent;
