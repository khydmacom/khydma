import React, {Component} from 'react';
import './adminDashboardComponent.css';
import ReactDOM from 'react-dom';
import {Link} from "@reach/router";
import {
    AppBar, Button, CssBaseline,
    Divider, Drawer,
    Hidden,
    IconButton,
    List,
    ListItem,
    ListItemIcon,
    ListItemText, MuiThemeProvider,
    Toolbar
} from "@material-ui/core";
import MenuIcon from '@material-ui/icons/Menu';
import {withStyles} from '@material-ui/core/styles';
import Category from '@material-ui/icons/Category';
import SupervisedUserCircle from '@material-ui/icons/SupervisedUserCircle';
import VerifiedUser from '@material-ui/icons/VerifiedUser';
import PermDataSetting from '@material-ui/icons/PermDataSetting';
import ViewCarousel from '@material-ui/icons/ViewCarousel';
import {Misc, CONST} from "../../../config";
import {connect} from "react-redux";
import {AuthActions} from "../../reducers/auth";
import {CurrentUsersActions} from "../../reducers/currentUsers";
import PropTypes from "prop-types";
import {redirectTo} from "@reach/router";
import logoFull from "../../../assets/icons/final/full.png";


const {reAuth, rejectToken} = AuthActions;
const {checkIfCurrentIsAdmin, emptyCurrentUser} = CurrentUsersActions;
const {pubSubConstants, localStorageIndexes} = CONST;
const pubSubJs = require('pubsub-js');


const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    grow: {
        flexGow: 1
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
        flexGow: 1
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        paddingTop: theme.spacing.unit,
        // padding: theme.spacing.unit * 3,
    },
});


class AdminDashboardComponent extends Component {
    state = {
        open: false,
        // selectedValue: emails[1],
        isAdmin: false,
        mobileOpen: false,
        drawerWidth: 0
    };

    handleClickOpen = () => {
        this.setState({
            open: true,
        });
    };

    handleClose = value => {
        this.setState({selectedValue: value, open: false});
    };

    handleDrawerToggle = () => {
        this.setState(state => ({mobileOpen: !state.mobileOpen}));
    };

    setMainViewPadding = (size) => {
        this.setState({drawerWidth: size});
    };

    handleLogout = () => {
        if (JSON.parse(localStorage.getItem(localStorageIndexes.user)))
            this.props.rejectToken({refreshToken: JSON.parse(localStorage.getItem(localStorageIndexes.user)).token.refreshToken}, (res) => {
                this.props.emptyCurrentUser();
                this.props.navigate('/');
            }, (err) => {
                console.log("logout err = ", err);
            });
    };

    componentDidMount(): void {

        this.setViewType();
        window.addEventListener("resize", this.setViewType);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.setViewType);
    }

    setViewType = () => {
        if (Misc.getSizeType() === 'xs')
            this.setMainViewPadding(0);
        else this.setMainViewPadding(drawerWidth);
    };

    render() {
        const {classes, theme} = this.props;

        const RenderHeader = (
            <div className={classes.grow}>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon/>
                        </IconButton>
                        <div className="headerAdminDash">
                            <span className="headerTitleAdmin">Tableau de bord</span>
                            <div className="rowAlign">
                                {/*<Button variant="contained" color="secondary" onClick={this.handleClickOpen}>
                                    Nouvelle catégorie
                                </Button>*/}
                                <Button color="secondary" className={classes.button} onClick={this.handleLogout}>
                                    Déconnexion
                                </Button>
                            </div>
                        </div>
                    </Toolbar>
                </AppBar>
                {/*<AddCategoryDialogComponent onClose={this.handleClose} open={this.state.open}/>*/}
            </div>
        );

        const RenderDrawer = (
            <div ref={(ref) => this.DrawerRef = ref}>
                <div className="columnAlign">
                    <Link to={'/'}>
                        <img src={logoFull} alt={"Khydma.com logo"} className="logoHeadSideNav"/>
                    </Link>
                </div>
                <Divider/>
                <List>
                    <Link to="/admin/">
                        <ListItem button>
                            <ListItemIcon>
                                <ViewCarousel/>
                            </ListItemIcon>
                            <ListItemText primary={"Vue d'ensemble"}/>
                        </ListItem>
                    </Link>
                    <Link to="/admin/params">
                        <ListItem button>
                            <ListItemIcon>
                                <PermDataSetting/>
                            </ListItemIcon>
                            <ListItemText primary={"Paramétres"}/>
                        </ListItem>
                    </Link>
                    <Divider/>
                    <Link to="/admin/users">
                        <ListItem button>
                            <ListItemIcon>
                                <SupervisedUserCircle/>
                            </ListItemIcon>
                            <ListItemText primary={"Utilisateurs"}/>
                        </ListItem>
                    </Link>

                    <Link to="/admin/pros">
                        <ListItem button>
                            <ListItemIcon>
                                <VerifiedUser/>
                            </ListItemIcon>
                            <ListItemText primary={"Professionnels"}/>
                        </ListItem>
                    </Link>
                    <Link to="/admin/categories">
                        <ListItem button>
                            <ListItemIcon>
                                <Category/>
                            </ListItemIcon>
                            <ListItemText primary={"Catégories"}/>
                        </ListItem>
                    </Link>
                </List>
            </div>
        );

        return (
            <MuiThemeProvider theme={Misc.THEME}>
                <CssBaseline/>
                {RenderHeader}
                <nav className={classes.drawer}>
                    {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                    <Hidden smUp implementation="css">
                        <Drawer
                            container={this.props.container}
                            variant="temporary"
                            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                            open={this.state.mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                        >
                            {RenderDrawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open
                        >
                            {RenderDrawer}
                        </Drawer>
                    </Hidden>
                </nav>
                <main className={classes.content} style={{paddingLeft: this.state.drawerWidth}}>
                    <div className={classes.toolbar}/>
                    {this.props.children}
                </main>
            </MuiThemeProvider>
        );
    }
}

let statesToProps = (state, props) => {
    if (state.currentUsersReducer && state.authReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            isAdmin: state.currentUsersReducer.isAdmin
        }
    } else
        return {}
};

export default connect(statesToProps, {
    checkIfCurrentIsAdmin,
    reAuth,
    rejectToken,
    emptyCurrentUser
})(withStyles(styles, {withTheme: true})(AdminDashboardComponent));
