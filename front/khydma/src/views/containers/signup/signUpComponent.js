import React, {Component} from 'react';
import './signupComponent.css';
import classNames from 'classnames';
import {withStyles, MuiThemeProvider, createMuiTheme} from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import Button from "@material-ui/core/es/Button/Button";
import {Formik} from 'formik';
import {CONST, Misc} from "../../../config";
import {AuthActions} from '../../reducers/auth'
import {connect} from "react-redux";
import {Link} from "@reach/router";
import {CurrentUsersActions} from "../../reducers/currentUsers";
import SnackBarContentWrapperComponent from "../../components/snackBarContentWrapper/snackBarContentWrapperComponent";
import {Snackbar} from "@material-ui/core";
import {RootsActions} from "../../reducers/roots";

const pubSubJs = require('pubsub-js');


const {signUp} = AuthActions;
const {getCurrentProfile} = CurrentUsersActions;

const {updateRoot} = RootsActions;


class SignUpComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            openSnack: false,
            snackBarMessage: '',
            snackVariant: 'success'
        }
    }
    handleChange = name => event => {
        this.setState({
            [name]: event.target.value,
        });
    };

    componentDidMount(): void {
        this.props.updateRoot(this.props);
    }

    handleSnackClose = () => {
        this.setState({
            openSnack: false
        })
    }

    setSnackOpen = () => {
        this.setState({
            openSnack: true
        })
    };

    setSnackMessage = (snackBarMessage: string) => {
        this.setState(state => ({
            snackBarMessage: snackBarMessage
        }))
    };

    setSnackVariant = (snackVariant: string) => {
        this.setState(state => ({
            snackVariant: snackVariant
        }))
    };

    openSnackBar = (snackBarMessage: string, variant: "success" | "warning" | "error" | "info") => {
        this.setSnackVariant(variant);
        this.setSnackMessage(snackBarMessage);
        this.setSnackOpen();
    };

    render() {
        const {validateForm} = Misc;
        const {openSnack, snackBarMessage, snackVariant} = this.state;

        if (this.props.isLoggedIn)
            this.props.navigate('/');
        return (
            <div className="loginComponent">
                {/*<div className="headingTitle">
                    <span>S'inscrire</span>
                </div>*/}
                <Snackbar
                    anchorOrigin={{vertical: 'bottom', horizontal: 'left'}}
                    open={openSnack} onClose={this.handleSnackClose}
                >
                    <SnackBarContentWrapperComponent onClose={this.handleSnackClose} variant={snackVariant} message={snackBarMessage}/>
                </Snackbar>
                <div className="fields">
                    <span className="signingTitle">Créez votre compte</span>
                    <Formik
                        initialValues={{
                            email: undefined,
                            password: undefined,
                            name: undefined,
                            lastName: undefined,
                            password2: undefined
                        }}
                        validate={validateForm}
                        onSubmit={(values, {setSubmitting}) => {
                            this.props.signUp(values, (res) => {
                                this.props.getCurrentProfile((profile) => {
                                    this.props.navigate(this.props.pathToGetBackTo ? this.props.pathToGetBackTo : this.props.proForm ? '/proform' : '/', {replace: true});
                                }, (err) => {
                                    console.log("err = ", err);
                                });
                                setSubmitting(false);
                            }, (err) => {
                                console.log("signup err = ", err);
                                this.openSnackBar(err.message, "error");
                                setSubmitting(false);
                            })
                        }}
                    >
                        {({
                              values,
                              errors,
                              touched,
                              handleChange,
                              handleBlur,
                              handleSubmit,
                              isSubmitting,
                          }) => (
                            <MuiThemeProvider theme={Misc.FORM_THEME}>
                                <form onSubmit={handleSubmit} className="fields noBorder">
                                    <TextField
                                        label="Nom"
                                        className="text"
                                        type="text"
                                        name="name"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        color="secondary"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.name && touched.name && errors.name)}
                                        helperText={errors.name && touched.name && errors.name}
                                        value={values.name || ''}
                                    />
                                    <TextField
                                        label="Prénom"
                                        className="text"
                                        type="text"
                                        name="lastName"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.lastName && touched.lastName && errors.lastName)}
                                        helperText={errors.lastName && touched.lastName && errors.lastName}
                                        value={values.lastName || ''}
                                    />
                                    <TextField
                                        label="E-mail"
                                        className="text"
                                        type="email"
                                        name="email"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        error={!!(errors.email && touched.email && errors.email)}
                                        helperText={errors.email && touched.email && errors.email}
                                        value={values.email || ''}
                                    />
                                    <TextField
                                        label="Mot de pass"
                                        className="text"
                                        type="password"
                                        name="password"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.password || ''}
                                        error={!!(errors.password && touched.password && errors.password)}
                                        helperText={errors.password && touched.password && errors.password}
                                    />
                                    <TextField
                                        label="Répéter le mot de pass"
                                        className="text"
                                        type="password"
                                        name="password2"
                                        autoComplete="off"
                                        margin="normal"
                                        variant="outlined"
                                        onChange={handleChange}
                                        onBlur={handleBlur}
                                        value={values.password2 || ''}
                                        error={!!(errors.password2 && touched.password2 && errors.password2)}
                                        helperText={errors.password2 && touched.password2 && errors.password2}
                                    />
                                    <div className="buttonContainer">
                                        <Button type="submit" disabled={Object.keys(errors).length > 0 || isSubmitting}
                                                color="primary" variant="contained" className="signBtn">
                                            Créer compte
                                        </Button>
                                    </div>
                                </form>
                            </MuiThemeProvider>
                        )}
                    </Formik>
                    {/*<div className="buttonContainer">
                        <Button color="secondary" variant="contained" className="signBtn">
                            Créer compte
                        </Button>
                    </div>*/}
                   {/* <div className="loginSocialChoice">Ou connectez-vous avec</div>

                    <div className="buttonContainer">
                        <Button variant="contained" className="signBtn fb">
                            Facebook
                        </Button>
                    </div>
                    <div className="buttonContainer">
                        <Button variant="contained" className="signBtn gg">
                            Google
                        </Button>
                    </div>*/}
                </div>

                <div className="switchSign">
                    <span>Vous avez déjà un compte?  </span>
                    <Link to="/login">
                        <Button color="secondary">Se connecter</Button>
                    </Link>
                </div>
            </div>
        );
    }
}

/*SignUpComponent.propTypes = {
    classes: PropTypes.object.,
};*/

let statesToProps = (state, props) => {
    if (state.authReducer && state.proFormFillingReducer && state.rootsReducer) {
        return {
            isLoggedIn: state.authReducer.isLoggedIn,
            proForm: state.proFormFillingReducer.proForm,
            pathToGetBackTo: state.rootsReducer.pathToGetBackTo
        }
    } else
        return {}
};

export default connect(statesToProps, {signUp, getCurrentProfile, updateRoot})(SignUpComponent);
