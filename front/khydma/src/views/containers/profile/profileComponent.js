import React, {Component} from 'react';
import './profileComponent.css';
import {
    GalleryProfileComponent,
    ProfileHeaderComponent,
    ProfileIntroductionComponent,
    ProfileMetricsAndLinksComponent, ReviewsListProfileComponent, ReviewsProfileComponent
} from "./components";
import {connect} from 'react-redux';
import {prosActions} from '../../reducers/pros';
import {RootsActions} from '../../reducers/roots';
import * as PropTypes from 'prop-types';
import ProApi from "../../reducers/pros/prosApi";
import {Helmet} from 'react-helmet';
import * as queryString from "query-string";
import PubsubJs from 'pubsub-js';

const {getProByAttribute} = prosActions;
const {updateToGetBack, updateRoot} = RootsActions;

class ProfileComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            gallery: [],
            myReview: null
        };
    }

    fetchGalleryImages = () => {
        // console.log({proId: this.props.proId});
        ProApi.getProGallery({proId: this.props.proId}, (suc, res, err) => {
            // console.log(suc, res, err);
            if (suc)
                this.setState({
                    gallery: res
                });
        })
    };

    fetchReviews = () => {
        ProApi.getAllReviewsByQuery({proId: this.props.proId}, (suc, allRevs, err) => {
            if (suc) {
                // updating my review
                if (this.props.user)
                    ProApi.getReviewByQuery({
                        userId: this.props.user._id,
                        proId: this.props.proId
                    }, (succ, singleRev, _err) => {
                        if (succ) {
                            if (this.state.myReview) {
                                let myReview = {...this.state.myReview};
                                let keys = Object.keys(singleRev);
                                keys.forEach((k, idx) => {
                                    myReview[k] = singleRev[k];
                                    this.setState({myReview});
                                });
                            } else this.setState({myReview: {...singleRev}});
                        }
                    });

                // updating the reviews list
                console.log(allRevs);
                if (allRevs || Object.keys(allRevs).length > 0)
                    this.setState({
                        reviews: allRevs.reviews,
                        stats: allRevs.stats
                    });
                else this.setState({
                    reviews: null,
                    stats: null,
                });
            }
        })
    };

    componentDidMount(): void {
        this.props.updateRoot(this.props);
        this.getProByQueryId();
        // this.props.getProByAttribute(this.isSuccess, this.isError, {_id: this.props.proId});
        this.props.updateToGetBack(this.props.isLoggedIn ? '' : this.props.location.pathname);
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        this.fetchRevsAndGallery();
    }

    fetchRevsAndGallery = () => {
        if (this.state.gallery.length === 0)
            this.fetchGalleryImages();
        if (!this.state.reviews)
            this.fetchReviews();
    };

    getProByQueryId = () => {
        ProApi.getProByQuery({_id: this.props.proId}, this.isSuccess);
    };


    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        if (prevProps !== this.props) {
            this.passQueryToShowImage(this.props);
            this.fetchRevsAndGallery();
        }
        if (prevProps.proId !== this.props.proId) {
            this.getProByQueryId();
            this.props.updateToGetBack(this.props.isLoggedIn ? '' : this.props.location.pathname);
        }
    }

    passQueryToShowImage = (props) => {
        let parsed = queryString.parse(props.location.search);
        // console.log(parsed);
        if (this.state.gallery.length > 0)
            if (parsed.photo) {
                let filtered = this.state.gallery.filter(x => x._id === parsed.photo);
                if (filtered[0])
                    PubsubJs.publish('showImageFromLink', filtered[0]);
            }
    };

    isSuccess = (succ, res, err) => {
        // called by the callback function of getProByQuery
        if (succ) {
            this.setState({
                proByQuery: res
            });
            this.fetchRevsAndGallery();
        } else {
            console.log(err, this.props);
        }
        console.log(res)
        // this.fetchRevsAndGallery();
    };

    isError = (err) => {
        console.log(err);
    };

    deleteReview = () => {
        ProApi.deleteReview({_id: this.state.myReview}, (succ, res, err) => {
            // console.log(succ, res, err);
            if (succ) {
                this.fetchReviews();
                this.setState({
                    myReview: null
                })
            }
        })
    };

    render() {
        const {isLoggedIn, navigate, user} = this.props;
        const {proByQuery, myReview, reviews, stats} = this.state;
        return proByQuery ?
            <div className="profileComponent">
                <Helmet>
                    <meta name="robots" content="index, follow"/>
                    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
                    <title>{`${proByQuery.businessName} | ${proByQuery.categoryId.label} | ${proByQuery.locString} -- khydma.com`}</title>
                    <meta property="og:title"
                          content={`${proByQuery.businessName} | ${proByQuery.categoryId.label} | ${proByQuery.locString} -- khydma.com`}/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:description"
                          content={`Retrouver RAPIDEMENT le numéro de téléphone et adresse du ${proByQuery.businessName}, ${proByQuery.categoryId.label}, ainsi que beaucoup d'autres professionel en Tunisie. ${proByQuery.categoryId.label} ${proByQuery.locString} -- khydma.com`}/>
                    <meta property="og:url" content={`https://khydma.com/profile/${proByQuery._id}`}/>
                    <meta property="og:image" content={proByQuery.businessImagePath}/>
                    <meta property="og:image:type" content="image/png"/>
                    <meta property="og:image:width" content="298"/>
                    <meta property="og:image:height" content="298"/>
                    <meta property="og:image:alt"
                          content={`${proByQuery.businessName} | ${proByQuery.categoryId.label} | ${proByQuery.locString} -- khydma.com`}/>
                    <meta name="description"
                          content={`Retrouver RAPIDEMENT le numéro de téléphone et adresse du ${proByQuery.businessName}, ${proByQuery.categoryId.label}, ainsi que beaucoup d'autres professionel en Tunisie. ${proByQuery.categoryId.label} ${proByQuery.locString} -- khydma.com`}/>

                </Helmet>
                <div className="mainViewContainer columnAlign">
                    <div className="profileContainer">
                        <ProfileHeaderComponent pro={proByQuery} isLoggedIn={isLoggedIn} navigate={navigate}
                                                user={user} onRevUpdate={this.fetchReviews} myReview={myReview}/>
                    </div>

                    <div className="metricsProfile">
                        <ProfileMetricsAndLinksComponent pro={proByQuery} stats={stats ? stats : {}}/>
                    </div>
                    <div className="metricsProfile">
                        <ProfileIntroductionComponent
                            introduction={proByQuery.introduction} links={proByQuery.internetLinks}/>
                    </div>
                    <div className="metricsProfile">
                        <GalleryProfileComponent gallery={this.state.gallery}/>
                    </div>
                    {
                        reviews && stats && stats.total ? [
                            <div key={66} className="metricsProfile">
                                <ReviewsProfileComponent stats={stats}/>
                            </div>,
                            <div key={69} className="metricsProfile">
                                <ReviewsListProfileComponent deleteReview={this.deleteReview}
                                                             reviewsList={reviews}
                                                             myReview={myReview}/>
                            </div>
                        ] : <div/>
                    }
                </div>
                <div className="profileComponentSpacerVertical"/>
            </div>
            : <div/>;
    }
}

ProfileComponent.propTypes = {
    getProByAttribute: PropTypes.func,
    proId: PropTypes.string,
    proByQuery: PropTypes.object,
    isLoggedIn: PropTypes.bool
};

let statesToProps = (state, props) => {
    if (state.prosReducer && state.authReducer && state.rootsReducer && state.currentUsersReducer) {
        return {
            user: state.currentUsersReducer.user,
            proByQuery: state.prosReducer.proByQuery // to be rechecked
        }
    } else
        return {}
};

export default connect(statesToProps, {
    getProByAttribute, updateToGetBack, updateRoot
})(ProfileComponent);
