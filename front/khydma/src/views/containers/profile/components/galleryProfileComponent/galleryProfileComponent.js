import React, {Component} from 'react';
import './galleryProfileComponent.css';
import PropTypes from "prop-types";
import OwlCarousel from 'react-owl-carousel';
import {Link} from "@reach/router";

import {Misc} from '../../../../../config';

import 'owl.carousel/dist/assets/owl.carousel.css';
import 'owl.carousel/dist/assets/owl.theme.default.css';
import {ButtonBase} from "@material-ui/core";
import {FullImageGalleryDialog} from "../../../../dialogs/fullImageGalleryDialog";
import PubSubJs from 'pubsub-js';


class GalleryProfileComponent extends Component {

    constructor(props) {
        super(props);
        this.state = {
            carouselItem: 3,
            open: false,
            imageToShow: null
        }
    }

    handleDialogOpen = (image) => {
        // console.log(image);
        this.setState({open: true, imageToShow: image});
    };

    handleDialogClose = () => {
        this.setState({open: false})
    };

    componentWillMount() {
        Misc.getSizeType();
    };

    componentDidMount() {
        PubSubJs.subscribe('showImageFromLink', (text, image) => {
            this.handleDialogOpen(image)
        });
        window.addEventListener("resize", this.carouselItemsNumber);
    };

    componentWillUnmount() {
        window.removeEventListener("resize", this.carouselItemsNumber);
    }

    carouselItemsNumber = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({
                    carouselItem: 1
                });
                return;
            case 'sm':
                this.setState({
                    carouselItem: 2
                });
                return;
            case 'md':
                this.setState({
                    carouselItem: 3
                });
                return;
            case 'lg':
                this.setState({
                    carouselItem: 3
                });
                return;
            case 'xl':
                this.setState({
                    carouselItem: 3
                });
                return;
            default:
                return;
        }
    };

    render() {
        const {open, imageToShow} = this.state;

        return this.props.gallery.length > 0 ? (
            <div className="mainProfileGallery">
                <span className="labelIntro">Galerie</span>
                <div className="images">
                    <OwlCarousel
                        id="galleryCarousel"
                        className="owl-theme"
                        center={(this.props.gallery.length > 3)}
                        // loop={true}
                        autoplayHoverPause={false}
                        items={this.state.carouselItem}
                    >
                        {
                            this.props.gallery.map((val, index) => {
                                return <Link key={index} to={`/profile/${val.proId}/?photo=${val._id}`}>
                                    <ButtonBase>
                                        <div className="galleryImage">
                                            <img src={val.imagePath} alt={val.description}/>
                                        </div>
                                    </ButtonBase>
                                </Link>
                            })
                        }
                    </OwlCarousel>
                </div>
                {/*the dialog to show a fullSize image*/}
                <FullImageGalleryDialog image={imageToShow} open={open} setClose={this.handleDialogClose}
                                        setOpen={this.handleDialogOpen}/>
            </div>
        ) : <div/>;
    }
}

GalleryProfileComponent.propTypes = {
    gallery: PropTypes.array
};

export default GalleryProfileComponent;
