import React, {Component} from 'react';
import './profileHeaderComponent.css';
import {Button} from "@material-ui/core";
import * as PropTypes from 'prop-types';
import Call from '@material-ui/icons/Call';
import NearMe from '@material-ui/icons/NearMe';
import Message from '@material-ui/icons/Message';
import profileFiller from '../../../../../assets/images/no.png';
import {
    NewMessageDialogWrapper,
    NewReviewDialogWrapper,
    RequestLoginDialogWrapper,
    ShowPhoneDialogWrapper
} from "../../../../dialogs";
import Star from '@material-ui/icons/Star';
import ProApi from "../../../../reducers/pros/prosApi";
import {Link} from "@reach/router";

//https://maps.google.com/?q=35.8885378,10.5939698

class ProfileHeaderComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false,
            openLogin: false,
            openReview: false,
            openPhone: false,
            loginRequestMessage: ''
        }
    }

    componentDidMount(): void {
        // console.log("the pro = ", this.props.pro);
    }

    handleDialogOpen = () => {
        if (this.props.isLoggedIn)
            this.props.navigate(`/dashboard/conversations?proId=${this.props.pro._id}`);
        else {
            this.setState({
                loginRequestMessage: 'Pour contacter le professionnel, vous devez vous identifier.'
            });
            this.handleRequestDialogOpen()
        }
    };
    handleRequestDialogOpen = () => {
        this.setState({openLogin: true})
    };

    reviewDialogRequest = () => {
        if (this.props.isLoggedIn)
            this.handleReviewDialogOpen();
        else {
            this.setState({
                loginRequestMessage: 'Pour laisser un avis, vous devez vous connecter'
            });
            this.handleRequestDialogOpen()
        }
    }

    handleReviewDialogOpen = () => {
        this.setState({openReview: true})
    };

    handleReviewDialogClose = () => {
        this.setState({openReview: false})
    };

    handlePhoneDialogOpen = () => {
        this.setState({openPhone: true})
    };

    handlePhoneDialogClose = () => {
        this.setState({openPhone: false})
    };

    handleDialogClose = () => {
        this.setState({open: false});
    };
    handleRequestDialogClose = () => {
        this.setState({openLogin: false});
    };

    handleReviewConfirm = (rev) => {
        // console.log(rev);
        ProApi.addSingleReview(rev, (succ, review, err) => {
            if (succ) {
                // console.log(review);
                this.props.onRevUpdate();
            } else console.log(err);
        });
        this.handleReviewDialogClose();
    };

    render() {
        const {pro, user, myReview} = this.props;
        const {open, openLogin, openReview, openPhone, loginRequestMessage} = this.state;

        return pro ? <div className="mainProfileHeaderDiv">
            <img className="profileAvatar" src={pro.businessImagePath ? pro.businessImagePath : profileFiller}
                 alt={pro.businessName}/>
            <div className="profileDataDiv">
                {/*<span className="dataDivFiller"></span>*/}
                <h1 className="profileName">{pro.businessName}</h1>
                <h1 className="profileSubName">{`${pro.categoryId.label} à ${pro.locString}`}</h1>

                {/*buttons to show when small screen*/}
                <div className="profileHeaderButtons">
                    <div className="profileHeaderButtonDiv">
                        <Button className="profileHeaderActionButtons" variant="contained" color="secondary"
                                onClick={this.handleDialogOpen}
                        >
                            <Message className="profileHeaderActionButtonsIcon"/>
                        </Button>
                    </div>
                    <div className="profileHeaderButtonDiv">
                        <Button className="profileHeaderActionButtons" variant="contained"
                                color="secondary" onClick={this.handlePhoneDialogOpen}>
                            <Call className="profileHeaderActionButtonsIcon"/>
                        </Button>
                    </div>
                    {/*<div className="profileHeaderButtonDiv">
                        <a href={`https://maps.google.com/?q=${pro.loc[1] + ',' + pro.loc[0]}`}>
                            <Button className="profileHeaderActionButtons" variant="contained"
                                    color="secondary">
                                <NearMe className="profileHeaderActionButtonsIcon"/>
                            </Button>
                        </a>
                    </div>*/}
                    <div className="profileHeaderButtonDiv">
                        <Button className="profileHeaderActionButtons goldButton" variant="contained"
                                onClick={this.reviewDialogRequest}
                        >
                            <Star className="profileHeaderActionButtonsIcon"/>
                        </Button>
                    </div>
                </div>

                {/*buttons to be shown when big screen*/}
                <div className="profileButtons">
                    <div className="profileButtonDiv">
                        <Button className="profileActionButtons" variant="contained" color="secondary"
                                onClick={this.handleDialogOpen}
                        >
                            <Message className="profileActionButtonsIcon"/>
                            Message
                        </Button>
                    </div>
                    <div className="profileButtonDiv">
                        <Button className="profileActionButtons" variant="contained" color="secondary" onClick={this.handlePhoneDialogOpen}>
                            <Call className="profileActionButtonsIcon"/>
                            Appel
                        </Button>
                    </div>
                    {/*<Button className="profileActionButtons" variant="contained"
                            color="secondary">
                        <a href={`https://maps.google.com/?q=${pro.loc[1] + ',' + pro.loc[0]}`}>

                            <NearMe className="profileHeaderActionButtonsIcon"/>
                            Y aller
                        </a>
                    </Button>*/}
                    <div className="profileButtonDiv">
                        <Button className="profileActionButtons goldButton" variant="contained"
                                onClick={this.reviewDialogRequest}
                        >
                            <Star className="profileActionButtonsIcon"/>
                            Avis
                        </Button>
                    </div>
                </div>
            </div>
            <NewMessageDialogWrapper open={open} setClose={this.handleDialogClose}
                                     setOpen={this.handleDialogOpen} pro={pro} user={user}/>
            <RequestLoginDialogWrapper open={openLogin} setOpen={this.handleRequestDialogOpen}
                                       message={loginRequestMessage}
                                       setClose={this.handleRequestDialogClose} navigate={this.props.navigate}
            />
            <NewReviewDialogWrapper open={openReview} myReview={myReview} setOpen={this.handleReviewDialogOpen}
                                    setClose={this.handleReviewDialogClose} pro={pro} user={user}
                                    setConfirm={this.handleReviewConfirm}
            />
            <ShowPhoneDialogWrapper open={openPhone} setClose={this.handlePhoneDialogClose}
                                    setOpen={this.handlePhoneDialogOpen} pro={pro} user={user}
            />

        </div> : <div/>
    }
}

ProfileHeaderComponent.propTypes = {
    pro: PropTypes.object,
    user: PropTypes.object,
    isLoggedIn: PropTypes.bool,
    navigate: PropTypes.func,
    onRevUpdate: PropTypes.func,
    myReview: PropTypes.object
};


export default ProfileHeaderComponent;
