import React, {Component} from 'react';
import './reviewsListProfileComponent.css';
import {Button, IconButton} from "@material-ui/core";
import {ReviewsContentComponent} from "../reviewsContent";
import * as PropTypes from 'prop-types';
import Delete from '@material-ui/icons/Delete';
import {ConfirmReviewDelete} from "../../../../dialogs";

class ReviewsListProfileComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            reviews: [],
            showMore: true,
            open: false
        }
    }

    setRevDialogOpen = () => {
        this.setState({
            open: true
        });
    };

    setRevDialogClose = () => {
        this.setState({
            open: false
        });
    };

    setDeleteConfirm = () => {
        this.props.deleteReview();
        this.setRevDialogClose();
    };


    componentDidMount() {

    }

    showMore() {
        this.setState({
            showMore: !this.state.showMore
        });
    }

    render() {
        const {reviewsList, myReview} = this.props;

        return (
            <div className="mainReviewsListProfile">
                {
                    myReview ? [<div key="455" className="myReviewHolder">
                        <ReviewsContentComponent review={myReview}/>
                        <IconButton onClick={this.setRevDialogOpen}>
                            <Delete/>
                        </IconButton>
                    </div>, <div key="455454" className="vertDivRev"/>,
                        <div key="45555" className="myrevAndOthersDev"/>,
                        <div key="44455" className="vertDivRev"/>
                    ] : <div/>
                }
                {(reviewsList.length > 5 && this.state.showMore) ?
                    reviewsList.slice(0, 4).map((rev, index) => {
                        if (!myReview || rev._id !== myReview._id)
                            return <ReviewsContentComponent key={index} review={rev}/>
                    }) : reviewsList.map((rev, index) => {
                        if (!myReview || rev._id !== myReview._id)
                            return <ReviewsContentComponent key={index} review={rev}/>
                    })
                }
                {reviewsList.length > 5 ? <Button onClick={this.showMore.bind(this)}
                                                  color="secondary">{this.state.showMore ? 'Afficher plus d\'avis' : 'Afficher moins d\'avis'}</Button> :
                    <div/>}
                <ConfirmReviewDelete open={this.state.open} setOpen={this.setRevDialogOpen}
                                     setClose={this.setRevDialogClose} setConfirm={this.setDeleteConfirm}
                />
            </div>
        );
    }
}

ReviewsListProfileComponent.propTypes = {
    reviewsList: PropTypes.array,
    myReview: PropTypes.object,
    deleteReview: PropTypes.func
};

export default ReviewsListProfileComponent;
