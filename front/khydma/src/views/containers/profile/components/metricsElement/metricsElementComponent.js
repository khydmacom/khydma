import React, {Component} from 'react';
import './metricsElementComponent.css';
import {Button} from "@material-ui/core";
// import Message from "@material-ui/core/SvgIcon/SvgIcon";
import Call from '@material-ui/icons/Call';
import Message from '@material-ui/icons/Message';
import Star from '@material-ui/icons/Star';
import {faFacebook, faInstagram, faYoutube} from "@fortawesome/free-brands-svg-icons"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";


class MetricsElementComponent extends Component {
    render() {
        const {props} = this;
        return (
            <div className="mainProfileMetrics">
                <span className="label">{props.label}</span>
                <span className="counter">
                    {props.label === 'Avis' ? <Star className="star"/> : (
                        props.label === 'Appels' ? <Call color="secondary"/> : <span/>)
                    }
                    {
                        props.label === 'Tarif' ? <div>{(props.value ? props.value + "TND - " : "")} <small>Prestation</small></div> :
                        <div>{props.value}</div>
                    }
                </span>
            </div>
        );
    }
}

export default MetricsElementComponent;
