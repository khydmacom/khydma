import React, {Component} from 'react';
import './reviewsCounterComponent.css';
import Star from '@material-ui/icons/Star';
import {ProgressBarsComponent} from "../progressBars";
import * as PropTypes from 'prop-types';
import {Misc} from '../../../../../config';

class ReviewsCounterComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            totalAvrg: 0,
            totalReviewers: 0, // 93
            allReviewsNumber: 0
        }
    }

    componentDidMount(): void {
        this.calculateAvgReviewers();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps !== this.props)
            this.calculateAvgReviewers();
    }

    calculateAvgReviewers = () => {
        console.log( this.props.reviewsLevel);
        let totalReviewers = this.props.reviewsLevel.total;
        let allReviewsNumber = 0;
        let stateObj = {};
        for (let rev of Object.keys(this.props.reviewsLevel)) {
            if (rev !== 'total')
                allReviewsNumber += (Misc.rateParser(rev) * this.props.reviewsLevel[rev]);

            stateObj['totalReviewers'] = totalReviewers;
            stateObj['allReviewsNumber'] = allReviewsNumber;
            stateObj['totalAvrg'] = (allReviewsNumber / totalReviewers).toFixed(0);
        }
        this.setState(stateObj);
        // console.log('ReviewsCounterComponent = >', this.state, totalReviewers, allReviewsNumber);
    };

    render() {
        return (
            <div className="mainProfileReviewsCounter">
                <div className="total">
                    <span>
                        <Star className="star star_size_counter"/>
                        <span className="total_avg_text">{this.state.totalAvrg}</span>
                    </span>
                    <div>
                        <span className="total_avg_text biggerTextRevs">{this.state.totalReviewers + " Avis"}</span>
                    </div>
                </div>
                <div className="progressBars">
                    <ProgressBarsComponent reviewsLevel={this.props.reviewsLevel}
                                           totalReviewers={this.state.totalReviewers}/>
                </div>
            </div>
        );
    }
}

ReviewsCounterComponent.propTypes = {
    reviewsLevel: PropTypes.object
};

export default ReviewsCounterComponent;
