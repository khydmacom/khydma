import React, {Component} from 'react';
import './detailsCardComponent.css';
import {Button} from "@material-ui/core";
// import Message from "@material-ui/core/SvgIcon/SvgIcon";
import Call from '@material-ui/icons/Call';
import Message from '@material-ui/icons/Message';
import Star from '@material-ui/icons/Star';
import {faFacebook, faInstagram, faYoutube} from "@fortawesome/free-brands-svg-icons"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ProfileMetricsAndLinksComponent} from "../profileMetricsAndLinks";


class DetailsCardComponent extends Component {
    render() {
        return (
            <div className="detailsCardMainComponent shadow">
                <div>
                    <ProfileMetricsAndLinksComponent/>
                </div>
                <div className="verticalSeparator"/>
                <div className="details">
                    <ul style={{listStyle: 'none'}}>
                        <li>
                            <span className="daysDetailsCard _title">Heures d'ouverture: </span>
                        </li>
                        <li>
                            <span className="daysDetailsCard">Lundi - Vendredi</span>
                        </li>
                        <li>
                            <span className="daysDetailsCard timeDetailsCard">De: <span>8h</span></span>
                        </li>
                        <li>
                            <span className="daysDetailsCard timeDetailsCard">A: <span>16h</span></span>
                        </li>
                    </ul>
                </div>
            </div>
        );
    }
}

export default DetailsCardComponent;
