import React, {Component} from 'react';
import './reviewsContentComponent.css';
import * as PropTypes from 'prop-types';
import {Button} from "@material-ui/core";
import frenchStrings from 'react-timeago/lib/language-strings/fr';
import buildFormatter from 'react-timeago/lib/formatters/buildFormatter';
import TimeAgo from 'react-timeago';
import {StarsWidget} from "../../../../components";

const formatter = buildFormatter(frenchStrings);

class ReviewsContentComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            review: "",
            reviewWords: []
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.cutIntroToShow(this.props);
        }, 100);
        // this.cutIntroToShow();
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if (nextProps !== this.props)
            this.cutIntroToShow(nextProps)
    }

    cutIntroToShow(props) {
        this.setState({
            review: props.review.comment,
            reviewWords: props.review.comment.split(" "),
            showMore: false
        });
        // console.log(this.state.reviewWords.length);

        if (this.state.reviewWords.length > 30) {
            let lastWord = this.state.reviewWords[30];
            let indexOfLastWord = this.state.review.indexOf(lastWord);

            this.setState({
                review: this.state.review.slice(0, indexOfLastWord) + "...",
                showMore: true
            });
        }
    }

    showMore() {
        if (this.state.showMore)
            this.setState({
                review: this.props.review.comment,
                showMore: !this.state.showMore
            });
        else this.cutIntroToShow();
    }


    render() {
        const {review} = this.props;
        console.log(review.creation_date);

        return (
            <div className="mainReviewsContent">
                <div className="rowAlignNameRate">
                    <span className="userNameReview">{review.userId.name + " " + review.userId.lastName}</span>
                    <StarsWidget disabled={true} totalStars={5} rate={review.rate}/>
                </div>
                <span className="contentReview">
                    {this.state.review}
                    {this.state.reviewWords.length > 30 ? <Button onClick={this.showMore.bind(this)}
                                                                  color="secondary">{this.state.showMore ? 'Afficher plus' : 'Afficher moins'}</Button> :
                        <em/>}
                </span>
                <span className="dateReview"><TimeAgo date={new Date(review.creation_date)} formatter={formatter}/></span>
            </div>
        );
    }
}

ReviewsContentComponent.propTypes = {
    review: PropTypes.object
};

export default ReviewsContentComponent;
