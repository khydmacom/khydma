import {ProfileHeaderComponent} from './profileHeader';
import {ProfileMetricsAndLinksComponent} from './profileMetricsAndLinks';
import {MetricsElementComponent} from './metricsElement';
import {ProfileIntroductionComponent} from './profileIntroduction';
import {GalleryProfileComponent} from './galleryProfileComponent';
import {ReviewsProfileComponent} from './reviewsProfileComponent';
import {ReviewsCounterComponent} from './reviewsCounter';
import {ProgressBarsComponent} from './progressBars';
import {ReviewsListProfileComponent} from './reviewsListProfile';
import {ReviewsContentComponent} from './reviewsContent';
import {DetailsCardComponent} from './detailsCard';
import {WorkingHoursComponent} from './workingHours';

export {
    ProfileHeaderComponent,
    MetricsElementComponent,
    ProfileMetricsAndLinksComponent,
    ProfileIntroductionComponent,
    GalleryProfileComponent,
    ReviewsProfileComponent,
    ReviewsCounterComponent,
    ProgressBarsComponent,
    ReviewsListProfileComponent,
    ReviewsContentComponent,
    DetailsCardComponent,
    WorkingHoursComponent
}
