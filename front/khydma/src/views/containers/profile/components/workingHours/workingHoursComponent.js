import React, {Component} from 'react';
import './workingHoursComponent.css';
import {Button} from "@material-ui/core";
// import Message from "@material-ui/core/SvgIcon/SvgIcon";
import Call from '@material-ui/icons/Call';
import Message from '@material-ui/icons/Message';
import Star from '@material-ui/icons/Star';
import {faFacebook, faInstagram, faYoutube} from "@fortawesome/free-brands-svg-icons"
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {ProfileMetricsAndLinksComponent} from "../profileMetricsAndLinks";


class WorkingHoursComponent extends Component {
    render() {
        const {pro} = this.props;
        return (
            <div className="workingHoursMainComponent">
                {pro.workingDaysStarting ? <div>
                    <span className="label">Jours d'ouverture</span>
                    <span
                        className="workingHoursCounter">{`${pro.workingDaysStarting} - ${pro.workingDaysEnding}`}</span>
                </div> : <div/>}
                {
                    pro.workingHoursStarting ? <div>
                        <span className="label">Heures d'ouverture</span>
                        <span
                            className="workingHoursCounter">{`${pro.workingHoursStarting} - ${pro.workingHoursEnding}`}</span>
                    </div> : <div/>
                }

            </div>
        );
    }
}

export default WorkingHoursComponent;
