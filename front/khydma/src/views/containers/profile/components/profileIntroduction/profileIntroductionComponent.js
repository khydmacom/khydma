import React, {Component} from 'react';
import './profileIntroductionComponent.css';
import {Button} from "@material-ui/core";
import * as PropTypes from 'prop-types';
import Language from '@material-ui/icons/Language';


class ProfileIntroductionComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            introduction: "",
            introductionWords: [],
            links: []
        };
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        if (nextProps.introduction !== this.props.introduction) {
            // console.log(nextProps.introduction);
            this.cutIntroToShow(nextProps);
            this.cutLinksToArray(nextProps);
        }
    }

    cutIntroToShow(props) {
        this.setState({
            introduction: props.introduction,
            introductionWords: props.introduction.split(" "),
            showMore: false
        });
        // console.log(this.state.introductionWords.length);

        if (this.state.introductionWords.length > 50) {
            let lastWord = this.state.introductionWords[50];
            let indexOfLastWord = this.state.introduction.indexOf(lastWord);

            this.setState({
                introduction: this.state.introduction.slice(0, indexOfLastWord) + "...",
                showMore: true
            });
        }
    }

    showMore() {
        this.setState({
            showMore: !this.state.showMore
        });
    }

    render() {
        const {introduction, links} = this.props;

        return (
            <div className="mainProfileIntro">
                <span className="labelIntro">Introduction</span>
                {introduction.length > 240 ?
                    [<p key={5} id="intro" className="introContent">
                        {
                            !this.state.showMore ? introduction.slice(0, 54) + '...' : introduction
                        }
                    </p>, <Button key={6} onClick={this.showMore.bind(this)}
                                  color="secondary">{this.state.showMore ? 'Afficher plus' : 'Afficher moins'}</Button>] :
                    <p id="intro" className="introContent">
                        {
                            introduction
                        }
                    </p>
                }

                <br/>
                {
                    links ? links.split(",").map((link, idx) => {
                        return <div key={idx} className="linkAndIcon">
                            <Language style={{opacity: (idx === 0 ? 1 : 0)}}/>
                            <a className="linkProfile" href={link.replace('www.', 'http://')} target="_blank">{link}</a>
                        </div>
                    }) : <div/>
                }
            </div>
        );
    }
}

ProfileIntroductionComponent.propTypes = {
    introduction: PropTypes.string,
    links: PropTypes.string
};

export default ProfileIntroductionComponent;
