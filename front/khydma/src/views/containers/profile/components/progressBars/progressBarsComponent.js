import React, {Component} from 'react';
import './progressBarsComponent.css';
import Star from '@material-ui/icons/Star';
import {Misc} from '../../../../../config';

import * as PropTypes from 'prop-types';

class ProgressBarsComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            totalReviewers: 0,
            reviewsLevel: []
        }
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps !== this.props)
            this.setReviewsProgress();
    }

    setReviewsProgress() {
        let reviewsLevel = [];
        let stateObj = {};
        stateObj['totalReviewers'] = this.props.totalReviewers;
        Object.keys(this.props.reviewsLevel).map((revKey, index) => {
            if (revKey !== "total") {
                let rev = this.props.reviewsLevel[revKey];
                reviewsLevel.push({rate: Misc.rateParser(revKey), level: this.percentage(rev)});
                stateObj['reviewsLevel'] = reviewsLevel;
            }
        });

        this.setState(stateObj);
    }

    percentage(amount) {
        return ((amount / this.props.totalReviewers) * 100).toFixed(0)
    }

    render() {

        return (
            <div className="mainProfileProgressBars">
                {
                    this.state.reviewsLevel.map((val, index) => {
                        return <div className="progressBar" key={index}>
                            <span className="levelRate row">
                                <Star className="levelRate _"/>
                                {val.rate}
                            </span>
                            <div className="bar_progressBars">
                                <div className="progress_progressBars" style={{width: val.level + '%'}}/>
                            </div>
                            <span className="levelVal">{val.level + "%"}</span>
                        </div>
                    })
                }
            </div>
        );
    }
}

ProgressBarsComponent.propTypes = {
    reviewsLevel: PropTypes.object,
    totalReviewers: PropTypes.number
};

export default ProgressBarsComponent;
