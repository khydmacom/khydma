import React, {Component} from 'react';
import './reviewsProfileComponent.css';
import * as PropTypes from 'prop-types';
import {ReviewsCounterComponent} from "../reviewsCounter";

class ReviewsProfileComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return this.props.stats.total ? (
            <div className="mainProfileReview">
                <span className="labelIntro">Avis</span>
                <div className="reviewsCounter_callside">
                    <ReviewsCounterComponent reviewsLevel={this.props.stats}/>
                </div>
            </div>
        ) : <div/>;
    }
}

ReviewsProfileComponent.propTypes = {
    stats: PropTypes.object
};

export default ReviewsProfileComponent;
