import React, {Component} from 'react';
import './profileMetricsAndLinksComponent.css';
import {MetricsElementComponent} from "../metricsElement";
import {WorkingHoursComponent} from "../workingHours";
import * as PropTypes from 'prop-types';
import {Misc} from '../../../../../config';
import ProsApi from "../../../../reducers/pros/prosApi";

class ProfileMetricsAndLinksComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            metricsArray: []
        }
    }

    componentWillUpdate(nextProps: Readonly<P>, nextState: Readonly<S>, nextContext: any): void {
        // console.log(nextProps.stats)
        if (nextProps.stats !== this.props.stats) {
            this.calculateAvgReviewers(nextProps.stats)
        }
    }

    calculateAvgReviewers = (stats) => {
        let totalReviewers = stats.total;
        let allReviewsNumber = 0;
        let stateObj = {};
        let metricsArray = [];

        for (let rev of Object.keys(stats)) {
            if (rev !== 'total')
                allReviewsNumber += (Misc.rateParser(rev) * stats[rev]);

            stateObj['totalReviewers'] = totalReviewers;
            stateObj['allReviewsNumber'] = allReviewsNumber;
            stateObj['totalAvrg'] = (allReviewsNumber / totalReviewers).toFixed(0);
        }
        if (stateObj.totalReviewers)
            metricsArray.push({
                label: 'Avis',
                value: stateObj.totalAvrg
            });
        if (this.props.pro.businessFee)
            metricsArray.push({
                label: 'Tarif',
                value: this.props.pro.businessFee,
                unit: Misc.getBusinessFeeRate(this.props.pro.businessFeeRate)
            });
        this.setState({metricsArray});
    };

    render() {
        const {pro} = this.props;
        return (
            <div className="mainProfileMetricsAndLinks">
                {
                    this.state.metricsArray.map((val, index) => {
                        return <MetricsElementComponent key={index} label={val.label} value={val.value}
                                                        unit={val.unit}/>
                    })
                }
                {
                    pro.workingDaysStarting || pro.workingHoursStarting ? <WorkingHoursComponent pro={pro}/> : <div/>
                }

            </div>);

    }
}

ProfileMetricsAndLinksComponent.propTypes = {
    stats: PropTypes.object,
    pro: PropTypes.object
};

export default ProfileMetricsAndLinksComponent;
