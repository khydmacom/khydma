import React, {Component} from 'react';
import './popGridComponent.css';
import {PopSliderCardComponent} from "../../../home/components";
import * as PropTypes from 'prop-types';
import {Link} from "@reach/router";

class PopGridComponent extends Component {
    render() {
        const {searchResults} = this.props;

        return (
            <div className="mainSlidersDiv mainViewContainer">
                <h1 className="popTitle title">{searchResults && searchResults.label ? 'Services Populaires Dans La Catégorie ' + searchResults.label : 'La catégorie recherchée n\'est pas dans notre liste pour le moment'}</h1>
                <div className="cardsGid">
                    {
                        searchResults && searchResults.categories ?
                            searchResults.categories.map((cat, idx) => {
                                return <div className="cardDiv" key={idx}>
                                    <Link to={`/${cat.gotKids ? 'categories' : 'search'}?q=${cat.label}`}>
                                        <PopSliderCardComponent category={cat}/>
                                    </Link>
                                </div>
                            }) : <div/>
                    }
                </div>
            </div>
        );
    }
}

PopGridComponent.propTypes = {
    searchResults: PropTypes.object
};

export default PopGridComponent;
