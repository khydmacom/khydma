import React, {Component} from 'react';
import './categoriesMainHead.css';
import * as PropTypes from 'prop-types';
import {SearchBarComponent} from "../../../../components";
import {Misc} from "../../../../../config";

const {ServicesRenderer} = Misc;

class CategoriesMainHeadComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            results: [],
            open: false,
            anchorEl: null,
            value: '',
            suggestions: [],
            selectedCat: null
        }
    }

    onChange = (event, {newValue}) => {
        this.setState({
            value: newValue
        });
    };

    setAnchorEl = newValue => {
        this.setState({
            anchorEl: newValue
        });
    };

    // method to get suggestions
    getSuggestions = (event) => {
        let value = event.target ? event.target.value : event;
        let results = value ? [...this.props.categories].filter((cat) => {
            let search = value;
            let exp = new RegExp(search.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
            return exp.test(cat.label.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
        }) : [];

        return results
    };

    handleMenuClick = (cat) => {
        this.setState({selectedCat: cat})
    };

    onSuggestionsFetchRequested = ({value}) => {
        this.setState({
            suggestions: this.getSuggestions(value)
        });
    };

    onSuggestionsClearRequested = () => {
        this.setState({
            suggestions: []
        });
    };

    render() {
        const {value, suggestions, anchorEl, results, open, selectedCat} = this.state;
        const {category, categories} = this.props;
        // console.log(category);

        return (
            <div className="mainHeadDiv">
                {/*<div className="logoHead"/>*/}
                <h1 className="title maiHeadCategory">{category && category.label ? category.label : 'Découvrez nos catégories populaires'}</h1>
                <SearchBarComponent suggestions={suggestions} results={results} open={open} value={value}
                                    onChange={this.onChange} setAnchorEl={this.setAnchorEl} anchorEl={anchorEl}
                                    onSuggestionsFetchRequested={this.onSuggestionsFetchRequested}
                                    onSuggestionsClearRequested={this.onSuggestionsClearRequested}
                                    handleMenuClick={this.handleMenuClick}
                                    categories={categories} categoriesPage={true}/>
                <div className="spacing"/>
                <div className="actionIconsWrapper">
                    <div className="actionIcons">
                        <ServicesRenderer/>
                    </div>
                </div>
            </div>
        );
    }
}

CategoriesMainHeadComponent.propTypes = {
    category: PropTypes.object,
    categories: PropTypes.array
}

export default CategoriesMainHeadComponent;
