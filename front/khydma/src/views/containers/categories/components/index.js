import {CategoriesMainHeadComponent} from './categoriesMainHead';
import {PopGridComponent} from './popGrid';

export {
    CategoriesMainHeadComponent,
    PopGridComponent
}
