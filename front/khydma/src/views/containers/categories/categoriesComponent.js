import React, {Component} from 'react';
import './categoriesComponent.css';
import {CategoriesMainHeadComponent, PopGridComponent} from "./components";
import {connect} from "react-redux";
import {categoriesActions} from "../../reducers/categories";
import * as queryString from "query-string";
import {Misc} from "../../../config";
import {Helmet} from 'react-helmet';
import {RootsActions} from "../../reducers/roots";

const {getCategories} = categoriesActions;
const {updateRoot} = RootsActions;
const {reorderCategories} = Misc;


class CategoriesComponent extends Component {

    constructor(props) {
        super(props);

        this.state = {
            value: '',
            selectedCategory: {},
            reorderedCategories: []
        }
    }

    componentDidMount(): void {
        this.props.updateRoot(this.props);
        this.props.getCategories(this.isGetCatsSuccess, this.isGetCatsError)
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        if (prevProps.location !== this.props.location) {
            this.passQueryToFilter(this.props, this.state.reorderedCategories);
        }
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (nextProps.allCategories !== this.props.allCategories) {
            let cats = reorderCategories(nextProps.allCategories).newArray;
            this.passQueryToFilter(nextProps, cats);

            this.setState({
                reorderedCategories: cats
            });
        }
    }

    isGetCatsSuccess = (succ) => {
        // console.log(succ);
    };

    isGetCatsError = (err) => {
        console.log(err)
    };

    filterCategories(props, parsed, reorderedCategories) {
        let results = parsed.q ? [...reorderedCategories].filter((cat) => {
            let search = parsed.q;
            let exp = new RegExp(search.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
            return exp.test(cat.label.toLowerCase().normalize('NFD').replace(/[\u0300-\u036f]/g, ""));
        }) : [];

        this.setState({
            selectedCategory: results[0]
        })
    }

    passQueryToFilter = (props, reorderedCategories) => {
        let parsed = queryString.parse(props.location.search);
        if (parsed.q) {
            this.setState({
                value: parsed.q
            })
        }
        this.filterCategories(props, parsed, reorderedCategories);
    };


    render() {
        // const {allCategories} = this.props;
        const {selectedCategory, reorderedCategories} = this.state;
        return reorderedCategories && reorderedCategories.length > 0 ? (
            <div className="homeComponent">
                <Helmet>
                    <title>{`${selectedCategory && selectedCategory.label ? selectedCategory.label : 'Découvrez nos catégories populaires'} | Khydma.com`}</title>
                    <meta property="og:title" content={`${selectedCategory && selectedCategory.label ? selectedCategory.label : 'Découvrez nos catégories populaires'} | Khydma.com`}/>
                    <meta property="og:type" content="website"/>
                    <meta property="og:site_name" content="Khydma.com"/>
                    <meta property="og:description" content={`Contacter des ${selectedCategory && selectedCategory.label ? selectedCategory.categories.map((text, idx) => {return `${text.label}, `;}) : 'Découvrez nos catégories populaires'} et plus! | khydma.com`}/>
                    <meta name="description" content={`Contacter des ${selectedCategory && selectedCategory.label ? selectedCategory.categories.map((text, idx) => {return `${text.label}, `;}) : 'Découvrez nos catégories populaires'} et plus! | khydma.com`} />
                </Helmet>
                <CategoriesMainHeadComponent categories={reorderedCategories} category={selectedCategory}/>
                <PopGridComponent searchResults={selectedCategory}/>
            </div>
        ) : <div/>;
    }
}

let statesToProps = (state, props) => {
    if (state.currentUsersReducer) {
        return {
            allCategories: state.categoriesReducer.allCategories
        }
    } else
        return {}
};

export default connect(statesToProps, {getCategories, updateRoot})(CategoriesComponent);
