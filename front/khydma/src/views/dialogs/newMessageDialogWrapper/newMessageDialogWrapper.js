import React, {Component} from 'react';
import './newMessageDialogWrapper.css';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Misc, CONST} from "../../../config";
import pubSubJs from "pubsub-js";
import {MuiThemeProvider} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {SocketServices} from '../../../config/services';

const _SocketServices = SocketServices;
const {ioTriggers} = CONST;

export default class NewMessageDialogWrapper extends Component {
    /*theme = useTheme();
    fullScreen = useMediaQuery(this.theme.breakpoints.down('sm'));*/
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            errorMessage: '',
            text: ''
        }
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };

    handleTextChange = (event) => {
        event.persist()
        this.setState({
            [event.target.name]: event.target.value
        });
        setTimeout(() => {
            if (event && event.target && event.target.value.length < 10)
                this.setState({
                    errorMessage: 'Votre message doit comporter au moins 10 caractères'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 333)
    };

    handleTextBlur = (event) => {
        setTimeout(() => {
            if (this.state.text < 10)
                this.setState({
                    errorMessage: 'Votre message doit comporter au moins 10 caractères'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 444)
    }

    setTextValueFiller = (name) => {
      this.setState({
          text: `Bonjour ${name},\n`
      })
    };


    componentDidMount(): void {
        // we call before we subscribe to fire it then we sub, so it fires each time we resize
        this.setViewType();

        window.addEventListener("resize", this.setViewType);
        if (this.props.pro) {
            this.setTextValueFiller(this.props.pro.businessName);
        }
    }

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
    }

    sendMessage = () => {

        let socketMessageBody = {
            senderId: this.props.user._id,
            text: this.state.text,
            userId: this.props.user._id,
            proId: this.props.pro._id,
            receiverId: this.props.pro.userId._id
        };

        _SocketServices.prototype.socket.emit(ioTriggers.newMessage, socketMessageBody);
        this.props.setClose();
        console.log('sendMessage = ', socketMessageBody);
    };

    render() {
        const {open, setOpen, setClose, pro, ...other} = this.props;
        const {errorMessage, text} = this.state;
        return (
            <div>
                <Dialog
                    fullScreen={this.state.fullScreen}
                    open={open}
                    onClose={setClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{`Envoyer un message à ${pro.businessName}`}</DialogTitle>
                    <DialogContent>
                        <MuiThemeProvider theme={Misc.FORM_THEME}>
                            <DialogContentText>
                                Ecrivez votre message ici
                            </DialogContentText>
                            <TextField
                                className="textUpdate"
                                autoComplete="off"
                                onChange={this.handleTextChange}
                                onBlur={this.handleTextBlur}
                                error={!!(errorMessage)}
                                helperText={errorMessage}
                                value={text}
                                label="Message"
                                type="text"
                                name="text"
                                multiline={true}
                                rowsMax="6"
                                margin="normal"
                                variant="outlined"
                            />
                        </MuiThemeProvider>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={setClose} color="secondary">
                            Annuler
                        </Button>
                        <Button onClick={this.sendMessage} color="secondary" autoFocus disabled={Boolean(errorMessage) || text.length < 10}>
                            Envoyer
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

NewMessageDialogWrapper.propTypes = {
    className: PropTypes.string,
    open: PropTypes.bool,
    setOpen: PropTypes.func,
    setClose: PropTypes.func,
    pro: PropTypes.object,
    user: PropTypes.object
};
