import {NewMessageDialogWrapper} from './newMessageDialogWrapper';
import {RequestLoginDialogWrapper} from './requestLoginDialogWrapper';
import {FullImageGalleryDialog} from './fullImageGalleryDialog';
import {ShowPhoneDialogWrapper} from './showPhoneDialogWrapper';
import {NewReviewDialogWrapper} from './newReviewDialogWrapper';
import {ConfirmDelete} from './confirmDelete';
import {ConfirmReviewDelete} from './confirmReviewDelete';
import {SingleConversationDialogWrapper} from './singleConversationDialogWrapper';

export {
    NewMessageDialogWrapper,
    RequestLoginDialogWrapper,
    FullImageGalleryDialog,
    ShowPhoneDialogWrapper,
    NewReviewDialogWrapper,
    ConfirmDelete,
    ConfirmReviewDelete,
    SingleConversationDialogWrapper
}
