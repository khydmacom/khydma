import React, {Component, useState} from 'react';
import './newReviewDialogWrapper.css';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Misc, CONST} from "../../../config";
import StarIcon from '@material-ui/icons/Star';
import StarBorderIcon from '@material-ui/icons/StarBorder';
import {MuiThemeProvider} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {StarsWidget} from "../../components";


export default class NewReviewDialogWrapper extends Component {
    /*theme = useTheme();
    fullScreen = useMediaQuery(this.theme.breakpoints.down('sm'));*/
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false,
            errorMessage: '',
            text: '',
            rating: 5,
            rate: 1
        }
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };

    handleTextChange = (event) => {
        event.persist();
        this.setState({
            [event.target.name]: event.target.value
        });
        setTimeout(() => {
            if (event && event.target && event.target.value.length < 10)
                this.setState({
                    errorMessage: 'Votre commentaire doit comporter au moins 10 caractères'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 333)
    };

    handleTextBlur = (event) => {
        setTimeout(() => {
            if (this.state.text < 10)
                this.setState({
                    errorMessage: 'Votre commentaire doit comporter au moins 10 caractères'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 444)
    }

    setTextValueFiller = (name) => {
        this.setState({
            text: `${name},\n`
        })
    };


    componentDidMount(): void {
        // we call before we subscribe to fire it then we sub, so it fires each time we resize
        this.setViewType();

        window.addEventListener("resize", this.setViewType);
        if (this.props.pro) {
            this.setTextValueFiller(this.props.pro.businessName);
        }
        if (this.props.myReview) {
            this.setState({
                text: this.props.myReview.comment,
                rate: this.props.myReview.rate
            })
        }
    }

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
    }

    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (nextProps.myReview !== this.props.myReview) {
           if (nextProps.myReview)
            this.setState({
                text: nextProps.myReview.comment,
                rate: nextProps.myReview.rate
            });
           else {
               this.setState({
                   text: `${this.props.pro.businessName}, \n`,
                   rate: 1
               });
           }
        }
    }


    sendMessage = () => {

        let socketMessageBody = {
            comment: this.state.text,
            userId: this.props.user._id,
            proId: this.props.pro._id,
            rate: this.state.rate
        };
        console.log('sendMessage = ', socketMessageBody);

        this.props.setConfirm(socketMessageBody);
    };

    handleOnEnter = () => {
        /*ProsApi.getReviewByQuery({
            userId: this.props.user._id,
            proId: this.props.pro._id
        }, (succ, rev, err) => {
            console.log(rev)
            if (succ)
                this.setState({
                    text: rev.comment,
                    rate: rev.rate
                })
        })*/
    };


    render() {
        const {open, setOpen, setClose, pro, ...other} = this.props;
        const {errorMessage, text, rating, rate} = this.state;
        return (
            <div>
                <Dialog
                    onEnter={this.handleOnEnter}
                    fullScreen={this.state.fullScreen}
                    open={open}
                    onClose={setClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{`Laisser un avis pour ${pro.businessName}`}</DialogTitle>
                    <DialogContent>
                        <MuiThemeProvider theme={Misc.FORM_THEME}>
                            <DialogContentText>
                                Donner une note
                            </DialogContentText>
                            <StarsWidget totalStars={rating} rate={rate} onStarsUpdate={(r) => {
                                this.setState({rate: r})
                            }}/>
                            <DialogContentText>
                                Ecrivez votre commentaire ici
                            </DialogContentText>
                            <TextField
                                className="textUpdate"
                                autoComplete="off"
                                onChange={this.handleTextChange}
                                onBlur={this.handleTextBlur}
                                error={!!(errorMessage)}
                                helperText={errorMessage}
                                value={text}
                                label="Commentaire"
                                type="text"
                                name="text"
                                multiline={true}
                                rowsMax="6"
                                margin="normal"
                                variant="outlined"
                            />
                        </MuiThemeProvider>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={setClose} color="secondary">
                            Annuler
                        </Button>
                        <Button onClick={this.sendMessage} color="secondary" autoFocus
                                disabled={Boolean(errorMessage) || text.length < 10}>
                            Envoyer
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

NewReviewDialogWrapper.propTypes = {
    className: PropTypes.string,
    open: PropTypes.bool,
    setOpen: PropTypes.func,
    setClose: PropTypes.func,
    setConfirm: PropTypes.func,
    pro: PropTypes.object,
    myReview: PropTypes.object,
    user: PropTypes.object
};
