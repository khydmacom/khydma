import React, {Component} from 'react';
import './singleConversationDialogWrapper.css';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import CloseIcon from '@material-ui/icons/Close';
import {Misc} from "../../../config";
import {AppBar, IconButton, MuiThemeProvider, Toolbar, Typography, withStyles} from "@material-ui/core";
import {MessageComponent} from "../../components/messageComponent";
import {Link} from "@reach/router";
import TextField from "@material-ui/core/TextField";
import Send from "@material-ui/icons/Send";
import * as ReactDOM from "react-dom";
import {MessagingApi} from "../../reducers/messaging";

const styles = theme => ({
    appBar: {
        position: 'relative',
    },
    title: {
        marginLeft: 15,
        flex: 1,
        color: "#2ba84a"
    },
});

class SingleConversationDialogWrapper extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errorMessage: '',
            text: '',
            messages: []
        }
    }

    handleTextChange = (event) => {
        event.persist();
        this.setState({
            [event.target.name]: event.target.value
        });
        setTimeout(() => {
            if (event && event.target && event.target.value.length < 1)
                this.setState({
                    errorMessage: 'Vous ne pouvez pas envoyer un message vide!'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 333)
    };

    handleTextBlur = (event) => {
        setTimeout(() => {
            if (this.state.text < 1)
                this.setState({
                    errorMessage: 'Vous ne pouvez pas envoyer un message vide!'
                });
            else {
                this.setState({
                    errorMessage: ''
                });
            }
        }, 444)
    };

    onMessagesScroll = (event) => {
        const scrollTop = this.messagesHistory.scrollTop;
        if (scrollTop === 0) {
            console.log('on top!!!!')
        }
    };

    scrollToBottom = () => {
        const scrollHeight = this.messagesHistory.scrollHeight;
        const height = this.messagesHistory.clientHeight;
        const maxScrollTop = scrollHeight - height;
        ReactDOM.findDOMNode(this.messagesHistory).scrollTop = maxScrollTop > 0 ? maxScrollTop : 0;
    };

    getMessagesList = (props) => {
        MessagingApi.getConvs((props.profile.businessName ?
                {proId: props.profile._id, userId: props.user._id}
                : {proId: props.user.proId, userId: props.profile._id}
        ), (_succ, _res, err) => {
            if (_succ) {
                MessagingApi.getMsgs({conversationId: _res[0]._id}, (succ, res, err) => {
                    if (succ) {
                        this.setState({
                            messages: [],
                        });
                        this.setState({
                            messages: res,
                            conversationId: _res[0]._id
                        });
                    } else {
                        console.log(err)
                    }
                });
                console.log(_res);
            }
        })
    };



    componentWillReceiveProps(nextProps: Readonly<P>, nextContext: any): void {
        if (Object.keys(nextProps.profile).length > 0)
            this.getMessagesList(nextProps);
    }

    componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void {
        /*if (prevProps.profile !== this.props.profile) {
            this.getMessagesList(this.props);
        }*/

        if (this.messagesHistory)
            this.scrollToBottom();
    }

    render() {
        const {open, setOpen, setClose, profile, navigate, className, lastNewMessage, sendMessage, user, classes} = this.props;
        const {errorMessage, text, messages} = this.state;

        return (
            <div>
                <Dialog
                    fullScreen={true}
                    open={open}
                    onClose={setClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <AppBar className={classes.appBar}>
                        <Toolbar>
                            <IconButton edge="start" color="inherit" onClick={setClose} aria-label="Close">
                                <CloseIcon/>
                            </IconButton>
                            {profile._id ? <Typography variant="h6" className={classes.title}>
                                {profile.businessName ? profile.businessName :
                                    `${profile.name} ${profile.lastName}`
                                }
                            </Typography> : <div/>}
                        </Toolbar>
                    </AppBar>
                    <div className="sectionTitleSeparator"/>
                    {
                        messages && messages.length > 1 ?
                            <div className="messagesListContainer" onScroll={this.onMessagesScroll} ref={(ref) => (
                                this.messagesHistory = ref)
                            }>
                                {messages.map((msg, index) => {
                                    return <div key={index} className="singleMessageContainer">
                                        <MessageComponent message={msg} currentUser={{_id: user._id}}
                                                          previousUserId={messages[index - 1] ? (messages[index - 1].senderIdPro ? messages[index - 1].senderIdPro : messages[index - 1].senderId) : null}/>
                                    </div>
                                })}
                            </div> : <div className="CTANoMessages">
                                <span className="noConvosText">Commencer une nouvelle conversation en recherchant des professionnels</span>
                                <Link to={'/'}>
                                    <Button variant={"contained"} color={"secondary"}>
                                        Chercher des professionnels
                                    </Button>
                                </Link>
                            </div>
                    }

                    <div className="inputDivMessageMain">
                        <MuiThemeProvider theme={Misc.FORM_THEME}>
                            <TextField
                                className="textUpdate"
                                autoComplete="off"
                                onChange={this.handleTextChange}
                                onBlur={this.handleTextBlur}
                                error={!!(errorMessage)}
                                helperText={errorMessage}
                                value={text}
                                label="Message"
                                type="text"
                                name="text"
                                multiline={true}
                                rowsMax="6"
                                margin="normal"
                                variant="outlined"
                            />
                        </MuiThemeProvider>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            aria-haspopup="true"
                            className="sendButtonMain"
                            color="secondary"
                            onClick={() => {
                                sendMessage(text);
                                setTimeout(() => {
                                    this.setState({
                                        text: ''
                                    })
                                }, 455);
                            }}
                            disabled={!profile || Boolean(errorMessage) || text.length < 1}
                        >
                            <Send/>
                        </IconButton>
                    </div>
                </Dialog>
            </div>
        );
    }
}

SingleConversationDialogWrapper.propTypes = {
    className: PropTypes.string,
    open: PropTypes.bool,
    setOpen: PropTypes.func,
    setClose: PropTypes.func,
    navigate: PropTypes.func,
    sendMessage: PropTypes.func,
    profile: PropTypes.object,
    user: PropTypes.object,
    lastNewMessage: PropTypes.object,
    classes: PropTypes.any
};

export default withStyles(styles)(SingleConversationDialogWrapper);
