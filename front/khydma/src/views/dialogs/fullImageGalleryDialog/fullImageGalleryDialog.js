import React, {Component} from 'react';
import './fullImageGalleryDialog.css';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import imageFiller from '../../../assets/images/no.png'
import {Misc} from "../../../config";
import {IconButton} from "@material-ui/core";
import Colse from '@material-ui/icons/Close';


export default class FullImageGalleryDialog extends Component {
    /*theme = useTheme();
    fullScreen = useMediaQuery(this.theme.breakpoints.down('sm'));*/
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false
        }
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };


    componentDidMount(): void {
        // we call before we subscribe to fire it then we sub, so it fires each time we resize
        this.setViewType();

        window.addEventListener("resize", this.setViewType);
    }

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
    }

    render() {
        const {open, setOpen, setClose, pro, navigate, image, ...other} = this.props;
        return (
            <div>
                <Dialog
                    fullScreen={this.state.fullScreen}
                    open={open}
                    onClose={setClose}
                    aria-labelledby="responsive-dialog-title"
                    className="mainDialogDiv"
                >
                    {
                        image ? <div className="fullImageDialogWrapper">
                            <img src={image.imagePath} className="imageGalleryFull" alt={image.description}/>
                            <div className="dialogFullImageContent">
                                <span className="contentHeaderFullImage">
                                    <IconButton onClick={setClose}>
                                        <Colse/>
                                    </IconButton>
                                </span>
                                <p className="fullImageDescription imageDescripTitle">Description</p>
                                <p className="fullImageDescription">{image.description}</p>
                            </div>
                        </div> : <div/>
                    }

                </Dialog>
            </div>
        );
    }
}

FullImageGalleryDialog.propTypes = {
    className: PropTypes.string,
    open: PropTypes.bool,
    setOpen: PropTypes.func,
    setClose: PropTypes.func,
    navigate: PropTypes.func,
    image: PropTypes.object
};
