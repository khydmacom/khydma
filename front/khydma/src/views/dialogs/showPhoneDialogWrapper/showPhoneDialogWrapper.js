import React, {Component} from 'react';
import './showPhoneDialogWrapper.css';
import * as PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import {Misc} from "../../../config";


export default class ShowPhoneDialogWrapper extends Component {
    /*theme = useTheme();
    fullScreen = useMediaQuery(this.theme.breakpoints.down('sm'));*/
    constructor(props) {
        super(props);

        this.state = {
            fullScreen: false
        }
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };


    componentDidMount(): void {
        // we call before we subscribe to fire it then we sub, so it fires each time we resize
        this.setViewType();

        window.addEventListener("resize", this.setViewType);
    }

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
    }

    render() {
        const {open, setOpen, setClose, pro, navigate, ...other} = this.props;
        return (
            <div>
                <Dialog
                    fullScreen={this.state.fullScreen}
                    open={open}
                    onClose={setClose}
                    aria-labelledby="responsive-dialog-title"
                >
                    <DialogTitle id="responsive-dialog-title">{`Numéro de téléphone de ${pro.businessName}`}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>{`${pro.businessPhone}`}</DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={setClose} color="secondary">
                            Fermer
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

ShowPhoneDialogWrapper.propTypes = {
    className: PropTypes.string,
    open: PropTypes.bool,
    setOpen: PropTypes.func,
    setClose: PropTypes.func,
    navigate: PropTypes.func,
    pro: PropTypes.object,
    user: PropTypes.object
};
