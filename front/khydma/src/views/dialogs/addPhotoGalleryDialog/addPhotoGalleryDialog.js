import React, {Component} from 'react';
import ReactDOM from 'react-dom';
import './addPhotoGalleryDialog.css';
import * as PropTypes from 'prop-types';
import {
    Button,
    Dialog,
    DialogContent,
    DialogTitle,
    FormControl,
    InputLabel, MenuItem,
    MuiThemeProvider,
    OutlinedInput,
    Select, withStyles
} from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import {Misc} from "../../../config";
import CircularProgress from '@material-ui/core/CircularProgress';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    menu: {
        width: 200,
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
});

class AddPhotoGalleryDialog extends Component {
    constructor(props) {
        super(props);

        this.state = {
            labelWidth: 0,
            parentId: '',
            description: '',
            progress: 0,
            imagePath: null,
            fullScreen: false
        }
    }

    setViewType = () => {
        switch (Misc.getSizeType()) {
            case 'xs':
                this.setState({fullScreen: true});
                return;
            case 'sm':
                this.setState({fullScreen: true});

                return;
            case 'md':
                this.setState({fullScreen: false});

                return;
            case 'lg':
                this.setState({fullScreen: false});

                return;
            case 'xl':
                this.setState({fullScreen: false});

                return;
            default:
                return;
        }
    };


    componentDidMount(): void {
        // we call before we subscribe to fire it then we sub, so it fires each time we resize
        this.setViewType();
        window.addEventListener("resize", this.setViewType);
    }

    componentWillUnmount(): void {
        window.removeEventListener("resize", this.setViewType);
    }


    handleClose = () => {
        this.props.onClose(this.props.selectedValue);
    };

    handleInput = (e) => {
        this.setState({description: e.target.value});
    };

    handleOnOn = value => {

    };

    handleCategoryChange = event => {
        this.setState({[event.target.name]: event.target.value});
    };

    successCb = (res) => {
        if (res && this.props.file) {
            this.props.updateCategoryImage({_id: res._id, file: this.props.file}, (prog) => {
                this.setState({
                    progress: prog.loaded
                })
            }, (suc) => {
                this.emptyState();
                this.props.showSnackBar((this.props.galleryToUpdate ? "Catégorie modifier avec succées!" : "Catégorie ajouter avec succées!"), "success");
                this.handleClose();
            }, this.errCb)
        } else {
            this.emptyState();
            this.props.showSnackBar((this.props.galleryToUpdate ? "Catégorie modifier avec succées!" : "Catégorie ajouter avec succées!"), "success");
            this.handleClose();
        }
    };

    emptyState = () => {
        this.props.onFileSelect(null);
        this.setState({
            parentId: '',
            label: '',
            progress: 0
        });
    }

    errCb = (err) => {
        console.log("err", err);
        this.props.showSnackBar(err.message, "error");
    };

    handleAddition = () => {
        this.props.newCategory({
            gallery: this.props.fileObj,
            description: this.state.description
        });
    };

    render() {
        const {
            showSnackBar, newCategory, updateCategory, reloadGallery,
            classes, onClose, selectedValue, allGallery, galleryToUpdate, file,
            onFileSelect, updateCategoryImage, ...other
        } = this.props;

        return (
            <Dialog onEnter={this.handleOnOn} onClose={this.handleClose}
                    aria-labelledby="simple-dialog-title" {...other}>
                <DialogTitle
                    id="simple-dialog-title">Ajouter une nouvelle photo</DialogTitle>
                <DialogContent>
                    <MuiThemeProvider theme={Misc.FORM_THEME}>
                        <div className="columnAlign">
                            <div
                                style={{backgroundImage: "url(" + (file ? file : require('../../../assets/images/thumb.jpg')) + ")"}}
                                className="categoryImageUpload shadow">
                                {this.state.progress ? <div className="progressCircleLayer">
                                    <CircularProgress variant="static" value={this.state.progress}/>
                                </div> : <div/>}
                            </div>
                            <input
                                id="___SelectFileButton___galleryImage"
                                type="file"
                                accept="image/*"
                                onChange={onFileSelect}
                                hidden
                            />
                            <Button color="primary" className={classes.button} onClick={() => {
                                global.document.getElementById('___SelectFileButton___galleryImage').click();
                            }}>
                                Choisissez une image
                            </Button>
                            <form className={classes.root} autoComplete="off">
                                <FormControl variant="outlined" color="primary" className={classes.formControl}>
                                    <TextField
                                        inputProps={{maxLength: 300}}
                                        id="outlined-email-input"
                                        label="Description"
                                        className={classes.textField + ' ' + 'text'}
                                        type="text"
                                        name="description"
                                        color="primary"
                                        multiline={true}
                                        rowsMax={6}
                                        maxLength={2}
                                        margin="normal"
                                        variant="outlined"
                                        value={this.state.description || ''}
                                        onChange={this.handleInput}
                                    />
                                </FormControl>
                            </form>

                            <Button disabled={this.state.description.length === 0 || !(file)}
                                    color="primary" variant="contained"
                                    onClick={this.handleAddition}
                                    className="confirmation"
                            >
                                <span>Ajouter</span>
                            </Button>
                        </div>
                    </MuiThemeProvider>
                </DialogContent>
            </Dialog>
        );
    }
}

AddPhotoGalleryDialog.propTypes = {
    onClose: PropTypes.func,
    open: PropTypes.bool,
    allGallery: PropTypes.array,
    newCategory: PropTypes.func,
    showSnackBar: PropTypes.func,
    galleryToUpdate: PropTypes.object,
    file: PropTypes.object,
    updateCategory: PropTypes.func,
    updateCategoryImage: PropTypes.func,
    onFileSelect: PropTypes.func,
    reloadGallery: PropTypes.func,
    fileObj: PropTypes.object
};
export default withStyles(styles)(AddPhotoGalleryDialog);
