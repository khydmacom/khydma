const express = require('express');
const router = express.Router();
const keys = require('../config/keys');
const secret = keys.serverSecret;
const expressJwt = require('express-jwt');
const authenticate = expressJwt({secret});

const Category = require('../models/category');
const User = require('../models/user');
const Upload = require('./upload');

const checkAdmin = (req, res, next) => {
    User.checkAdmin(req, (err, user, message) => {
        if (err) {
            // console.log('err', message);
            next(message);
        } else {
            next()
        }
    });
};

let response = (success, category, err, res) => {
    if (success) {
        return res.status(200).json(category);
    } else if (err) {
        let errCode = err.code; // exp usually 11000
        return res.status((errCode === 11000 ? 207 : 204)).json({message: (errCode === 11000 ? "Existe déjà!" : err.errmsg)});
    } else {
        return res.status(207).json({message: "Something went wrong"});
    }
};


router.get('/one/:_id', (req, res) => {
    Category.getOne(req.params, (success, category, err) => {
        response(success, category, err, res);
    });
});

router.get('/', (req, res) => {
    Category.getAll((success, category, err) => {
        response(success, category, err, res);
    });
});

// ones that require auth as admin

router.post('/new/', authenticate, checkAdmin, (req, res) => {
        let category = req.body;

        Category.newCategory(category, (success, category, err) => {
            response(success, category, err, res);
        })
});

router.get('/delete/:_id', authenticate, checkAdmin, (req, res) => {
        Category.removeOne(req.params, (success, category, err) => {
            response(success, category, err, res);
        })
});

router.post('/update/', authenticate, checkAdmin, (req, res) => {
        Category.updateOne(req.body, (success, category, err) => {
            console.log(success, category, err);
            response(success, category, err, res);
        })
});

router.post('/update/image', authenticate, checkAdmin, (req, res) => {
        Upload.postCategoryImage(req, (success, category, err) => {
            response(success, category, err, res);
        });
});

module.exports = router;
