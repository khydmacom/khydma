const express = require('express');
const router = express.Router();
const keys = require('../config/keys');
const secret = keys.serverSecret;
const expressJwt = require('express-jwt');
const authenticate = expressJwt({secret});

const Gallery = require('../models/galleryImage');
const User = require('../models/user');
const Pro = require('../models/pro');
const ProIndex = require('../models/proIndex');
const Upload = require('./upload');

const checkAdmin = (req, res, next) => {
    User.checkAdmin(req, (err, user, message) => {
        if (err) {
            // console.log('err', message);
            next(message);
        } else {
            next()
        }
    });
};

let response = (success, pro, err, res) => {
    if (success) {
        return res.status(200).json(pro);
    } else if (err) {
        let errCode = err.code; // exp usually 11000
        return res.status((errCode === 11000 ? 207 : 204)).json({message: (errCode === 11000 ? "Existe déjà!" : err.errmsg)});
    } else {
        return res.status(207).json({message: "Something went wrong"});
    }
};

// GET /something?queryPram1=red&queryParam2=blue
// that get form to use this route to get a a pro by his userId, Id or categoryId

router.get('/one/', (req, res) => {
    Pro.getOne(req.query, (success, pro, err) => {
        response(success, pro, err, res);
    });
});

// GET /something?queryPram1=red&queryParam2=blue
// that get form to use this route to get PROS by their categoryId or all of them no filter

router.get('/', (req, res) => {
    Pro.getAll(req, (success, pro, err) => {
        response(success, pro, err, res);
    });
});

router.get('/indexes/', (req, res) => {
    ProIndex.getByQueryAll(req, (success, pro, err) => {
        response(success, pro, err, res);
    });
});

router.get('/search/', (req, res) => {
    // console.log(req.query);
    /*Pro.listIndexes((err, res) => {
        console.log(res)
    })*/
    ProIndex.getAll(req, (success, pro, err) => {
        console.log(success, pro, err)
        response(success, pro, err, res);
    });
});

router.get('/category/', (req, res) => {
    Pro.getAllByCategoryName(req, (success, pro, err) => {
        response(success, pro, err, res);
    });
});

router.post('/new/', authenticate, (req, res) => {
        let pro = req.body;
        Pro.newPro(pro, (success, pro, err) => {
            response(success, pro, err, res);
        })
});

router.get('/delete/:_id', authenticate, checkAdmin, (req, res) => {
        Pro.removeOne(req.params, (success, pro, err) => {
            response(success, pro, err, res);
        })
});

router.post('/update/', authenticate, (req, res) => {
        Pro.updateOne(req.body, (success, pro, err) => {
            response(success, pro, err, res);
        })
});

router.post('/update/image', authenticate, (req, res) => {
        Upload.postProImage(req, (success, pro, err) => {
            response(success, pro, err, res);
        });
});

router.get('/gallery/', (req, res) => {
        Gallery.getAll(req, (success, pro, err) => {
            response(success, pro, err, res);
        });
});

router.get('/gallery/one/', (req, res) => {
        Gallery.getOne(req, (success, pro, err) => {
            response(success, pro, err, res);
        });
});

router.post('/gallery/add/', authenticate, (req, res) => {
        Upload.postProGalleryImage(req, (success, pro, err) => {
            response(success, pro, err, res);
        });
});

router.post('/gallery/delete/', authenticate, (req, res) => {
        Gallery.removeOne(req.body, (success, pro, err) => {
            response(success, pro, err, res);
        });
});

module.exports = router;
