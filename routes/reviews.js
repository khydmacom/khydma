const express = require('express');
const router = express.Router();
const keys = require('../config/keys');
const secret = keys.serverSecret;
const expressJwt = require('express-jwt');
const authenticate = expressJwt({secret});

const Review = require('../models/review');
const User = require('../models/user');
const Upload = require('./upload');

const checkAdmin = (req, res, next) => {
    User.checkAdmin(req, (err, user, message) => {
        if (err) {
            // console.log('err', message);
            next(message);
        } else {
            next()
        }
    });
};

let response = (success, review, err, res) => {
    if (success) {
        return res.status(200).json(review);
    } else if (err) {
        let errCode = err.code; // exp usually 11000
        return res.status((errCode === 11000 ? 207 : 204)).json({message: (errCode === 11000 ? "Existe déjà!" : err.errmsg)});
    } else {
        return res.status(207).json({message: "Something went wrong"});
    }
};


router.get('/one/', (req, res) => {
    Review.getOne(req.query, (success, review, err) => {
        response(success, review, err, res);
    });
});

router.get('/', (req, res) => {
    Review.getAll(req, (success, review, err) => {
        response(success, review, err, res);
    });
});

router.get('/stats/', (req, res) => {
    Review.getStatistics(req, (success, review, err) => {
        response(success, review, err, res);
    });
});

// ones that require auth as admin

router.post('/new/', authenticate, (req, res) => {
        Review.updateOne(req.body, (success, review, err) => {
            response(success, review, err, res);
        })
});

router.post('/delete/', authenticate, (req, res) => {
        Review.removeOne(req.body, (success, review, err) => {
            response(success, review, err, res);
        })
});

/*router.post('/update/', authenticate, (req, res) => {
        Review.updateOne(req.body, (success, review, err) => {
            response(success, review, err, res);
        })
});*/

module.exports = router;
