const express = require('express');
const router = express.Router();
const keys = require('../config/keys');
const secret = keys.serverSecret;
const expressJwt = require('express-jwt');
const authenticate = expressJwt({secret});

const Conversation = require('../models/conversation');
const Message = require('../models/message');

let response = (success, conversation, err, res) => {
    if (success) {
        return res.status(200).json(conversation);
    } else if (err) {
        let errCode = err.code; // exp usually 11000
        return res.status((errCode === 11000 ? 207 : 204)).json({message: (errCode === 11000 ? "Existe déjà!" : err.errmsg)});
    } else {
        return res.status(207).json({message: "Something went wrong"});
    }
};

// get all conversation or messages based on the req.query the client will send.

router.get('/', authenticate, (req, res) => {
    Conversation.getAll(req, (success, conversation, err) => {
        response(success, conversation, err, res);
    });
});

router.get('/messages/', authenticate, (req, res) => {
    Message.getAll(req, (success, conversation, err) => {
        response(success, conversation, err, res);
    });
});

// add new message or conversation

router.post('/new/', authenticate, (req, res) => {
        let conversation = req.body;

        Conversation.newConversation(conversation, (success, conversation, err) => {
            response(success, conversation, err, res);
        })
});

router.post('/new/message/', authenticate, (req, res) => {
        let message = req.body;

        Message.newMessage(message, (success, conversation, err) => {
            response(success, conversation, err, res);
        })
});

// remove conversation

router.get('/delete/:_id', authenticate, (req, res) => {
        Conversation.removeOne(req.params, (success, conversation, err) => {
            response(success, conversation, err, res);
        })
});

module.exports = router;
