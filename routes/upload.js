const express = require('express');
const expressJwt = require('express-jwt');
const path = require('path');
const fs = require('fs');
const keys = require('../config/keys');
const secret = keys.serverSecret;
const driverFields = keys.driverFields;
const authenticate = expressJwt({secret});
const mime = require('mime');
const multer = require('multer');
const crypto = require('crypto');


const allowedUserFields = [{
    name: 'profilePicture',
    maxCount: 1
}];

const allowedCategoryFields = [{
    name: 'picture',
    maxCount: 1
}];

const allowedProFields = [{
    name: 'picturePro',
    maxCount: 1
}, {
    name: 'gallery',
    maxCount: 1
}];

const uploadDir = __routeDir + '/public/images/';

if (!fs.existsSync(uploadDir)) {
    fs.mkdirSync(uploadDir);
}


const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, __routeDir + uploadDir);
    },
    filename: function (req, file, cb) {
        // console.log(file.filename, file);
        crypto.pseudoRandomBytes(8, function (err, raw) {
            cb(null, file.fieldname + raw.toString('hex') + Date.now() + '.' + mime.getExtension(file.mimetype));
        });
    }
});

const uploadUser = multer({storage: storage}).fields(allowedUserFields);
const uploadPro = multer({storage: storage}).fields(allowedProFields);
const uploadProGallery = multer({storage: storage}).fields(allowedProFields);
const uploadCategory = multer({storage: storage}).fields(allowedCategoryFields);

// Loading needed model to update
const User = require('../models/user');
const Category = require('../models/category');
const Pro = require('../models/pro');
const GalleryImage = require('../models/galleryImage');

let postCategoryImage = (req, cb) => {
    uploadCategory(req, null, (err) => {
        if (err) {
            return cb(false, null, err);
        }        // const user = req.user.user;
        const files = req.files;
        const body = req.body;
        // console.log(req);
        const host = req.protocol + "://" + req.headers.host;
        const imagePath = `${host}/images/${files[allowedCategoryFields[0].name][0].filename}`;
        Category.findByIdAndUpdate(body._id, {imagePath: imagePath}, {new: true}).populate('parentId').then((category) => {
            if (category) return cb(true, category, null);
            else return cb(false, null, null);
        }).catch((error) => {
            return cb(false, null, error);
        });
    });
};

let postProImage = (req, cb) => {
    uploadPro(req, null, (err) => {
        if (err) {
            return cb(false, null, err);
        }        // const user = req.user.user;
        const files = req.files;
        const body = req.body;
        // console.log(req);
        const host = req.protocol + "://" + req.headers.host;
        const imagePath = `${host}/images/${files[allowedProFields[0].name][0].filename}`;
        Pro.findByIdAndUpdate(body._id, {businessImagePath: imagePath}, {new: true}).populate('parentId').then((category) => {
            if (category) return cb(true, category, null);
            else return cb(false, null, null);
        }).catch((error) => {
            return cb(false, null, error);
        });
    });
};

let postProGalleryImage = (req, cb) => {
    uploadProGallery(req, null, (err) => {
        if (err) {
            return cb(false, null, err);
        }        // const user = req.user.user;
        const files = req.files;
        const body = req.body;
        // console.log(req);
        const host = req.protocol + "://" + req.headers.host;
        const imagePath = `${host}/images/${files[allowedProFields[1].name][0].filename}`;
        GalleryImage.newGalleryImage({...body, imagePath}, cb)
    });
};

let postUserImage = (req, cb) => {

    uploadUser(req, null, (err) => {
        if (err) {
            return cb(false, null, err);
        }
        const user = req.user;
        const files = req.files;
        const host = req.protocol + "://" + req.headers.host;
        const imagePath = `${host}/images/${files[allowedUserFields[0].name][0].filename}`;
        User.findByIdAndUpdate(user._id, {profilePicture: imagePath}, {new: true}).then((newUser) => {
            if (newUser) return cb(true, newUser, null);
            else return cb(false, null, null);
        }).catch((error) => {
            return cb(false, null, error);
        });
    });
};


module.exports = {
    postCategoryImage: postCategoryImage,
    postUserImage: postUserImage,
    postProImage: postProImage,
    postProGalleryImage: postProGalleryImage
};
