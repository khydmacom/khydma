const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const crypto = require('crypto');
const passport = require('passport');
const jwt = require('jsonwebtoken');
const keys = require('../config/keys');
const verifModule = require('../config/verification');
const secret = keys.serverSecret;
const FB = keys.fb;
const expressJwt = require('express-jwt');
const authenticate = expressJwt({secret});
const nodemailer = require("nodemailer");

// const {checkPermission} = require('../config/auth');

// Load User model
const User = require('../models/user');
const Client = require('../models/client');
const Upload = require('./upload');

var welcomeMail = (name) => {
    return `
        <p>Bonjour ${name},</p>
            <br>
        <p>Vous venez de vous inscrire à l’application Khydma.com</p>
            <br>
        <p>L’équipe Khydma.com</p>`;
};

// async..await is not allowed in global scope, must use a wrapper
async function sendEmail(email, name) {
    // create reusable transporter object using the default SMTP transport
    let transporter = nodemailer.createTransport({
        host: "ssl0.ovh.net",
        port: 587,
        secure: false,
        auth: {
            user: "badii@xdabier.com",
            pass: "badet@123"
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: '"L\’équipe khydma.com" <badii@xdabier.com>', // sender address
        to: email, // list of receivers
        subject: "Bienvenue sur Khydma.com", // Subject line
        html: welcomeMail(name) // html body
    };

    // send mail with defined transport object
    let info = await transporter.sendMail(mailOptions);

    console.log("Message sent: %s", info.messageId);
    // Preview only available when sending through an Ethereal account
    console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));

}

let serializeClient = (req, res, next) => {
    if (req.user)
        Client.newClient({userId: req.user._id}, (err, client) => {
            if (err)
                return next(err);
            req.user = {clientId: client._id, _id: req.user._id};
            next();
        });
    else {
        return res.json(req.authInfo);
    }
};

let validateRefreshToken = (req, res, next) => {
    Client.findUserOfToken(res, (err, user) => {
        if (err) next(err);

        req.user = user;
        next();
    })
};

let rejectToken = (req, res, next) => {
    Client.rejectToken(req.body, next)
};

let generateAccessToken = (req, res, next) => {
    req.token = req.token || {};
    req.token.accessToken = jwt.sign({
        _id: req.user._id,
        clientId: req.user.clientId
    }, secret, {
        expiresIn: "48h"
    });
    next();
};

let generateRefreshToken = (req, res, next) => {
    req.token.refreshToken = req.user.clientId.toString() + '.' + crypto.randomBytes(
        40).toString('hex');
    Client.storeToken({
        id: req.user.clientId,
        refreshToken: req.token.refreshToken
    }, next);
};

let passportLocal = (req, res, next) => {
    passport.authenticate('local', {session: false}, (err, user, info) => {
        if (!user) return respond.error(req, res, info);
        req.user = {_id: user._id};
        req.authInfo = info;
        next();
    })(req, res, next);
};

let registerUser = (req, res, next) => {
    const body = req.body;
    const _id = body._id;

    User.findOne({email: body.email}).then(user => {
        if (user) {
            return res.status(205).json({message: 'L\'email existe déjà'});
        } else {
            let newUser = new User(body);
            bcrypt.genSalt(10, (err, salt) => {
                bcrypt.hash(newUser.password, salt, (err, hash) => {
                    if (err) throw err;
                    newUser.password = hash;
                    newUser.save()
                        .then(user => {
                            // delete user.password;
                            sendEmail(user.email, user.name).then((val) => {
                                req.body = {
                                    email: body.email,
                                    password: body.password
                                };

                                next()
                            }).catch(err => {
                                console.log(err);
                                next();
                            });
                        })
                        .catch(err => {
                            console.log(err)
                            if (err.code === 11000) {
                                return res.status(207).json({message: "DupPhoneNumber"});
                            }
                        });
                });
            });
        }
    });
};

let checkAdmin = (req, res, next) => {
    User.checkAdmin(req, (err, user, message) => {
        if (err) {
            // console.log('err', message);
            next(message);
        } else {
            next()
        }
    });
};

let checkPro = (req, res, next) => {
    User.checkPro(req, (err, user, message) => {
        if (err) {
            // console.log('err', message);
            next(message);
        } else {
            next()
        }
    });
};

//////////////////////
// server responses //
//////////////////////
const respond = {
    auth: function (req, res) {
        res.status(200).json({
            user: req.user,
            token: req.token
        });
    },
    token: function (req, res) {
        res.status(201).json({
            token: req.token
        });
    },
    reject: function (req, res) {
        req.logout();
        res.status(204).end();
    },
    error: function (req, res, info) {
        res.status(207).json(info);
    }
};

let crudResponse = (success, data, err, res) => {
    if (success) {
        return res.status(200).json(data);
    } else if (err) {
        return res.status(204).json({message: 'error happened', ...err});
    } else {
        return res.status(207).json({message: "Something went wrong"});
    }
};

////////// USER ROUTES //////////////

router.post('/login', passportLocal, serializeClient, generateAccessToken, generateRefreshToken, respond.auth);
router.post('/register', verifModule.verifSignData, registerUser, passportLocal, serializeClient, generateAccessToken, generateRefreshToken, respond.auth);

router.post('/token', validateRefreshToken, generateAccessToken, respond.token);
router.post('/token/reject', rejectToken, respond.reject);

router.get('/me', authenticate, function (req, res) {
    return res.status(200).json(req.user);
});

router.get('/admin', authenticate, checkAdmin, function (req, res) {
    return res.status(200).json(req.user);
});

router.get('/pro', authenticate, checkPro, function (req, res) {
    return res.status(200).json(req.user);
});

router.post('/update-pass', authenticate, (req, res) => {
   User.authenticate(req.body.email, req.body.password, (err, user, message) => {
       if (!user) return res.json(message);
       User.updatePassword(req.body, (success, user, err) => {
           crudResponse(success, user, err, res);
       })
   })
});

router.get('/me/all', authenticate, (req, res) => {
    User.findOne({_id: req.user._id}, "-password -permissionLevel").then((user) => {
        return res.json(user);
    })
});

router.get('/:_id', (req, res) => {
    User.findOne({_id: req.params._id}, "name lastName profilePicture loc").then((user) => {
        return res.json(user);
    });
});

router.post('/update', authenticate, (req, res) => {
    User.updateOne(req.body, (success, user, err) => {
        crudResponse(success, user, err, res);
    })
});

router.post('/remove', authenticate, checkAdmin, (req, res) => {
    User.removeOne(req.body, (success, user, err) => {
        crudResponse(success, user, err, res);
    })
});

router.get('/', authenticate, checkAdmin, (req, res) => {
    User.getAll((success, user, err) => {
        crudResponse(success, user, err, res);
    })
});

router.post('/update/image', authenticate, (req, res) => {
    Upload.postUserImage(req, (success, category, err) => {
        crudResponse(success, category, err, res);
    });
});


module.exports = router;
