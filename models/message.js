const mongoose = require('mongoose');

const MessageSchema = new mongoose.Schema({
    text: {
        type: String,
        required: true
    },
    senderIdPro: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pro'
    },
    senderId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    conversationId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Conversation'
    },
    creation_date: {
        type: Date,
        default: Date.now
    }
});


MessageSchema.statics.newMessage = (body, cb) => {
    let _message = new Message(body);
    _message.save().then((message) => {
        if (message) return cb(true, message, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

MessageSchema.statics.getAll = (req, cb) => {
    Message.find(req.query).then((messages) => {
        if (messages) return cb(true, messages, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

MessageSchema.statics.removeOne = (body, cb) => {
    Message.findByIdAndDelete(body._id).then((message) => {
        if (message) return cb(true, message, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

MessageSchema.statics.getOne = (body, cb) => {
    Message.findOne(body).then((message) => {
        if (message) return cb(true, message, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

MessageSchema.statics.updateOne = (body, cb) => {
    const {_id} = body;
    console.log('messages.statics.updateOne = ', body);
    delete body._id;
    Message.findByIdAndUpdate(_id, body, {new: true}).then((message) => {
        if (message) return cb(true, message, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};


const Message = mongoose.model('Message', MessageSchema);

module.exports = Message;
