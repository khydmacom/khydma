const mongoose = require('mongoose');

const ClientSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    accessToken: {
        type: String
    },
    refreshToken: {
        type: String
    },
    creation_date: {
        type: Date,
        default: Date.now
    }
});

ClientSchema.statics.newClient = (body, cb) => {
    const client = new Client(body);
    client.save().then((_client) => {
        return cb(null, _client);
    }).catch((err) => {
        return cb(err, {});
    })
};

ClientSchema.statics.storeToken = (body, cb) => {
    Client.findByIdAndUpdate(body.id, {refreshToken: body.refreshToken}, {new: true}).then((client) => {
        return cb(null, client);
    }).catch((err) => {
        return cb(err, null)
    })
};

ClientSchema.statics.findUserOfToken = (data, cb) => {
    const body = data.req.body;
    if (!body.refreshToken)
        return cb(new Error('invalid token'));

    Client.findOne({refreshToken: body.refreshToken}).then((client) => {
        if (client) {
            return cb(null, {_id: client.userId, clientId: client._id});
        } else return cb(new Error('no user found for this token'))
    }).catch((err) => {
        return cb(new Error(err));
    });
};

ClientSchema.statics.rejectToken = (body, cb) => {
    Client.findOneAndRemove({refreshToken: body.refreshToken}).then((client) => {
        return cb(null, client);
    }).catch((err) => {
        return cb(err, null)
    })
};

const Client = mongoose.model('Client', ClientSchema);

module.exports = Client;
