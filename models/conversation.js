const mongoose = require('mongoose');

const Message = require('./message');

const ConversationSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    proId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pro'
    },
    lastMessageId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Message'
    }
});

ConversationSchema.statics.newConversation = (body, cb) => {
    let _conversation = new Conversation(body);
    _conversation.save().then((conversation) => {
        if (conversation) return cb(true, conversation, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ConversationSchema.statics.getAll = (req, cb) => {

    Conversation.find(req.query).populate("userId", "name lastName profilePicture").populate("proId", "businessName businessImagePath userId").populate('lastMessageId').then((categories) => {
        if (categories) return cb(true, categories, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ConversationSchema.statics.removeOne = (body, cb) => {
    Conversation.findByIdAndDelete(body._id).then((conversation) => {
        if (conversation) return cb(true, conversation, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ConversationSchema.statics.getOne = (body, cb) => {
    Conversation.findOne(body).populate("userId", "name lastName profilePicture").populate("proId", "businessName businessImagePath userId").populate('lastMessageId').then((conversation) => {
        if (conversation) return cb(true, conversation, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ConversationSchema.statics.updateOne = (body, cb) => {
    const {userId, proId} = body;
    console.log('onversationSchema.statics.updateOne = ', body);
    Conversation.findOneAndUpdate({userId, proId}, body, {new: true, upsert: true}).populate("userId", "name lastName profilePicture").populate("proId", "businessName businessImagePath userId").populate('lastMessageId').then((conversation) => {
        if (conversation) return cb(true, conversation, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

// deleting all related messages

ConversationSchema.post('findByIdAndDelete', function(doc, next) {
    Message.deleteMany({conversationId: doc._id}).then((result) => {
        // console.log(result);
        next();
    })
});


const Conversation = mongoose.model('Conversation', ConversationSchema);

module.exports = Conversation;
