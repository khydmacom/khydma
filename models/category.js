const mongoose = require('mongoose');

const CategorySchema = new mongoose.Schema({
    label: {
        type: String,
        required: true,
        unique: true
    },
    imagePath: {
        type: String,
        // required: true,
        // unique: true
    },
    order: {
        type: Number,
        default: 0
        // required: true,
        // unique: true
    },
    parentId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    gotKids: {
        type: Boolean,
        default: false
    },
    creation_date: {
        type: Date,
        default: Date.now
    }
});

CategorySchema.index({label: 'text'});

CategorySchema.statics.newCategory = (body, cb) => {
    let _category = new Category(body);
    _category.save().then((category) => {
        if (category) return cb(true, category, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

CategorySchema.statics.getAll = (cb) => {
    Category.find({}).populate('parentId', "-imagePath").then((categories) => {
        if (categories) return cb(true, categories, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

CategorySchema.statics.removeOne = (body, cb) => {
    Category.findByIdAndDelete(body._id).then((category) => {
        if (category) return cb(true, category, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

CategorySchema.statics.getOne = (body, cb) => {
    Category.findOne(body).populate({path: 'parentId', populate: {path: 'parentId'}}).then((category) => {
        if (category) return cb(true, category, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

CategorySchema.statics.updateOne = (body, cb) => {
    const {_id, parentId, ...others} = body;
    console.log('ategorySchema.statics.updateOne = ', parentId);
    // delete body._id;

    Category.findOneAndUpdate({_id}, (parentId && parentId.length <= 1 ) ? {...others} : parentId ? {...{parentId, ...others}} : {...others}, {new: true}).populate('parentId', "-imagePath").then((category) => {
        if (category) return cb(true, category, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

// populating save result to avoid undefined error
const populateAndPass = (doc, next) => {
    doc.populate('parentId').execPopulate().then(() => {
        next();
    });
};

CategorySchema.post('save', function (doc, next) {
    if (doc.parentId) {
        Category.findOneAndUpdate({_id: doc.parentId}, {gotKids: true}).then((res) => {
            console.log(res);
            populateAndPass(doc, next);
        })
    } else populateAndPass(doc, next);
});


const Category = mongoose.model('Category', CategorySchema);

module.exports = Category;
