const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const permissionLevels = require('../config/config').permissionLevels;

const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    isPro: {
        type: Boolean,
        default: false
    },
    email: {
        type: String,
        required: true,
        trim: true,
        unique: true
    },
    password: {
        type: String,
        required: true
    },
    fbId: {
        type: String
    },
    googleId: {
        type: String
    },
    profilePicture: {
        type: String
    },
    permissionLevel: {
        type: Number,
        default: 1
    },
    proId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pro'
    },
    creation_date: {
        type: Date,
        default: Date.now
    },
    locString: {
        type: String,
    },
    loc: {
        type: [],
        index: '2dsphere'
    }
});

UserSchema.index({loc: "2dsphere"});

UserSchema.statics.upsertFbUser = (accessToken, refreshToken, profile, cb) => {
    // var that = this;
    return User.findOne({
        fbId: profile.id
    }, (err, user) => {
        // no user was found, lets create a new one
        if (!user) {
            let newUser = new User({
                name: (profile.name.last_name ? profile.name.first_name : profile.displayName),
                lastName: (profile.name.last_name ? profile.name.last_name : ' '),
                email: profile.emails[0].value,
                fbId: profile.id,
                password: profile.id,
                profilePicture: profile.photos[0].value
            });

            bcrypt.genSalt(10, (_err, salt) => {
                bcrypt.hash(newUser.password, salt, (err_, hash) => {
                    if (err_) throw err_;
                    newUser.password = hash;
                    newUser.save()
                        .then(_user => {
                            userData = _user;
                            delete _user.password;

                            return cb(null, userData, {message: 'connexion réussie'});
                        })
                        .catch(err__ => console.log(err__));
                });
            });
        } else {
            return cb(err, user, {message: 'connexion réussie'});
        }
    });
};

UserSchema.statics.upsertGGUser = (accessToken, refreshToken, profile, cb) => {
    // var that = this;
    return User.findOne({
        googleId: profile.id
    }, (err, user) => {
        // no user was found, lets create a new one
        if (!user) {
            let newUser = new User({
                email: profile.emails[0].value,
                name: (profile.name.givenName ? profile.name.givenName : profile.name),
                lastName: (profile.name.familyName ? profile.name.familyName : " "),
                googleId: profile.id,
                password: profile._json.link,
                profilePicture: profile._json.picture
            });

            bcrypt.genSalt(10, (_err, salt) => {
                bcrypt.hash(newUser.password, salt, (err_, hash) => {
                    if (err_) throw err_;
                    newUser.password = hash;
                    newUser.save()
                        .then(_user => {
                            userData = _user;
                            delete _user.password;

                            return cb(null, userData, {message: 'connexion réussie'});
                        })
                        .catch(err__ => console.log(err__));
                });
            });
        } else {
            return cb(err, user, {message: 'connexion réussie'});
        }
    });
};

UserSchema.statics.authenticate = (email, password, cb) => {
    User.findOne({
        email: email
    }).then(user => {
        if (!user) {
            return cb(null, null, {message: 'Cet email n\'est pas enregistré'});
        }
        // Match password
        bcrypt.compare(password, user.password, (err, isMatch) => {
            if (err) throw err;
            if (isMatch) {
                return cb(null, user, {message: 'Connexion réussie à l’application'});
            } else {
                return cb(null, null, {message: 'Mot de passe incorrect'});
            }
        });
    }).catch((err) => {
        console.log("errr = ", err);
        return cb(err, null, {message: 'err'});
    });
};

UserSchema.statics.updatePassword = (data, cb) => {
    const {newPassword, _id, ...others} = data;

    bcrypt.genSalt(10, (_err, salt) => {
        bcrypt.hash(newPassword, salt, (err_, hash) => {
            if (err_) throw err_;
            let pass = hash;

            User.findByIdAndUpdate(_id, {password: pass}, {new: true}).then((user) => {
                if (user) return cb(true, user, null);
                else return cb(false, null, null);
            }).catch((error) => {
                return cb(false, null, error);
            });
        });
    });
}

UserSchema.statics.checkAdmin = (data, cb) => {
    const user = data.user;
    // console.log(data, user._id);

    User.findOne({_id: user._id, permissionLevel: permissionLevels.admin}, "_id").then((user) => {
        if (user)
            cb(null, user, null);
        else cb(true, null, {message: 'Not Admin'});
    })
};

UserSchema.statics.checkPro = (data, cb) => {
    const user = data.user;
    User.findOne({_id: user._id, permissionLevel: permissionLevels.pro, isPro: true}, "-permissionLevel -password").populate('proId').then((user) => {
        if (user)
            cb(null, user, null);
        else cb(true, null, {message: 'Not Pro'});
    })
};


UserSchema.statics.newUser = (body, cb) => {
    let _user = new User(body);
    _user.save().then((user) => {
        if (user) return cb(true, user, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

UserSchema.statics.getAll = (cb) => {
    User.find({}, "-password").then((users) => {
        if (users) return cb(true, users, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

UserSchema.statics.removeOne = (body, cb) => {
    User.findByIdAndDelete(body._id).then((user) => {
        if (user) return cb(true, user, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

/*UserSchema.statics.getOne = (body, cb) => {
    User.findOne(body).then((user) => {
        if (user) return cb(true, user, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};*/

UserSchema.statics.updateOne = (body, cb) => {
    const {_id} = body;
    delete body._id;

    if (body.loc) {
        body.loc[0] = Number(body.loc[0]);
        body.loc[1] = Number(body.loc[1]);
    }

    User.findByIdAndUpdate(_id, body, {new: true}).select("-password -permissionLevel").then((user) => {
        if (user) return cb(true, user, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

const User = mongoose.model('User', UserSchema);

module.exports = User;
