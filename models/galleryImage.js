const mongoose = require('mongoose');

const GalleryImageSchema = new mongoose.Schema({
    description: {
        type: String,
        required: true
    },
    proId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pro',
        required: true
    },
    imagePath: {
        type: String,
        required: true
    },
    creation_date: {
        type: Date,
        default: Date.now
    }
});


GalleryImageSchema.statics.newGalleryImage = (body, cb) => {
    let gGalleryImage = new GalleryImage(body);
    gGalleryImage.save().then((galleryImage) => {
        if (galleryImage) return cb(true, galleryImage, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

GalleryImageSchema.statics.getAll = (req, cb) => {
    // console.log(req.query)
    GalleryImage.find(req.query).then((galleryImage) => {
        console.log(galleryImage);
        if (galleryImage) return cb(true, galleryImage, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

GalleryImageSchema.statics.removeOne = (body, cb) => {
    GalleryImage.findByIdAndDelete(body._id).then((galleryImage) => {
        if (galleryImage) return cb(true, galleryImage, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

GalleryImageSchema.statics.getOne = (body, cb) => {
    GalleryImage.findOne(body).then((galleryImage) => {
        if (galleryImage) return cb(true, galleryImage, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

GalleryImageSchema.statics.updateOne = (body, cb) => {
    const {_id} = body;
    console.log('galleryImage.statics.updateOne = ', body);
    delete body._id;
    GalleryImage.findByIdAndUpdate(_id, body, {new: true}).then((galleryImage) => {
        if (galleryImage) return cb(true, galleryImage, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};


const GalleryImage = mongoose.model('GalleryImage', GalleryImageSchema);

module.exports = GalleryImage;
