const mongoose = require('mongoose');

const ReviewSchema = new mongoose.Schema({
    comment: {
        type: String,
        required: true
    },
    rate: {
        type: Number,
        required: true
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    proId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pro'
    },
    creation_date: {
        type: Date,
        default: Date.now
    }
});


ReviewSchema.statics.newReview = (body, cb) => {
    let _review = new Review(body);
    _review.save().then((review) => {
        if (review) return cb(true, review, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ReviewSchema.statics.getAll = (req, cb) => {
    Review.find(req.query).populate('userId').then((reviews) => {
        if (reviews) {
            let stats = {};
            stats['fives'] = [...reviews].filter(x => x.rate === 5).length;
            stats['fours'] = [...reviews].filter(x => x.rate === 4).length;
            stats['threes'] = [...reviews].filter(x => x.rate === 3).length;
            stats['twos'] = [...reviews].filter(x => x.rate === 2).length;
            stats['ones'] = [...reviews].filter(x => x.rate === 1).length;
            stats['total'] = [...reviews].length;

            return cb(true, {reviews: reviews, stats: stats}, null);
        }
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ReviewSchema.statics.getStatistics = (req, cb) => {
    Review.find(req.query).then((reviews) => {
        if (reviews) {
            let stats = {};
            stats['fives'] = [...reviews].filter(x => x.rate === 5).length;
            stats['fours'] = [...reviews].filter(x => x.rate === 4).length;
            stats['threes'] = [...reviews].filter(x => x.rate === 3).length;
            stats['twos'] = [...reviews].filter(x => x.rate === 2).length;
            stats['ones'] = [...reviews].filter(x => x.rate === 1).length;
            stats['total'] = [...reviews].length;

            return cb(true, stats, null);
        }
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ReviewSchema.statics.removeOne = (body, cb) => {
    Review.findByIdAndDelete(body._id).then((review) => {
        if (review) return cb(true, review, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ReviewSchema.statics.getOne = (body, cb) => {
    Review.findOne(body).populate('userId').then((review) => {
        if (review) return cb(true, review, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ReviewSchema.statics.updateOne = (body, cb) => {
    const {_id, userId, proId, ...others} = body;

    Review.findOneAndUpdate({userId, proId}, {userId, proId, ...others}, {new: true, upsert: true}).then((review) => {
        if (review) return cb(true, review, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};




const Review = mongoose.model('Review', ReviewSchema);

module.exports = Review;
