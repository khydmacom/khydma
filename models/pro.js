const mongoose = require('mongoose');
const User = require('./user');
const permissionLevels = require('../config/config').permissionLevels;

const Category = require('./category');
const ProIndex = require('./proIndex');

const distance = 20000;

const ProSchema = new mongoose.Schema({
    businessName: {
        type: String,
        required: true,
        unique: true
    },
    workingDaysStarting: {
        type: String
    },
    workingDaysEnding: {
        type: String
    },
    workingHoursStarting: {
        type: String
    },
    workingHoursEnding: {
        type: String
    },
    businessFee: {
        type: Number
    },
    businessFeeRate: {
        type: String
    },
    businessImagePath: {
        type: String
    },
    businessPhone: {
        type: String,
        required: true,
    },
    introduction: {
        type: String,
        required: true,
    },
    internetLinks: {
        type: String
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true
    },
    categoryId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category',
        required: true,
    },
    locString: {
        type: String,
        required: true,
    },
    loc: {
        type: [],
        index: "2dsphere"
    },
    dist: {
        calculated: {
            type: Number
        }
    },
    creation_date: {
        type: Date,
        default: Date.now
    },
    suspended: {
        type: Boolean,
        default: false
    }
});

ProSchema.index({loc: "2dsphere"});

ProSchema.statics.newPro = (body, cb) => {
    body.loc[0] = Number(body.loc[0]);
    body.loc[1] = Number(body.loc[1]);

    let _pro = new Pro(body);
    _pro.save().then((pro) => {
        if (pro) return cb(true, pro, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProSchema.statics.getAll = (req, cb) => {
    const {loc, limit, ...others} = req.query;

    if (loc && JSON.parse(loc)) {
        var locFilter = JSON.parse(loc);
        locFilter[0] = Number(JSON.parse(locFilter[0]));
        locFilter[1] = Number(JSON.parse(locFilter[1]));
        console.log(typeof locFilter[0], locFilter);
    }

    console.log(others);

    let _limit = (limit ? Number(limit) : 9999);

    let filter = locFilter ? {
        loc: {
            $near: {
                $maxDistance: distance,
                $geometry: {
                    type: "Point",
                    coordinates: locFilter
                }
            }
        }
    } : {};

    Pro.find({...others, ...filter} ? {...others, ...filter} : {...filter}).populate('userId', "-permissionLevel -password").limit(_limit).populate("categoryId").then((categories) => {
        console.log(categories);
        if (categories) return cb(true, categories, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProSchema.statics.getAllByCategoryName = (req, cb) => {
    if (JSON.parse(req.query.loc)) {
        var locFilter = JSON.parse(req.query.loc);
        locFilter[0] = Number(locFilter[0]);
        locFilter[1] = Number(locFilter[1]);
        console.log(typeof locFilter[0], locFilter);

    }

    let filter = locFilter ? {
        loc: {
            $near: {
                $maxDistance: distance,
                $geometry: {
                    type: "Point",
                    coordinates: locFilter
                }
            }
        }
    } : {};

    Category.findOne({"label": {$regex: req.query.label, $options: 'i'}}).then((cat) => {
        console.log(locFilter, {categoryId: cat._id, ...filter});

        Pro.find({categoryId: cat._id, ...filter}).populate("categoryId").then((categories) => {
            console.log(categories)
            if (categories) return cb(true, categories, null);
            else return cb(false, null, null);
        }).catch((error) => {
            return cb(false, null, error);
        });
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProSchema.statics.removeOne = (body, cb) => {
    Pro.findByIdAndDelete(body._id).then((pro) => {
        if (pro) return cb(true, pro, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProSchema.statics.getOne = (body, cb) => {
    Pro.findOne(body).populate('userId', "-permissionLevel -password").populate("categoryId").then((pro) => {
        if (pro) return cb(true, pro, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProSchema.statics.updateOne = (body, cb) => {
    const {_id} = body;

    if (body.loc) {
        body.loc[0] = Number(body.loc[0]);
        body.loc[1] = Number(body.loc[1]);
    }

    delete body._id;
    Pro.findOneAndUpdate({_id}, body, {new: true}).populate('userId', "-permissionLevel -password").populate("categoryId").then((pro) => {
        if (pro) return cb(true, pro, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

// populating save result to avoid undefined error

ProSchema.post('save', function (doc, next) {
    User.findByIdAndUpdate(doc.userId, {
        permissionLevel: permissionLevels.pro,
        isPro: true,
        proId: doc._id
    }, {new: true}).then((user) => {
        doc.populate("categoryId").populate('userId', "-permissionLevel -password").execPopulate().then((proRes) => {
            Category.getOne({_id: proRes.categoryId._id}, (succ, res, err) => {
                let parentCategoryNames = '';
                if (succ) {
                    parentCategoryNames = `${res.parentId.label} ${res.parentId.parentId ? res.parentId.parentId.label : ''}`;
                }
                let newIndex = {
                    businessName: proRes.businessName,
                    introduction: proRes.introduction,
                    proId: proRes._id,
                    categoryName: proRes.categoryId.label,
                    locString: proRes.locString,
                    parentCategoryNames,
                    loc: proRes.loc,
                    suspended: false
                };
                ProIndex.newProIndex(newIndex, (success, result, err) => {
                    if (success) next();
                    else next(err);
                });
            });
        });
    });
});

// updating Index collection

ProSchema.post('findOneAndUpdate', (error, res, next) => {
    ProIndex.updateOne(res, (success, result, err) => {
        console.log("// updating Index collection\n", success, result, err);
        next();
    })
});


const Pro = mongoose.model('Pro', ProSchema);

module.exports = Pro;
