const mongoose = require('mongoose');
const permissionLevels = require('../config/config').permissionLevels;

const ProModel = require('./pro');

const distance = 20000;

const ProIndexSchema = new mongoose.Schema({
    businessName: {
        type: String
    },
    introduction: {
        type: String
    },
    internetLinks: {
        type: String
    },
    proId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Pro',
        required: true,
        unique: true
    },
    categoryName: {
        type: String,
        required: true
    },
    parentCategoryNames: {
        type: String,
        required: true
    },
    locString: {
        type: String,
        required: true
    },
    creation_date: {
        type: Date,
        default: Date.now
    },
    suspended: {
        type: Boolean,
        default: false
    }
});

ProIndexSchema.index({
    locString: 'text',
    introduction: 'text',
    businessName: 'text',
    parentCategoryNames: 'text',
    categoryName: 'text'
}, {weights: {categoryName: 5, parentCategoryNames: 4, introduction: 3, businessName: 2, locString: 1}});

ProIndexSchema.statics.newProIndex = (body, cb) => {
    body.loc[0] = Number(body.loc[0]);
    body.loc[1] = Number(body.loc[1]);

    let _proIndex = new ProIndex(body);
    _proIndex.save().then((proIndex) => {
        if (proIndex) {
            ProIndex.ensureIndexes(function (err) {
                if (err) return console.log(err);
            });
            return cb(true, proIndex, null);
        } else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

// search on indexed pros by query

ProIndexSchema.statics.getByQueryAll = (req, cb) => {
    const {limit, ...others} = req.query;

    let _limit = (limit ? Number(limit) : 9999);

    ProIndex.find(others ? {...others} : {}).populate({
        path: 'proId',
        populate: {path: "categoryId"}
    }).limit(_limit).then((pros) => {
        console.log(pros);
        if (pros) return cb(true, pros, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

// here we perform a full text search

ProIndexSchema.statics.getAll = (req, cb) => {
    if (req.query.lat && req.query.lng) {
        var locFilter = [];
        locFilter[0] = Number(req.query.lng);
        locFilter[1] = Number(req.query.lat);
    }

    let searchPhrase = (req.query.q ? req.query.q : '');

    let filter = Boolean(locFilter) ? {
        loc: {
            $near: {
                $maxDistance: distance,
                $geometry: {
                    type: "Point",
                    coordinates: locFilter
                }
            }
        }
    } : {};


    ProIndex.find({$text: {$search: searchPhrase}, suspended: false})
        .select("proId").populate({path: 'proId', populate: {path: "categoryId"}}).then((proIndexed) => {
        // console.log(Boolean(locFilter), proIndexed);
        if (Boolean(locFilter))
            require('./pro').find(filter).then((prosByLoc) => {
                if (proIndexed) {
                    let filteredProsByLoc = [];
                    // console.log('prosByLoc = ', prosByLoc);
                    filteredProsByLoc = proIndexed.filter((pr) => {
                        return prosByLoc.findIndex((x) => {
                            // console.log(x._id, pr.proId._id)
                            return String(x._id) === String(pr.proId._id)
                        }) > -1;
                    });


                    return cb(true, filteredProsByLoc.filter((__pro) => {
                        console.log(Number(__pro.proId.businessFee), Number(req.query.maxPrice));
                        return Number(__pro.proId.businessFee ? __pro.proId.businessFee : 0) < Number(req.query.maxPrice)
                    }), null);
                } else return cb(false, null, null);
            });
        else return cb(true, proIndexed, null);

    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProIndexSchema.statics.removeOne = (body, cb) => {
    ProIndex.findByIdAndDelete(body._id).then((proIndex) => {
        if (proIndex) return cb(true, proIndex, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProIndexSchema.statics.getOne = (body, cb) => {
    ProIndex.findOne(body).populate('userId', "-permissionLevel -password").populate("categoryId").then((proIndex) => {
        if (proIndex) return cb(true, proIndex, null);
        else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};

ProIndexSchema.statics.updateOne = (body, cb) => {
    if (body.loc) {
        body.loc[0] = Number(body.loc[0]);
        body.loc[1] = Number(body.loc[1]);
    }

    const {_id, ...others} = body;

    ProIndex.findOneAndUpdate({proId: _id}, others, {new: true}).then((proIndex) => {
        if (proIndex) {
            ProIndex.ensureIndexes(function (err) {
                if (err) return console.log(err);
            });
            return cb(true, proIndex, null);
        } else return cb(false, null, null);
    }).catch((error) => {
        return cb(false, null, error);
    });
};


const ProIndex = mongoose.model('ProIndex', ProIndexSchema);

module.exports = ProIndex;
