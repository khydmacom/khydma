const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');

const app = express();
const http = require('http').createServer(app);
const io = require('./config/io');

io(http);

// Passport Config
require('./config/passport')(passport);

// DB Config
const db = require('./config/keys').mongoURI;

//defining a global to get to the root directory from any module .
global.__routeDir = __dirname;

// EJS
// app.use(expressLayouts);
// app.set('view engine', 'ejs');
app.use(express.static(__dirname + '/public'));


// Express body parser
app.use(express.urlencoded({ extended: true }));

// Express session
/*app.use(
  session({
    secret: 'secret',
    resave: true,
    saveUninitialized: true
  })
);*/

// Passport middleware
app.use(passport.initialize());
// app.use(passport.session());

app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Credentials', 'true');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,PUT,PATCH,POST,DELETE');
    res.header('Access-Control-Expose-Headers', 'Content-Length');
    res.header('Access-Control-Allow-Headers', 'Origin, Accept, Authorization, Content-Type, X-Requested-With, Range');
    if (req.method === 'OPTIONS') {
        return res.sendStatus(200);
    } else {
        return next();
    }
});


// Global variables
app.use(function(req, res, next) {
  /*res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');*/
  next();
});

// Routes
app.use('/', require('./routes/index.js'));
app.use('/api/users', require('./routes/users.js'));
app.use('/api/category', require('./routes/categories.js'));
app.use('/api/pro', require('./routes/pro.js'));
app.use('/api/messaging', require('./routes/messaging'));
app.use('/api/reviews', require('./routes/reviews'));
// app.use('/api/upload', require('./routes/upload.js').router);

const PORT = 3085;


// Connect to MongoDB
mongoose.connect(db, { useNewUrlParser: true })
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.log(err));

// app.listen(PORT, console.log(`Server started on port ${PORT}`));
const server = http.listen(PORT, () => {
    console.log('server is running on port', server.address().port);
});
